package com.partisiablockchain.governance.bporchestration;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.secata.stream.SafeDataOutputStream;

final class AccountPluginRpc {
  static final long REGISTER_BP_ASSOCIATED_TOKENS = 25_000_0000L;
  static final byte ASSOCIATE_INVOCATION_BYTE = 33;
  static final byte DISASSOCIATE_ALL_TOKENS_FOR_CONTRACT_INVOCATION_BYTE = 7;

  @SuppressWarnings("unused")
  private AccountPluginRpc() {}

  static byte[] associate(BlockchainAddress contractAddress) {
    return SafeDataOutputStream.serialize(
        stream -> {
          stream.writeByte(ASSOCIATE_INVOCATION_BYTE);
          stream.writeLong(REGISTER_BP_ASSOCIATED_TOKENS);
          contractAddress.write(stream);
        });
  }

  static byte[] disassociate(BlockchainAddress contractAddress) {
    return SafeDataOutputStream.serialize(
        stream -> {
          stream.writeByte(DISASSOCIATE_ALL_TOKENS_FOR_CONTRACT_INVOCATION_BYTE);
          contractAddress.write(stream);
        });
  }
}
