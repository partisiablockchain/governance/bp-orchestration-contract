package com.partisiablockchain.governance.bporchestration;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.tree.AvlTree;
import com.secata.tools.immutable.FixedList;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/** State for the {@link BpOrchestrationContract}. */
@Immutable
public final class BpOrchestrationContractState implements StateSerializable {

  @SuppressWarnings("Immutable")
  private final ThresholdKey thresholdKey;

  private final FixedList<BlockchainAddress> kycAddresses;
  private final int sessionId;
  private final int retryNonce;
  private final LargeOracleUpdate oracleUpdate;
  private final AvlTree<BlockchainAddress, BlockProducer> blockProducers;
  private final FixedList<BlockProducer> committee;
  private final BlockchainAddress largeOracleContract;
  private final BlockchainAddress systemUpdateContractAddress;
  private final BlockchainAddress rewardsContractAddress;
  private final AvlTree<Integer, CommitteeSignature> committeeLog;

  private final Hash domainSeparator;
  private final long broadcastRoundDelay;
  private final long committeeEnabledTimestamp;
  private final FixedList<BlockchainAddress> confirmedBlockProducersSinceLastCommittee;
  private final SemanticVersion minimumNodeVersion;

  @SuppressWarnings("unused")
  private BpOrchestrationContractState() {
    this.kycAddresses = null;
    this.sessionId = 0;
    this.retryNonce = 0;
    this.thresholdKey = null;
    this.oracleUpdate = null;
    this.blockProducers = null;
    this.committee = null;
    this.largeOracleContract = null;
    this.systemUpdateContractAddress = null;
    this.rewardsContractAddress = null;
    this.domainSeparator = null;
    this.committeeLog = null;
    this.broadcastRoundDelay = 0;
    this.committeeEnabledTimestamp = 0;
    this.confirmedBlockProducersSinceLastCommittee = null;
    this.minimumNodeVersion = null;
  }

  BpOrchestrationContractState(
      FixedList<BlockchainAddress> kycAddresses,
      int sessionId,
      int retryNonce,
      ThresholdKey thresholdKey,
      LargeOracleUpdate oracleUpdate,
      AvlTree<BlockchainAddress, BlockProducer> blockProducers,
      FixedList<BlockProducer> committee,
      BlockchainAddress largeOracleContract,
      BlockchainAddress systemUpdateContractAddress,
      BlockchainAddress rewardsContractAddress,
      Hash domainSeparator,
      AvlTree<Integer, CommitteeSignature> committeeLog,
      long broadcastRoundDelay,
      long committeeEnabledTimestamp,
      FixedList<BlockchainAddress> confirmedBlockProducersSinceLastCommittee,
      SemanticVersion minimumNodeVersion) {
    this.kycAddresses = kycAddresses;
    this.sessionId = sessionId;
    this.retryNonce = retryNonce;
    this.thresholdKey = thresholdKey;
    this.oracleUpdate = oracleUpdate;
    this.blockProducers = blockProducers;
    this.committee = committee;
    this.largeOracleContract = largeOracleContract;
    this.systemUpdateContractAddress = systemUpdateContractAddress;
    this.rewardsContractAddress = rewardsContractAddress;
    this.domainSeparator = domainSeparator;
    this.committeeLog = committeeLog;
    this.broadcastRoundDelay = broadcastRoundDelay;
    this.committeeEnabledTimestamp = committeeEnabledTimestamp;
    this.confirmedBlockProducersSinceLastCommittee = confirmedBlockProducersSinceLastCommittee;
    this.minimumNodeVersion = minimumNodeVersion;
  }

  /**
   * Create an initial state.
   *
   * @param kycAddresses the addresses of KYC accounts selecting committees
   * @param initialBlockProducers the initial large oracle
   * @param initialThresholdKey the initial threshold public key
   * @param initialKeyId the initial ID of the threshold public key
   * @param largeOracleContract the address of the large oracle contract
   * @param systemUpdateContractAddress address of the SystemUpdateContract
   * @param rewardsContractAddress address of the RewardsContract
   * @param domainSeparator a domain separator used when authorizing new oracles
   * @param broadcastRoundDelay the minimum delay in seconds between each broadcast round
   * @param blockProductionTime the block production time for the creation of the initial state
   * @return a new state object.
   */
  public static BpOrchestrationContractState initial(
      FixedList<BlockchainAddress> kycAddresses,
      List<BlockProducer> initialBlockProducers,
      BlockchainPublicKey initialThresholdKey,
      String initialKeyId,
      BlockchainAddress largeOracleContract,
      BlockchainAddress systemUpdateContractAddress,
      BlockchainAddress rewardsContractAddress,
      Hash domainSeparator,
      long broadcastRoundDelay,
      long blockProductionTime) {
    AvlTree<BlockchainAddress, BlockProducer> blockProducers =
        AvlTree.create(
            initialBlockProducers.stream()
                .collect(Collectors.toMap(BlockProducer::getIdentity, producer -> producer)));
    return new BpOrchestrationContractState(
        kycAddresses,
        0,
        0,
        new ThresholdKey(initialThresholdKey, initialKeyId),
        null,
        blockProducers,
        FixedList.create(initialBlockProducers),
        largeOracleContract,
        systemUpdateContractAddress,
        rewardsContractAddress,
        domainSeparator,
        AvlTree.create(),
        broadcastRoundDelay,
        blockProductionTime,
        FixedList.create(),
        null);
  }

  /**
   * Marks a block producer as pending.
   *
   * @param blockProducer the block producer that becomes pending
   * @return updated state
   */
  public BpOrchestrationContractState markAsPending(BlockProducer blockProducer) {
    BlockProducer associatedBlockProducer =
        blockProducer.updateStatus(BlockProducer.BlockProducerStatus.PENDING);
    AvlTree<BlockchainAddress, BlockProducer> updatedBlockProducers =
        blockProducers.set(blockProducer.getIdentity(), associatedBlockProducer);
    return new BpOrchestrationContractState(
        kycAddresses,
        sessionId,
        retryNonce,
        thresholdKey,
        oracleUpdate,
        updatedBlockProducers,
        committee,
        largeOracleContract,
        systemUpdateContractAddress,
        rewardsContractAddress,
        domainSeparator,
        committeeLog,
        broadcastRoundDelay,
        committeeEnabledTimestamp,
        confirmedBlockProducersSinceLastCommittee,
        minimumNodeVersion);
  }

  /**
   * Set the status of a producer to {@link BlockProducer.BlockProducerStatus#CONFIRMED}.
   *
   * @param blockProducer the block producer that becomes confirmed
   * @return updated state
   */
  BpOrchestrationContractState markAsConfirmed(BlockProducer blockProducer) {
    BlockProducer associatedBlockProducer =
        blockProducer.updateStatus(BlockProducer.BlockProducerStatus.CONFIRMED);
    AvlTree<BlockchainAddress, BlockProducer> updatedBlockProducers =
        blockProducers.set(blockProducer.getIdentity(), associatedBlockProducer);
    return new BpOrchestrationContractState(
        kycAddresses,
        sessionId,
        retryNonce,
        thresholdKey,
        oracleUpdate,
        updatedBlockProducers,
        committee,
        largeOracleContract,
        systemUpdateContractAddress,
        rewardsContractAddress,
        domainSeparator,
        committeeLog,
        broadcastRoundDelay,
        committeeEnabledTimestamp,
        confirmedBlockProducersSinceLastCommittee,
        minimumNodeVersion);
  }

  private boolean isCommitteeMember(BlockchainAddress address) {
    return getCommittee().stream().anyMatch(bp -> bp.getIdentity().equals(address));
  }

  /**
   * Remove an existing block producer. If the producer is part of the current committee, then it is
   * not removed immediately, but marked as {@link BlockProducer.BlockProducerStatus#REMOVED} and
   * will be removed later.
   *
   * @param blockProducer the producer to remove
   * @param disassociateTokensConsumer a consumer for disassociating the tokens of the producer
   * @return updated state.
   */
  public BpOrchestrationContractState removeBlockProducer(
      BlockProducer blockProducer, Consumer<BlockchainAddress> disassociateTokensConsumer) {
    boolean inOracleUpdate =
        oracleUpdate != null && oracleUpdate.getProducers().contains(blockProducer);
    AvlTree<BlockchainAddress, BlockProducer> updateBlockProducers = blockProducers;
    if (inOracleUpdate || isCommitteeMember(blockProducer.getIdentity())) {
      updateBlockProducers =
          updateBlockProducers.set(
              blockProducer.getIdentity(),
              blockProducer.updateStatus(BlockProducer.BlockProducerStatus.REMOVED));
    } else {
      updateBlockProducers = updateBlockProducers.remove(blockProducer.getIdentity());
      disassociateTokensConsumer.accept(blockProducer.getIdentity());
    }
    return new BpOrchestrationContractState(
        kycAddresses,
        sessionId,
        retryNonce,
        thresholdKey,
        oracleUpdate,
        updateBlockProducers,
        committee,
        largeOracleContract,
        systemUpdateContractAddress,
        rewardsContractAddress,
        domainSeparator,
        committeeLog,
        broadcastRoundDelay,
        committeeEnabledTimestamp,
        confirmedBlockProducersSinceLastCommittee,
        minimumNodeVersion);
  }

  /**
   * Confirm a new pending block producer or a proposed block producer update.
   *
   * <p>If a block producer update is denied, we do not immediately disassociate their tokens, since
   * they could be part of the current committee. Instead, we change their status to DECLINED.
   *
   * @param blockProducer address of pending account or account who proposed update
   * @return updated state
   */
  public BpOrchestrationContractState confirmBlockProducer(BlockProducer blockProducer) {
    AvlTree<BlockchainAddress, BlockProducer> updateBlockProducers = blockProducers;
    FixedList<BlockchainAddress> updatedConfirmedBlockProducersSinceLastCommittee =
        confirmedBlockProducersSinceLastCommittee;
    BlockchainAddress blockProducerIdentity = blockProducer.getIdentity();

    boolean hasBlockProducerBeenRegisteredThisCommittee =
        confirmedBlockProducersSinceLastCommittee.contains(blockProducerIdentity);
    if (blockProducer.getStatus() == BlockProducer.BlockProducerStatus.PENDING
        && !hasBlockProducerBeenRegisteredThisCommittee) {
      updatedConfirmedBlockProducersSinceLastCommittee =
          updatedConfirmedBlockProducersSinceLastCommittee.addElement(blockProducerIdentity);
    }
    updateBlockProducers =
        updateBlockProducers.set(
            blockProducerIdentity,
            blockProducer.updateStatus(BlockProducer.BlockProducerStatus.CONFIRMED));

    return new BpOrchestrationContractState(
        kycAddresses,
        sessionId,
        retryNonce,
        thresholdKey,
        oracleUpdate,
        updateBlockProducers,
        committee,
        largeOracleContract,
        systemUpdateContractAddress,
        rewardsContractAddress,
        domainSeparator,
        committeeLog,
        broadcastRoundDelay,
        committeeEnabledTimestamp,
        updatedConfirmedBlockProducersSinceLastCommittee,
        minimumNodeVersion);
  }

  /**
   * Get the block producers.
   *
   * @return the current block producers
   */
  public AvlTree<BlockchainAddress, BlockProducer> getBlockProducers() {
    return blockProducers;
  }

  /**
   * Get the address of the KYC account confirming registered block producers.
   *
   * @return the KYC address
   */
  public FixedList<BlockchainAddress> getKycAddresses() {
    return kycAddresses;
  }

  /**
   * Create a large oracle update with the new committee and reset the counter of confirmed block
   * producers.
   *
   * @param selectedCommittee the new committee
   * @return the updated state
   */
  public BpOrchestrationContractState createCommitteeUpdate(List<BlockProducer> selectedCommittee) {
    return new BpOrchestrationContractState(
        kycAddresses,
        sessionId,
        retryNonce,
        thresholdKey,
        LargeOracleUpdate.create(selectedCommittee),
        blockProducers,
        committee,
        largeOracleContract,
        systemUpdateContractAddress,
        rewardsContractAddress,
        domainSeparator,
        committeeLog,
        broadcastRoundDelay,
        committeeEnabledTimestamp,
        FixedList.create(),
        minimumNodeVersion);
  }

  /**
   * Removes all confirmed producers that have been declined since last committee update and was
   * inactive during the large oracle update protocol and disassociates their tokens.
   *
   * @param disassociateTokensConsumer consumer handling disassociating tokens
   * @return updated block producers
   */
  private AvlTree<BlockchainAddress, BlockProducer> removeDeclinedProducers(
      Consumer<BlockchainAddress> disassociateTokensConsumer,
      AvlTree<BlockchainAddress, BlockProducer> blockProducers,
      List<BlockchainAddress> activeAddresses) {
    AvlTree<BlockchainAddress, BlockProducer> withoutDeclinedProducers = blockProducers;
    for (BlockchainAddress address : blockProducers.keySet()) {
      BlockProducer bp = withoutDeclinedProducers.getValue(address);
      if (bp.getStatus() == BlockProducer.BlockProducerStatus.REMOVED
          && !activeAddresses.contains(bp.getIdentity())) {
        withoutDeclinedProducers = withoutDeclinedProducers.remove(address);
        disassociateTokensConsumer.accept(address);
      }
    }
    return withoutDeclinedProducers;
  }

  /**
   * Get the current committee.
   *
   * @return the current committee
   */
  public List<BlockProducer> getCommittee() {
    return new ArrayList<>(Objects.requireNonNull(committee));
  }

  /**
   * Get session id.
   *
   * @return session id
   */
  public int getSessionId() {
    return sessionId;
  }

  /**
   * Get the current retry nonce.
   *
   * @return retry nonce.
   */
  public int getRetryNonce() {
    return retryNonce;
  }

  /**
   * Get threshold key.
   *
   * @return threshold key
   */
  public ThresholdKey getThresholdKey() {
    return Objects.requireNonNull(thresholdKey);
  }

  /**
   * Get oracle update state.
   *
   * @return oracle update state
   */
  public LargeOracleUpdate getOracleUpdate() {
    return oracleUpdate;
  }

  /**
   * Get large oracle contract.
   *
   * @return address of large oracle contract
   */
  public BlockchainAddress getLargeOracleContract() {
    return largeOracleContract;
  }

  /**
   * Get the address of the system update contract.
   *
   * @return the address of the system update contract
   */
  public BlockchainAddress getSystemUpdateContractAddress() {
    return systemUpdateContractAddress;
  }

  /**
   * Get the address of the rewards contract.
   *
   * @return the address of the rewards contract
   */
  public BlockchainAddress getRewardsContractAddress() {
    return rewardsContractAddress;
  }

  /**
   * Get the domain separator used for large oracle authorizations.
   *
   * @return the domain separator
   */
  public Hash getDomainSeparator() {
    return domainSeparator;
  }

  /**
   * Get list of confirmed block producers since last committee.
   *
   * @return the list of confirmed block producer since last committee
   */
  public FixedList<BlockchainAddress> getConfirmedBlockProducersSinceLastCommittee() {
    return confirmedBlockProducersSinceLastCommittee;
  }

  /**
   * Get the committee history for the contract.
   *
   * @return the committee history
   */
  public AvlTree<Integer, CommitteeSignature> getCommitteeLog() {
    return committeeLog;
  }

  /**
   * Start the process of updating the large oracle.
   *
   * @param update details about the large oracle update
   * @return state with an active update.
   */
  public BpOrchestrationContractState withLargeOracleUpdate(LargeOracleUpdate update) {
    return new BpOrchestrationContractState(
        kycAddresses,
        sessionId,
        retryNonce,
        thresholdKey,
        update,
        blockProducers,
        committee,
        largeOracleContract,
        systemUpdateContractAddress,
        rewardsContractAddress,
        domainSeparator,
        committeeLog,
        broadcastRoundDelay,
        committeeEnabledTimestamp,
        confirmedBlockProducersSinceLastCommittee,
        minimumNodeVersion);
  }

  /**
   * Update the state with a new oracle from the current update.
   *
   * @param signature the signature current committee approves next committee with
   * @param blockProductionTime the block production time the new committee is created at
   * @param disassociateTokensConsumer consumer for disassociating tokens
   * @return state with a new oracle.
   */
  public BpOrchestrationContractState withNewLargeOracle(
      Signature signature,
      long blockProductionTime,
      Consumer<BlockchainAddress> disassociateTokensConsumer) {

    ProducerActiveness producerActiveness = oracleUpdate.getProducerActiveness();

    AvlTree<BlockchainAddress, BlockProducer> removedDeclinedProducers =
        removeDeclinedProducers(
            disassociateTokensConsumer, blockProducers, producerActiveness.activeAddresses());

    AvlTree<BlockchainAddress, BlockProducer> markedInactiveProducers =
        markInactiveProducers(producerActiveness.inactiveAddresses(), removedDeclinedProducers);

    List<BlockProducer> newCommittee = new ArrayList<>();
    for (BlockchainAddress committeeMember : producerActiveness.activeAddresses()) {
      newCommittee.add(removedDeclinedProducers.getValue(committeeMember));
    }

    CommitteeSignature currentCommittee =
        CommitteeSignature.create(committee, signature, thresholdKey.getKey());
    AvlTree<Integer, CommitteeSignature> updatedCommitteeLog =
        committeeLog.set(sessionId, currentCommittee);
    BpOrchestrationContractState newState =
        new BpOrchestrationContractState(
            kycAddresses,
            sessionId + 1,
            oracleUpdate.getRetryNonce(),
            oracleUpdate.getThresholdKey(),
            null,
            markedInactiveProducers,
            FixedList.create(newCommittee),
            largeOracleContract,
            systemUpdateContractAddress,
            rewardsContractAddress,
            domainSeparator,
            updatedCommitteeLog,
            broadcastRoundDelay,
            blockProductionTime,
            confirmedBlockProducersSinceLastCommittee,
            minimumNodeVersion);
    // This will trigger a new update in case there is enough producers confirmed during the
    // previous update.
    return newState.checkUpdateCommittee();
  }

  /**
   * Sets the status of all producers who did not participate in the large oracle update to {@link
   * BlockProducer.BlockProducerStatus#INACTIVE}.
   *
   * @param inactiveProducersAddresses addresses of producers who are inactive
   * @param confirmedProducers list of all confirmed producers
   * @return all producers with updated statuses for inactive producers
   */
  private AvlTree<BlockchainAddress, BlockProducer> markInactiveProducers(
      List<BlockchainAddress> inactiveProducersAddresses,
      AvlTree<BlockchainAddress, BlockProducer> confirmedProducers) {
    for (BlockchainAddress inactiveProducerAddress : inactiveProducersAddresses) {
      if (confirmedProducers.containsKey(inactiveProducerAddress)) {
        BlockProducer producer = confirmedProducers.getValue(inactiveProducerAddress);
        confirmedProducers =
            confirmedProducers.set(
                inactiveProducerAddress,
                producer.updateStatus(BlockProducer.BlockProducerStatus.INACTIVE));
      }
    }
    return confirmedProducers;
  }

  /**
   * Check if the committee should be updated. If the number of block producer confirmations has
   * exceeded five percent of the last committee size, the committee should be updated.
   *
   * @return updated state
   */
  public BpOrchestrationContractState checkUpdateCommittee() {
    long lastCommitteeSize = committee.size();
    boolean moreThanFivePercentDifference =
        (double) confirmedBlockProducersSinceLastCommittee.size() / lastCommitteeSize > 0.05;
    if (moreThanFivePercentDifference) {
      return checkCommitteeUpdateConditions();
    } else {
      return this;
    }
  }

  BpOrchestrationContractState checkCommitteeUpdateConditions() {
    List<BlockProducer> confirmedBlockProducers = getConfirmedBlockProducers();

    if (confirmedBlockProducers.size() > 3 && oracleUpdate == null) {
      return createCommitteeUpdate(confirmedBlockProducers);
    } else {
      return this;
    }
  }

  /**
   * Returns all confirmed block producers in state.
   *
   * @return confirmed block producers
   */
  public List<BlockProducer> getConfirmedBlockProducers() {
    List<BlockProducer> confirmedProducers = new ArrayList<>();
    for (BlockchainAddress address : blockProducers.keySet()) {
      BlockProducer bp = blockProducers.getValue(address);
      if (bp.getStatus() == BlockProducer.BlockProducerStatus.CONFIRMED) {
        confirmedProducers.add(bp);
      }
    }
    return confirmedProducers;
  }

  /**
   * Create the message hash used for proof-of-possession signatures.
   *
   * @param address the address of the block producer
   * @param publicKey the public key of the block producer
   * @param blsPublicKey the bls public key of the block producer
   * @return the message hash
   */
  public Hash createPopMessage(
      BlockchainAddress address, BlockchainPublicKey publicKey, BlsPublicKey blsPublicKey) {
    return Hash.create(
        h -> {
          address.write(h);
          publicKey.write(h);
          blsPublicKey.write(h);
          h.writeLong(getSessionId());
        });
  }

  /**
   * Set block producer in state. Overwrites any block producer with the same identity with the
   * supplied block producer.
   *
   * @param blockProducer block producer added to state
   * @return updated state
   */
  public BpOrchestrationContractState setBlockProducer(BlockProducer blockProducer) {
    return new BpOrchestrationContractState(
        kycAddresses,
        sessionId,
        retryNonce,
        thresholdKey,
        oracleUpdate,
        blockProducers.set(blockProducer.getIdentity(), blockProducer),
        committee,
        largeOracleContract,
        systemUpdateContractAddress,
        rewardsContractAddress,
        domainSeparator,
        committeeLog,
        broadcastRoundDelay,
        committeeEnabledTimestamp,
        confirmedBlockProducersSinceLastCommittee,
        minimumNodeVersion);
  }

  /**
   * Checks if a block producer is in the system.
   *
   * @param identity the identity of the block producer
   * @return whether or not the block producer is in the system
   */
  public boolean hasRegistered(BlockchainAddress identity) {
    return blockProducers.containsKey(identity);
  }

  /**
   * Checks if the public key or BLS public key is already in use by another block producer.
   *
   * @param publicKey the public key that is checked
   * @param blsPublicKey the BLS public key that is checked
   * @return true if both the public public key and BLS public key are unique otherwise false
   */
  public boolean arePublicKeysUnique(BlockchainPublicKey publicKey, BlsPublicKey blsPublicKey) {
    for (BlockProducer bp : blockProducers.values()) {
      BlockchainPublicKey bpPublicKey = bp.getPublicKey();
      BlsPublicKey bpBlsPublicKey = bp.getBlsPublicKey();

      if (bpPublicKey.equals(publicKey) || bpBlsPublicKey.equals(blsPublicKey)) {
        return false;
      }
    }
    return true;
  }

  /**
   * Sets the minimum node version in contract state. The minimum node version excludes block
   * producers with older versions than minimum node version from joining new committees.
   *
   * @param newMinimumVersion new minimum version
   * @return updated state
   */
  BpOrchestrationContractState setMinimumNodeVersion(SemanticVersion newMinimumVersion) {
    return new BpOrchestrationContractState(
        kycAddresses,
        sessionId,
        retryNonce,
        thresholdKey,
        oracleUpdate,
        blockProducers,
        committee,
        largeOracleContract,
        systemUpdateContractAddress,
        rewardsContractAddress,
        domainSeparator,
        committeeLog,
        broadcastRoundDelay,
        committeeEnabledTimestamp,
        confirmedBlockProducersSinceLastCommittee,
        newMinimumVersion);
  }

  /**
   * Get the amount of seconds that the contract should wait between each broadcast round before
   * trying to advance. A value greater than 0 can be used to allow slower nodes to keep up.
   *
   * @return the broadcast round delay
   */
  long getBroadcastRoundDelay() {
    return broadcastRoundDelay;
  }

  /**
   * Get the timestamp of when the current committee was enabled.
   *
   * @return the timestamp when the committee was enabled
   */
  long getCommitteeEnabledTimeStamp() {
    return committeeEnabledTimestamp;
  }

  SemanticVersion getMinimumNodeVerison() {
    return minimumNodeVersion;
  }
}
