package com.partisiablockchain.governance.bporchestration;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;

/**
 * Rpc friendly BlockProducer.
 *
 * @param name name of the producer
 * @param website website of the producer
 * @param address address of the producer
 * @param identity blockchain address of the producer
 * @param numberOfVotes number of votes
 * @param publicKey public key of the producer
 * @param blsPublicKey bls public key of the producer
 * @param entityJurisdiction entity jurisdiction
 * @param serverJurisdiction server jurisdiction
 * @param status status of the producer
 */
public record BlockProducerRpc(
    String name,
    String website,
    String address,
    BlockchainAddress identity,
    long numberOfVotes,
    BlockchainPublicKey publicKey,
    BlsPublicKey blsPublicKey,
    int entityJurisdiction,
    int serverJurisdiction,
    BlockProducer.BlockProducerStatus status) {

  BlockProducer convert() {
    return new BlockProducer(
        name,
        website,
        address,
        identity,
        publicKey,
        blsPublicKey,
        numberOfVotes,
        entityJurisdiction,
        serverJurisdiction,
        status,
        null);
  }
}
