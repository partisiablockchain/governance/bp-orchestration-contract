package com.partisiablockchain.governance.bporchestration;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializableInline;

/** The threshold key to what the new large oracle key should be. */
@Immutable
public final class ThresholdKey implements StateSerializableInline {

  @SuppressWarnings("Immutable")
  private final BlockchainPublicKey key;

  private final String keyId;

  static ThresholdKey createFromStateAccessor(StateAccessor accessor) {
    BlockchainPublicKey key = accessor.get("key").blockchainPublicKeyValue();
    String keyId = accessor.get("keyId").stringValue();
    return new ThresholdKey(key, keyId);
  }

  static ThresholdKey createFromStateAccessorNullSafe(StateAccessor accessor) {
    if (accessor.isNull()) {
      return null;
    } else {
      return createFromStateAccessor(accessor);
    }
  }

  /**
   * Default constructor.
   *
   * @param key threshold oracle key
   * @param keyId the ID of the key
   */
  public ThresholdKey(BlockchainPublicKey key, String keyId) {
    this.key = key;
    this.keyId = keyId;
  }

  ThresholdKey() {
    this.key = null;
    this.keyId = "";
  }

  /**
   * Get the threshold oracle key.
   *
   * @return threshold oracle key
   */
  public BlockchainPublicKey getKey() {
    return key;
  }

  /**
   * Get the threshold oracle key ID.
   *
   * @return threshold oracle key ID
   */
  public String getKeyId() {
    return keyId;
  }

  @Override
  public String toString() {
    return "ThresholdKey{" + "key=" + key + ", keyId='" + keyId + '\'' + '}';
  }

  @Override
  public boolean equals(Object o) {
    if (o == null) {
      return false;
    }
    if (getClass() != o.getClass()) {
      return false;
    }
    final ThresholdKey other = (ThresholdKey) o;
    if (!this.key.equals(other.key)) {
      return false;
    }
    return this.keyId.equals(other.keyId);
  }

  @Override
  public int hashCode() {
    return key.hashCode() + keyId.hashCode();
  }
}
