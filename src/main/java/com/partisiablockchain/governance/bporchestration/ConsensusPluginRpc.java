package com.partisiablockchain.governance.bporchestration;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.Hash;
import com.secata.stream.SafeDataOutputStream;
import java.util.List;

final class ConsensusPluginRpc {

  static final byte SET_COMMITTEE_INVOCATION_BYTE = 0;

  @SuppressWarnings("unused")
  private ConsensusPluginRpc() {}

  static byte[] setCommittee(Hash hashId, List<BlockProducer> committee) {
    return SafeDataOutputStream.serialize(
        s -> {
          s.writeByte(SET_COMMITTEE_INVOCATION_BYTE);
          hashId.write(s);
          s.writeInt(committee.size());
          for (BlockProducer blockProducer : committee) {
            blockProducer.getIdentity().write(s);
            blockProducer.getPublicKey().write(s);
            blockProducer.getBlsPublicKey().write(s);
          }
        });
  }
}
