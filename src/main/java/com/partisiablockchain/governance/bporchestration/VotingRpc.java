package com.partisiablockchain.governance.bporchestration;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.secata.stream.DataStreamSerializable;
import java.util.List;

final class VotingRpc {

  static final byte RECEIVE_COMMITTEE_INFORMATION_INVOCATION_BYTE = 5;

  @SuppressWarnings("unused")
  private VotingRpc() {}

  static DataStreamSerializable receiveCommitteeInformation(List<BlockProducer> committee) {
    return stream -> {
      stream.writeByte(RECEIVE_COMMITTEE_INFORMATION_INVOCATION_BYTE);
      stream.writeInt(committee.size());
      for (BlockProducer producer : committee) {
        producer.getIdentity().write(stream);
        stream.writeLong(producer.getNumberOfVotes());
      }
    };
  }
}
