package com.partisiablockchain.governance.bporchestration;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import java.util.ArrayList;
import java.util.List;

/** Producers partitioned by activeness according to a bitmap. */
public final class ProducerActiveness {
  private final List<BlockProducer> active;
  private final List<BlockProducer> inactive;

  /**
   * Default constructor. Partitions producers based on activeness. Activeness is based on a bitmap
   * with '1' being active.
   *
   * @param producers producers to partition
   * @param activeness activeness of producer
   */
  public ProducerActiveness(List<BlockProducer> producers, Bitmap activeness) {
    List<BlockProducer> active = new ArrayList<>();
    List<BlockProducer> inactive = new ArrayList<>();
    for (int i = 0; i < producers.size(); i++) {
      BlockProducer producer = producers.get(i);
      if (activeness.testBit(i)) {
        active.add(producer);
      } else {
        inactive.add(producer);
      }
    }
    this.active = active;
    this.inactive = inactive;
  }

  /**
   * Get active producers.
   *
   * @return active producers
   */
  public List<BlockProducer> active() {
    return active;
  }

  /**
   * Get inactive producers.
   *
   * @return inactive producer
   */
  public List<BlockProducer> inactive() {
    return inactive;
  }

  /**
   * Get active producer addresses.
   *
   * @return active producer addresses
   */
  public List<BlockchainAddress> activeAddresses() {
    return active().stream().map(BlockProducer::getIdentity).toList();
  }

  /**
   * Get inactive producer addresses.
   *
   * @return inactive producer addresses
   */
  public List<BlockchainAddress> inactiveAddresses() {
    return inactive().stream().map(BlockProducer::getIdentity).toList();
  }
}
