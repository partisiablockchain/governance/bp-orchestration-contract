package com.partisiablockchain.governance.bporchestration;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializableInline;
import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataOutputStream;
import com.secata.stream.SafeListStream;
import java.util.Objects;

/** A PBC block producer. */
@Immutable
public final class BlockProducer implements StateSerializableInline, DataStreamSerializable {

  /** Utility for reading a list of block producer states from a stream. */
  public static final SafeListStream<BlockProducer> LIST_SERIALIZER =
      SafeListStream.create(null, BlockProducer::write);

  private final String name;
  private final String address;
  private final String website;
  private final BlockchainAddress identity;
  private final int entityJurisdiction;
  private final int serverJurisdiction;

  @SuppressWarnings("Immutable")
  private final BlockchainPublicKey publicKey;

  private final BlsPublicKey blsPublicKey;
  private final long numberOfVotes;
  private final BlockProducerStatus status;
  private final SemanticVersion nodeVersion;

  @SuppressWarnings("EnumOrdinal")
  static BlockProducer createFromStateAccessor(StateAccessor accessor) {
    String name = accessor.get("name").stringValue();
    String address = accessor.get("address").stringValue();
    String website = accessor.get("website").stringValue();
    BlockchainAddress identity = accessor.get("identity").blockchainAddressValue();
    int entityJurisdiction = accessor.get("entityJurisdiction").intValue();
    int serverJurisdiction = accessor.get("serverJurisdiction").intValue();
    BlockchainPublicKey publicKey = accessor.get("publicKey").blockchainPublicKeyValue();
    BlsPublicKey blsPublicKey = accessor.get("blsPublicKey").blsPublicKeyValue();
    long numberOfVotes = accessor.get("numberOfVotes").longValue();
    Enum<?> enumValue = accessor.get("status").cast(Enum.class);
    BlockProducerStatus status = BlockProducerStatus.values()[enumValue.ordinal()];
    SemanticVersion nodeVersion = null;
    if (accessor.hasField("nodeVersion")) {
      nodeVersion = SemanticVersion.createFromStateAccessor(accessor.get("nodeVersion"));
    }
    return new BlockProducer(
        name,
        website,
        address,
        identity,
        publicKey,
        blsPublicKey,
        numberOfVotes,
        entityJurisdiction,
        serverJurisdiction,
        status,
        nodeVersion);
  }

  @SuppressWarnings("unused")
  BlockProducer() {
    this.name = null;
    this.address = null;
    this.website = null;
    this.identity = null;
    this.publicKey = null;
    this.blsPublicKey = null;
    numberOfVotes = 0;
    entityJurisdiction = 0;
    serverJurisdiction = 0;
    status = null;
    nodeVersion = null;
  }

  /**
   * Create a new block producer.
   *
   * @param name the name of the producer
   * @param website the website of the producer
   * @param address the address of the producer
   * @param identity the address of the producer
   * @param publicKey the public key of the producer
   * @param blsPublicKey the BLS key of the producer
   * @param numberOfVotes the number of votes for the producer
   * @param entityJurisdiction the jurisdiction of the producer - expected format is ISO 3166-1
   *     numeric
   * @param serverJurisdiction the jurisdiction of the server - expected format is ISO 3166-1
   *     numeric
   * @param status the status of the blockproducer
   * @param nodeVersion the version of the node software that the block producer is running.
   */
  public BlockProducer(
      String name,
      String website,
      String address,
      BlockchainAddress identity,
      BlockchainPublicKey publicKey,
      BlsPublicKey blsPublicKey,
      long numberOfVotes,
      int entityJurisdiction,
      int serverJurisdiction,
      BlockProducerStatus status,
      SemanticVersion nodeVersion) {
    this.name = name;
    this.website = website;
    this.address = address;
    this.identity = identity;
    this.publicKey = publicKey;
    this.blsPublicKey = blsPublicKey;
    this.numberOfVotes = numberOfVotes;
    this.entityJurisdiction = entityJurisdiction;
    this.serverJurisdiction = serverJurisdiction;
    this.status = status;
    this.nodeVersion = nodeVersion;
  }

  /**
   * Get address of the producer.
   *
   * @return address of the producer
   */
  public BlockchainAddress getIdentity() {
    return Objects.requireNonNull(identity);
  }

  /**
   * Get number of votes for producer. Currently, this will always 1.
   *
   * @return number of votes
   */
  public long getNumberOfVotes() {
    return numberOfVotes;
  }

  /**
   * Get the public key of the block producer.
   *
   * @return the public key
   */
  public BlockchainPublicKey getPublicKey() {
    return Objects.requireNonNull(publicKey);
  }

  /**
   * Get BLS key of the producer.
   *
   * @return BLS key of the producer
   */
  public BlsPublicKey getBlsPublicKey() {
    return Objects.requireNonNull(blsPublicKey);
  }

  @Override
  public void write(SafeDataOutputStream stream) {
    stream.writeString(Objects.requireNonNull(getName()));
    stream.writeString(Objects.requireNonNull(getWebsite()));
    stream.writeString(Objects.requireNonNull(getAddress()));
    getIdentity().write(stream);
    stream.writeLong(getNumberOfVotes());
    getPublicKey().write(stream);
    getBlsPublicKey().write(stream);
    stream.writeInt(entityJurisdiction);
    stream.writeInt(serverJurisdiction);
    stream.writeEnum(status);
  }

  /**
   * Get the name of the producer.
   *
   * @return name of the producer
   */
  public String getName() {
    return name;
  }

  /**
   * Get the website of the producer.
   *
   * @return website of the producer
   */
  public String getWebsite() {
    return website;
  }

  /**
   * Get the address of the producer.
   *
   * @return address of the producer
   */
  public String getAddress() {
    return address;
  }

  /**
   * Get the entity jurisdiction of the producer.
   *
   * @return entity jurisdiction of the producer
   */
  public int getEntityJurisdiction() {
    return entityJurisdiction;
  }

  /**
   * Get the server jurisdiction of the producer.
   *
   * @return server jurisdiction of the producer
   */
  public int getServerJurisdiction() {
    return serverJurisdiction;
  }

  /**
   * Update the information of a block producer.
   *
   * @param newName updated name
   * @param newWebsite updated website
   * @param newAddress updated address
   * @param newEntityJurisdiction updated entity jurisdiction
   * @param newServerJurisdiction updated server jurisdiction
   * @return updated block producer
   */
  public BlockProducer updateInfo(
      String newName,
      String newWebsite,
      String newAddress,
      int newEntityJurisdiction,
      int newServerJurisdiction) {
    return new BlockProducer(
        newName,
        newWebsite,
        newAddress,
        identity,
        publicKey,
        blsPublicKey,
        numberOfVotes,
        newEntityJurisdiction,
        newServerJurisdiction,
        status,
        nodeVersion);
  }

  /**
   * Get the status of the block producer.
   *
   * @return the status of the block producer
   */
  public BlockProducerStatus getStatus() {
    return status;
  }

  /**
   * Get the version of the node software that the block producer is running.
   *
   * @return the node version of the block producer
   */
  public SemanticVersion getNodeVersion() {
    return nodeVersion;
  }

  /**
   * Update the Node version of a block producer.
   *
   * @param newVersion updated version
   * @return updated block producer
   */
  public BlockProducer updateNodeVersion(SemanticVersion newVersion) {
    return new BlockProducer(
        name,
        website,
        address,
        identity,
        publicKey,
        blsPublicKey,
        numberOfVotes,
        entityJurisdiction,
        serverJurisdiction,
        status,
        newVersion);
  }

  /**
   * Updates the status of the block producer.
   *
   * @param status the status that the block producer gets
   * @return the blockproducer with updated status
   */
  public BlockProducer updateStatus(BlockProducerStatus status) {
    return new BlockProducer(
        name,
        website,
        address,
        identity,
        publicKey,
        blsPublicKey,
        numberOfVotes,
        entityJurisdiction,
        serverJurisdiction,
        status,
        nodeVersion);
  }

  /** The different states of a block producer. */
  public enum BlockProducerStatus {
    /** Block producer is waiting for callback after associating tokens. */
    WAITING_FOR_CALLBACK,
    /** Block producer is pending confirmation. */
    PENDING,
    /** Block producer is confirmed. */
    CONFIRMED,
    /** Block producer is pending confirmation after updating public information. */
    PENDING_UPDATE,
    /** Block producer was removed but is part of committee until new committee is selected. */
    REMOVED,
    /**
     * A block producer is automatically marked with status {@link
     * BlockProducer.BlockProducerStatus#INACTIVE} as a part of the large oracle update protocol, if
     * the producer has status {@link BlockProducer.BlockProducerStatus#CONFIRMED} but do not
     * participate in the protocol. Having status {@link BlockProducer.BlockProducerStatus#INACTIVE}
     * prevents the block producer from joining any new committee.
     */
    INACTIVE
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    BlockProducer that = (BlockProducer) o;
    return getName().equals(that.name)
        && getWebsite().equals(that.website)
        && getAddress().equals(that.address)
        && getIdentity().equals(that.identity)
        && getNumberOfVotes() == that.getNumberOfVotes()
        && getPublicKey().equals(that.publicKey)
        && getBlsPublicKey().equals(that.blsPublicKey)
        && getEntityJurisdiction() == that.entityJurisdiction
        && getServerJurisdiction() == that.serverJurisdiction
        && getStatus() == that.status
        && Objects.equals(getNodeVersion(), that.nodeVersion);
  }

  @Override
  public int hashCode() {
    return Objects.hash(
        name,
        website,
        address,
        identity,
        numberOfVotes,
        publicKey,
        blsPublicKey,
        entityJurisdiction,
        serverJurisdiction,
        status,
        nodeVersion);
  }
}
