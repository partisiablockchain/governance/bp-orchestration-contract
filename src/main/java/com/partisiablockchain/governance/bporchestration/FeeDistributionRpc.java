package com.partisiablockchain.governance.bporchestration;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.secata.stream.DataStreamSerializable;
import java.util.List;

final class FeeDistributionRpc {

  static final byte UPDATE_COMMITTEE_INVOCATION_BYTE = 1;

  @SuppressWarnings("unused")
  private FeeDistributionRpc() {}

  static DataStreamSerializable updateCommittee(List<BlockchainAddress> addresses) {
    return stream -> {
      stream.writeByte(UPDATE_COMMITTEE_INVOCATION_BYTE);
      BlockchainAddress.LIST_SERIALIZER.writeDynamic(stream, addresses);
    };
  }
}
