package com.partisiablockchain.governance.bporchestration;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.secata.stream.DataStreamSerializable;
import java.util.List;
import java.util.stream.Collectors;

final class LargeOracleRpc {

  static final byte REPLACE_LARGE_ORACLE_INVOCATION_BYTE = 4;

  @SuppressWarnings("unused")
  private LargeOracleRpc() {}

  static DataStreamSerializable replaceLargeOracle(
      List<BlockProducer> committee, BlockchainPublicKey thresholdKey) {
    return stream -> {
      stream.writeByte(REPLACE_LARGE_ORACLE_INVOCATION_BYTE);
      BlockchainPublicKey.LIST_SERIALIZER.writeDynamic(
          stream, committee.stream().map(BlockProducer::getPublicKey).collect(Collectors.toList()));
      BlockchainAddress.LIST_SERIALIZER.writeDynamic(
          stream, committee.stream().map(BlockProducer::getIdentity).collect(Collectors.toList()));
      thresholdKey.write(stream);
    };
  }
}
