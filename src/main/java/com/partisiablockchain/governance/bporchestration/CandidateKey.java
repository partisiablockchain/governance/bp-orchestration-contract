package com.partisiablockchain.governance.bporchestration;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializableInline;

/** A proposal from a new large oracle member as to what the new large oracle key should be. */
@Immutable
public final class CandidateKey implements StateSerializableInline {

  private final ThresholdKey thresholdKey;
  private final int votes;

  @SuppressWarnings("unused")
  CandidateKey() {
    this.thresholdKey = null;
    this.votes = 0;
  }

  /**
   * Default constructor.
   *
   * @param thresholdKey proposed oracle key
   * @param votes number of votes for key
   */
  public CandidateKey(ThresholdKey thresholdKey, int votes) {
    this.thresholdKey = thresholdKey;
    this.votes = votes;
  }

  static CandidateKey createFromStateAccessor(StateAccessor accessor) {
    ThresholdKey thresholdKey = ThresholdKey.createFromStateAccessor(accessor.get("thresholdKey"));
    int votes = accessor.get("votes").intValue();
    return new CandidateKey(thresholdKey, votes);
  }

  /**
   * Get proposed oracle key.
   *
   * @return proposed oracle key
   */
  public ThresholdKey getThresholdKey() {
    return thresholdKey;
  }

  /**
   * Get number of votes for key.
   *
   * @return number of votes for key
   */
  public int getVotes() {
    return votes;
  }

  /**
   * Add vote for key.
   *
   * @return next candidate key state
   */
  public CandidateKey addVote() {
    return new CandidateKey(thresholdKey, votes + 1);
  }
}
