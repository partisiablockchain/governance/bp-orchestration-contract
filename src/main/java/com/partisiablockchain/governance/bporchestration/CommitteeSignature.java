package com.partisiablockchain.governance.bporchestration;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;
import com.secata.tools.immutable.FixedList;

@Immutable
final class CommitteeSignature implements StateSerializable {

  final FixedList<BlockProducer> committeeMembers;
  final Signature signature;
  final BlockchainPublicKey thresholdKey;

  @SuppressWarnings("unused")
  private CommitteeSignature() {
    committeeMembers = null;
    signature = null;
    thresholdKey = null;
  }

  private CommitteeSignature(
      FixedList<BlockProducer> committeeMembers,
      Signature signature,
      BlockchainPublicKey thresholdKey) {
    this.committeeMembers = committeeMembers;
    this.signature = signature;
    this.thresholdKey = thresholdKey;
  }

  static CommitteeSignature createFromStateAccessor(StateAccessor accessor) {
    FixedList<BlockProducer> committeeMembers =
        StateAccessorUtil.toFixedList(
            accessor.get("committeeMembers"), BlockProducer::createFromStateAccessor);
    Signature signature = accessor.get("signature").signatureValue();
    BlockchainPublicKey thresholdKey = accessor.get("thresholdKey").blockchainPublicKeyValue();
    return new CommitteeSignature(committeeMembers, signature, thresholdKey);
  }

  public static CommitteeSignature create(
      FixedList<BlockProducer> committeeMembers,
      Signature signature,
      BlockchainPublicKey thresholdKey) {
    return new CommitteeSignature(committeeMembers, signature, thresholdKey);
  }
}
