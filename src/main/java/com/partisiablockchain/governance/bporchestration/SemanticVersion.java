package com.partisiablockchain.governance.bporchestration;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;

/**
 * Semantic version.
 *
 * @param major major version
 * @param minor minor version
 * @param patch patch version
 */
@Immutable
public record SemanticVersion(int major, int minor, int patch)
    implements StateSerializable, Comparable<SemanticVersion> {

  /**
   * Compares versions against each other with major, minor, and patch taking precedence in that
   * order.
   *
   * @param other the object to be compared.
   * @return larger than 0 if strictly greater, 0 if equal or less than 0 otherwise, compared to
   *     other
   */
  @Override
  public int compareTo(SemanticVersion other) {
    if (this.major != other.major()) {
      return Integer.compare(this.major, other.major());
    }
    if (this.minor != other.minor()) {
      return Integer.compare(this.minor, other.minor());
    }
    return Integer.compare(this.patch, other.patch());
  }

  static SemanticVersion createFromStateAccessor(StateAccessor versionAccessor) {
    if (!versionAccessor.isNull()) {
      int major = versionAccessor.get("major").intValue();
      int minor = versionAccessor.get("minor").intValue();
      int patch = versionAccessor.get("patch").intValue();
      return new SemanticVersion(major, minor, patch);
    }
    return null;
  }
}
