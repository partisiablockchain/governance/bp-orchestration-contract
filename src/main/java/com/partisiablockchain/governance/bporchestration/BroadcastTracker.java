package com.partisiablockchain.governance.bporchestration;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.tree.AvlTree;
import java.util.List;

/** Keeps track of broadcast messages with unique senders. */
@Immutable
public final class BroadcastTracker implements StateSerializable {
  private final AvlTree<BlockchainAddress, Hash> messages;
  private final AvlTree<BlockchainAddress, Bitmap> bitmaps;
  private final Bitmap echos;
  private final long startTime;

  /**
   * Create a new object for tracking broadcast messages.
   *
   * @param echos a bitmap storing the index of parties who perform an echo in this broadcast round
   * @return a new BroadcastTracker object.
   */
  static BroadcastTracker create(Bitmap echos, long startTime) {
    return new BroadcastTracker(AvlTree.create(), AvlTree.create(), echos, startTime);
  }

  private BroadcastTracker(
      AvlTree<BlockchainAddress, Hash> messages,
      AvlTree<BlockchainAddress, Bitmap> bitmaps,
      Bitmap echoes,
      long startTime) {
    this.messages = messages;
    this.bitmaps = bitmaps;
    this.echos = echoes;
    this.startTime = startTime;
  }

  @SuppressWarnings("unused")
  BroadcastTracker() {
    messages = null;
    bitmaps = null;
    echos = null;
    startTime = 0;
  }

  static BroadcastTracker createFromStateAccessor(StateAccessor accessor, int bitmapSize) {
    AvlTree<BlockchainAddress, Hash> messages =
        StateAccessorUtil.toAvlTree(
            accessor.get("messages"), StateAccessor::hashValue, BlockchainAddress.class);
    AvlTree<BlockchainAddress, Bitmap> bitmaps =
        StateAccessorUtil.toAvlTree(
            accessor.get("bitmaps"), Bitmap::createFromStateAccessor, BlockchainAddress.class);
    Bitmap echos = Bitmap.createFromStateAccessor(accessor.get("echos"));
    long startTime = accessor.get("startTime").longValue();
    return new BroadcastTracker(messages, bitmaps, echos, startTime);
  }

  /**
   * Add a broadcast message for tracking.
   *
   * @param sender the sender of the message
   * @param message the hash of the message data
   * @param numberOfParties the number of people who may relay this message
   * @return updated tracker object.
   */
  BroadcastTracker addMessage(BlockchainAddress sender, Hash message, int numberOfParties) {
    if (messages.containsKey(sender)) {
      throw new IllegalArgumentException(sender + " has already sent a message");
    }
    Bitmap bitmap = Bitmap.create(numberOfParties);
    return new BroadcastTracker(
        messages.set(sender, message), bitmaps.set(sender, bitmap), echos, startTime);
  }

  /**
   * Update information about which messages a party is storing. This method receives a bitmap from
   * a particular party, indicating which messages that party holds. The bitmaps map is updated to
   * set the bits of all the messages indicated by the provided bitmap.
   *
   * @param party the party
   * @param bitmap the bitmap
   * @param parties the list of all parties
   * @return updated tracker object.
   */
  BroadcastTracker updateBitmap(
      BlockchainAddress party, byte[] bitmap, List<BlockchainAddress> parties) {
    int partyIndex = parties.indexOf(party);
    Bitmap bm = new Bitmap(new LargeByteArray(bitmap));
    AvlTree<BlockchainAddress, Bitmap> updated = bitmaps;
    int broadcasterIndex = 0;
    for (BlockchainAddress broadcaster : parties) {
      if (bm.testBit(broadcasterIndex)) {
        Hash hash = messages.getValue(broadcaster);
        if (hash != null) {
          Bitmap current = bitmaps.getValue(broadcaster);
          updated = updated.set(broadcaster, current.setBit(partyIndex));
        }
      }
      broadcasterIndex++;
    }
    return new BroadcastTracker(messages, updated, echos.setBit(partyIndex), startTime);
  }

  int getMessageCount() {
    return messages.size();
  }

  /**
   * Get the hash of a message from some sender.
   *
   * @param sender the sender
   * @return the message, or null if non were present.
   */
  public Hash getMessage(BlockchainAddress sender) {
    return messages.getValue(sender);
  }

  /**
   * Get the bitmap of the message sent by a producer.
   *
   * @param producer the producer
   * @return the bitmap or null if the producer did not send a message.
   */
  public Bitmap getMessageBitmap(BlockchainAddress producer) {
    Hash value = messages.getValue(producer);
    if (value == null) {
      return null;
    } else {
      return bitmaps.getValue(producer);
    }
  }

  /**
   * Creates a bitmap indicating which producers have successfully broadcast a message. A party is
   * considered a broadcaster if (1) it has sent a message, and (2) at least t+1 other parties also
   * store this message. t is defined as (n - 1)/3 where n is the number of producers.
   *
   * @param producers a list of producers
   * @return a bitmap.
   */
  Bitmap getBroadcastersBitmap(List<BlockProducer> producers) {
    int threshold = (producers.size() - 1) / 3;
    Bitmap bitmap = Bitmap.create(producers.size());
    for (int i = 0; i < producers.size(); i++) {
      BlockchainAddress producer = producers.get(i).getIdentity();
      Hash message = messages.getValue(producer);
      Bitmap messageBitmap = bitmaps.getValue(producer);
      if (message != null && messageBitmap.popCount() > threshold) {
        bitmap = bitmap.setBit(i);
      }
    }
    return bitmap;
  }

  /**
   * Get the indices of parties who have supplied an echo in this round.
   *
   * @return the echos.
   */
  Bitmap getEchos() {
    return echos;
  }

  /**
   * Get the block production time that this broadcast round was started.
   *
   * @return the start time of this broadcast round
   */
  long getStartTime() {
    return startTime;
  }
}
