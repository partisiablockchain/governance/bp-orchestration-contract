package com.partisiablockchain.governance.bporchestration;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.contract.reflect.Action;
import com.partisiablockchain.contract.reflect.AutoSysContract;
import com.partisiablockchain.contract.reflect.Callback;
import com.partisiablockchain.contract.reflect.Init;
import com.partisiablockchain.contract.reflect.RpcType;
import com.partisiablockchain.contract.reflect.Upgrade;
import com.partisiablockchain.contract.sys.GlobalPluginStateUpdate;
import com.partisiablockchain.contract.sys.LocalPluginStateUpdate;
import com.partisiablockchain.contract.sys.SysContractContext;
import com.partisiablockchain.contract.sys.SystemEventCreator;
import com.partisiablockchain.contract.sys.SystemEventManager;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.crypto.bls.BlsVerifier;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.tree.AvlTree;
import com.secata.stream.DataStreamSerializable;
import com.secata.tools.coverage.FunctionUtility;
import com.secata.tools.immutable.FixedList;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** The block producer orchestration contract handling the selection of the active committee. */
@AutoSysContract(BpOrchestrationContractState.class)
public final class BpOrchestrationContract {

  private static final Logger logger = LoggerFactory.getLogger(BpOrchestrationContract.class);

  static final long ONE_DAY_MILLISECONDS =
      24L /* hours */ * 60L /* minutes */ * 60L /* seconds */ * 1000L /* milliseconds */;

  static final long ONE_MONTH_MILLISECONDS = 28L * ONE_DAY_MILLISECONDS;

  static final class Invocations {

    static final int REGISTER = 0;
    static final int CONFIRM_BP = 1;
    static final int ADD_CANDIDATE_KEY = 2;
    static final int AUTHORIZE_NEW_ORACLE = 3;
    static final int ADD_HEARTBEAT = 4;
    static final int ADD_BROADCAST_HASH = 6;
    static final int ADD_BROADCAST_BITS = 12;
    static final int POKE_ONGOING_UPDATE = 8;
    static final int GET_PRODUCER_INFO = 9;
    static final int ADD_CONFIRMED_BP = 10;
    static final int REMOVE_BP = 11;
    static final int TRIGGER_NEW_COMMITTEE = 13;
    static final int MARK_AS_ACTIVE = 14;
    static final int UPDATE_NODE_VERSION = 15;
    static final int SET_MINIMUM_NODE_VERSION = 16;
    static final int GET_PRODUCERS = 17;

    private Invocations() {}
  }

  static final class Callbacks {

    static final int REGISTER = 0;
    static final int REGISTER_AND_CONFIRM = 1;

    private Callbacks() {}

    static DataStreamSerializable registerCallback(BlockProducer blockProducer) {
      return stream -> {
        stream.writeByte(BpOrchestrationContract.Callbacks.REGISTER);
        blockProducer.write(stream);
      };
    }

    static DataStreamSerializable registerAndConfirm(BlockProducer blockProducer) {
      return stream -> {
        stream.writeByte(BpOrchestrationContract.Callbacks.REGISTER_AND_CONFIRM);
        blockProducer.write(stream);
      };
    }
  }

  /**
   * Initialize the block producer orchestration contract.
   *
   * @param context execution context
   * @param kycAddresses initial kyc addresses
   * @param initialProducers initial producers
   * @param initialThresholdKey initial threshold key
   * @param initialThresholdKeyId initial threshold key id
   * @param largeOracleContract large oracle contract address
   * @param systemUpdateContractAddress voting contract address
   * @param rewardsContractAddress fee distribution contract address
   * @param domainSeparator domain separator
   * @param broadcastRoundDelay broadcast round delay
   * @return initial state
   */
  @Init
  public BpOrchestrationContractState create(
      SysContractContext context,
      List<BlockchainAddress> kycAddresses,
      List<BlockProducerRpc> initialProducers,
      BlockchainPublicKey initialThresholdKey,
      String initialThresholdKeyId,
      BlockchainAddress largeOracleContract,
      BlockchainAddress systemUpdateContractAddress,
      BlockchainAddress rewardsContractAddress,
      Hash domainSeparator,
      long broadcastRoundDelay) {

    long blockProductionTime = context.getBlockProductionTime();
    return BpOrchestrationContractState.initial(
        FixedList.create(kycAddresses),
        initialProducers.stream().map(BlockProducerRpc::convert).toList(),
        initialThresholdKey,
        initialThresholdKeyId,
        largeOracleContract,
        systemUpdateContractAddress,
        rewardsContractAddress,
        domainSeparator,
        broadcastRoundDelay,
        blockProductionTime);
  }

  /**
   * Migrate an existing contract to this version.
   *
   * @param oldState accessor for the old state
   * @param newKycAddress new KYC address
   * @return the migrated state
   */
  @Upgrade
  public BpOrchestrationContractState upgrade(
      StateAccessor oldState, @RpcType(nullable = true) BlockchainAddress newKycAddress) {
    final ThresholdKey thresholdKey =
        ThresholdKey.createFromStateAccessor(oldState.get("thresholdKey"));
    FixedList<BlockchainAddress> kycAddresses =
        StateAccessorUtil.toFixedList(
            oldState.get("kycAddresses"), StateAccessor::blockchainAddressValue);
    if (newKycAddress != null) {
      kycAddresses = kycAddresses.addElement(newKycAddress);
    }
    final int sessionId = oldState.get("sessionId").intValue();
    final int retryNonce = oldState.get("retryNonce").intValue();
    final LargeOracleUpdate oracleUpdate =
        LargeOracleUpdate.createFromStateAccessorNullSafe(oldState.get("oracleUpdate"));
    AvlTree<BlockchainAddress, BlockProducer> blockProducers =
        StateAccessorUtil.toAvlTree(
            oldState.get("blockProducers"),
            BlockProducer::createFromStateAccessor,
            BlockchainAddress.class);
    List<BlockchainAddress> confirmedBlockProducers = new ArrayList<>();
    for (BlockchainAddress id : blockProducers.keySet()) {
      final BlockProducer producer = blockProducers.getValue(id);
      if (producer.getStatus() == BlockProducer.BlockProducerStatus.PENDING_UPDATE) {
        blockProducers =
            blockProducers.set(
                id, producer.updateStatus(BlockProducer.BlockProducerStatus.CONFIRMED));
        confirmedBlockProducers.add(producer.getIdentity());
      }
    }

    FixedList<BlockProducer> committee =
        StateAccessorUtil.toFixedList(
            oldState.get("committee"), BlockProducer::createFromStateAccessor);
    BlockchainAddress largeOracleContract =
        oldState.get("largeOracleContract").blockchainAddressValue();
    BlockchainAddress systemUpdateContractAddress =
        oldState.get("systemUpdateContractAddress").blockchainAddressValue();
    BlockchainAddress rewardsContractAddress =
        oldState.get("rewardsContractAddress").blockchainAddressValue();
    AvlTree<Integer, CommitteeSignature> committeeLog =
        StateAccessorUtil.toAvlTree(
            oldState.get("committeeLog"),
            CommitteeSignature::createFromStateAccessor,
            Integer.class);
    Hash domainSeparator = oldState.get("domainSeparator").hashValue();
    long broadcastRoundDelay = oldState.get("broadcastRoundDelay").longValue();
    long committeeEnabledTimestamp = oldState.get("committeeEnabledTimestamp").longValue();
    FixedList<BlockchainAddress> confirmedBlockProducersSinceLastCommittee =
        oldState
            .get("confirmedBlockProducersSinceLastCommittee")
            .typedFixedList(BlockchainAddress.class)
            .addElements(confirmedBlockProducers);
    SemanticVersion minimumNodeVersion = null;
    if (oldState.hasField("minimumNodeVersion")) {
      minimumNodeVersion =
          SemanticVersion.createFromStateAccessor(oldState.get("minimumNodeVersion"));
    }

    return new BpOrchestrationContractState(
            kycAddresses,
            sessionId,
            retryNonce,
            thresholdKey,
            oracleUpdate,
            blockProducers,
            committee,
            largeOracleContract,
            systemUpdateContractAddress,
            rewardsContractAddress,
            domainSeparator,
            committeeLog,
            broadcastRoundDelay,
            committeeEnabledTimestamp,
            confirmedBlockProducersSinceLastCommittee,
            minimumNodeVersion)
        .checkUpdateCommittee();
  }

  /**
   * Register the caller as a block producer.
   *
   * <p>If the caller is not already registered, then an attempt is made to associate 250000000 of
   * the caller's tokens to this contract. If this is successful, then the caller is marked as
   * PENDING. Marking the producer as CONFIRMED requires an invocation from a KYCB provider.
   *
   * @param context the contract context containing sender and chain information
   * @param state the current state of the contract
   * @param producer the information used to register a new bp
   * @param signature the bp's signature on its own address, public key and bls public key
   * @return updated state
   */
  @Action(Invocations.REGISTER)
  public BpOrchestrationContractState register(
      SysContractContext context,
      BpOrchestrationContractState state,
      BlockProducerInformationRpc producer,
      BlsSignature signature) {

    BlockProducerInformation producerInformation = producer.convert(context.getFrom());
    producerInformation.validate(state, signature);
    ensure(
        !state.hasRegistered(producerInformation.identity),
        "Block producer has already registered");

    BlockProducer blockProducer = producerInformation.asProducerWaitingForCallback();

    SystemEventManager eventManager = context.getRemoteCallsCreator();
    eventManager.registerCallbackWithCostFromRemaining(
        BpOrchestrationContract.Callbacks.registerCallback(blockProducer));
    registerBpAssociatedTokens(producerInformation.identity, eventManager, context);
    return state.setBlockProducer(blockProducer);
  }

  /**
   * Changes a PENDING producer to CONFIRMED, making them eligible to participate in committees.
   *
   * <p>Can only be called by a known KYC provider contract.
   *
   * @param context the contract context containing sender and chain information
   * @param state the current state of the contract
   * @param bpAddress the address of the bp to confirm
   * @return updated state
   */
  @Action(Invocations.CONFIRM_BP)
  public BpOrchestrationContractState confirmBp(
      SysContractContext context, BpOrchestrationContractState state, BlockchainAddress bpAddress) {
    ensure(
        state.getKycAddresses().contains(context.getFrom()),
        "Only a KYC account can confirm pending block producer updates");
    BlockProducer blockProducer = state.getBlockProducers().getValue(bpAddress);

    ensure(blockProducer != null, "Only existing block producers can be confirmed");
    BlockProducer.BlockProducerStatus bpStatus = blockProducer.getStatus();
    ensure(
        bpStatus.equals(BlockProducer.BlockProducerStatus.PENDING),
        "Only pending block producers can be confirmed");

    return state.confirmBlockProducer(blockProducer).checkUpdateCommittee();
  }

  /**
   * Adds a candidate large oracle key.
   *
   * <p>Called by a member of an incoming committee member as part of the committee selection
   * protocol. When roughly 2/3 of the committee members have added the same key, then that key is
   * chosen as the large oracle key of the incoming committee.
   *
   * @param context the contract context containing sender and chain information
   * @param state the current state of the contract
   * @param candidateKey the candidate key
   * @param keyId the id of the key
   * @param honestParties bitmap showing the bp's that the sender deems honest
   * @return updated state
   */
  @Action(Invocations.ADD_CANDIDATE_KEY)
  public BpOrchestrationContractState addCandidateKey(
      SysContractContext context,
      BpOrchestrationContractState state,
      BlockchainPublicKey candidateKey,
      String keyId,
      byte[] honestParties) {
    BlockchainAddress from = context.getFrom();
    LargeOracleUpdate oracleUpdate = state.getOracleUpdate();
    ensure(oracleUpdate != null, "No update is currently in progress");
    ensure(
        oracleUpdate.getActiveBlockProducerIndex(from) != -1,
        "Only new oracle members can propose a new threshold key");
    ensure(
        oracleUpdate.getThresholdKey() == null,
        "Threshold public key of the new oracle have already been determined");
    Hash signatureRandomization = context.getCurrentTransactionHash();
    LargeOracleUpdate update =
        oracleUpdate.addCandidateKey(
            from,
            new ThresholdKey(candidateKey, keyId),
            Bitmap.fromBytes(honestParties),
            signatureRandomization);
    return state.withLargeOracleUpdate(update);
  }

  /**
   * Authorizes an incoming committee.
   *
   * <p>This method can be called by anyone with a signature produced by the current committee on
   * the new committee. The signature validates the public key chosen through {@link
   * #addCandidateKey} as well as the members of the new oracle. The method finalizes the committee
   * change protocol.
   *
   * <p>If the signature is valid then:
   *
   * <ul>
   *   <li>All contracts that require knowledge of the current committee are updated.
   *   <li>Producers who requested to be REMOVED have the tokens associated to this contract
   *       unlocked.
   * </ul>
   *
   * @param context the contract context containing sender and chain information
   * @param state the current state of the contract
   * @param signature the signature created by the previous committee
   * @return updated state
   */
  @Action(Invocations.AUTHORIZE_NEW_ORACLE)
  public BpOrchestrationContractState authorizeNewOracle(
      SysContractContext context, BpOrchestrationContractState state, Signature signature) {
    int sessionId = state.getSessionId();
    LargeOracleUpdate oracleUpdate = state.getOracleUpdate();
    Hash message = oracleUpdate.getMessageToBeSigned(sessionId + 1, state.getDomainSeparator());
    BlockchainPublicKey publicKey = signature.recoverPublicKey(message);
    if (publicKey.equals(state.getThresholdKey().getKey())) {
      Consumer<BlockchainAddress> disassociateTokensConsumer =
          createDisassociateTokensConsumer(context);
      BpOrchestrationContractState stateWithNewOracle =
          state.withNewLargeOracle(
              signature, context.getBlockProductionTime(), disassociateTokensConsumer);
      notifyContracts(context, stateWithNewOracle);
      logger.info(
          "Oracle with session ID "
              + stateWithNewOracle.getSessionId()
              + " was confirmed and enabled at block production time "
              + stateWithNewOracle.getCommitteeEnabledTimeStamp());
      return stateWithNewOracle;
    } else {
      throw new RuntimeException("New oracle signature invalid");
    }
  }

  /**
   * Adds a "heartbeat".
   *
   * <p>The heartbeat indicates which other producers the caller could connect to. I.e., which
   * producers the caller views as alive. The heartbeat is run as the first step of the committee
   * change protocol and is used to filter out producers who are very badly connected before the
   * large oracle key generation protocol is run.
   *
   * @param context the contract context containing sender and chain information
   * @param state the current state of the contract
   * @param bitmap the bps which the sender deem alive
   * @return updated state
   */
  @Action(Invocations.ADD_HEARTBEAT)
  public BpOrchestrationContractState addHeartbeat(
      SysContractContext context, BpOrchestrationContractState state, byte[] bitmap) {
    LargeOracleUpdate oracleUpdate = state.getOracleUpdate();
    ensure(oracleUpdate != null, "No oracle update in progress");
    ensure(
        oracleUpdate.getActiveProducers() == null,
        "Active participants have already been selected");
    BlockchainAddress from = context.getFrom();
    ensure(oracleUpdate.getBlockProducerIndex(from) != -1, "Unknown producer");
    return state.withLargeOracleUpdate(
        oracleUpdate.addHeartbeat(
            from, bitmap, context.getBlockProductionTime(), state.getMinimumNodeVerison()));
  }

  /**
   * Add the hash of a message that is being broadcast.
   *
   * <p>This method is part of the large oracle key generation protocol and is used to broadcast
   * large messages in a communication efficient manner.
   *
   * @param context the contract context containing sender and chain information
   * @param state the current state of the contract
   * @param roundData information about the current round
   * @param broadcast internally used value
   * @return updated state
   */
  @Action(Invocations.ADD_BROADCAST_HASH)
  public BpOrchestrationContractState addBroadcastHash(
      SysContractContext context,
      BpOrchestrationContractState state,
      RoundData roundData,
      Hash broadcast) {
    LargeOracleUpdate oracleUpdate = state.getOracleUpdate();
    ensure(oracleUpdate != null, "No update in progress");
    BlockchainAddress from = context.getFrom();
    ensure(oracleUpdate.getActiveBlockProducerIndex(from) != -1, "Producer not part of protocol");
    validateBroadcastMetadata(roundData, oracleUpdate, state);
    return state.withLargeOracleUpdate(oracleUpdate.addBroadcast(from, broadcast));
  }

  /**
   * Add a bitmap indicating which broadcasted messages the caller now holds.
   *
   * <p>This method is part of the large oracle key generation protocol. This method lets the caller
   * indicate which messages being broadcast through {@link #addBroadcastHash} it now holds. When a
   * message has a 1 bit from more than roughly 2/3 of committee members then the message is
   * considered as having been successfully broadcast.
   *
   * @param context the contract context containing sender and chain information
   * @param state the current state of the contract
   * @param roundData information about the current round
   * @param bits internally used value
   * @return updated state
   */
  @Action(Invocations.ADD_BROADCAST_BITS)
  public BpOrchestrationContractState addBroadcastBits(
      SysContractContext context,
      BpOrchestrationContractState state,
      RoundData roundData,
      byte[] bits) {
    LargeOracleUpdate oracleUpdate = state.getOracleUpdate();
    ensure(oracleUpdate != null, "No update in progress");
    BlockchainAddress from = context.getFrom();
    ensure(oracleUpdate.getActiveBlockProducerIndex(from) != -1, "Producer not part of protocol");
    validateBroadcastMetadata(roundData, oracleUpdate, state);
    int byteCount = oracleUpdate.getActiveProducers().size() / 8 + 1;
    // The size of the bytes
    ensure(
        byteCount == bits.length, "Invalid amount of bytes: " + byteCount + " != " + bits.length);
    return addBroadcastBits(context, state, oracleUpdate, from, bits);
  }

  /**
   * Progressing to next broadcast round is decided based on two rules.
   *
   * <ul>
   *   <li>Either
   *       <ul>
   *         <li>Have received * 2t+1 broadcast messages
   *         <li>Have seen more than 2t+1 echos
   *         <li>The * current round has lasted for longer than state.broadcastRoundDelay.
   *       </ul>
   *   <li>Or
   *       <ul>
   *         <li>Have received 3t+1 broadcast messages (i.e., messages from everyone)
   *         <li>Have * seen more than 2t+1 echos.
   *       </ul>
   * </ul>
   *
   * @param context the contract context containing sender and chain information
   * @param state the current state of the contract
   * @param oracleUpdate the ongoing oracle update
   * @param from the sender of broadcast bits
   * @param bits bits send by the sender
   * @return Updated state
   */
  private static BpOrchestrationContractState addBroadcastBits(
      SysContractContext context,
      BpOrchestrationContractState state,
      LargeOracleUpdate oracleUpdate,
      BlockchainAddress from,
      byte[] bits) {
    LargeOracleUpdate update = oracleUpdate.updateBroadcastBits(from, bits);
    long blockProductionTime = context.getBlockProductionTime();
    boolean enoughTimeHasPassed =
        update.enoughTimeHasPassed(blockProductionTime, state.getBroadcastRoundDelay());
    boolean enoughBroadcastsReceived = update.hasReceivedEnoughMessages();
    boolean broadcastMessagesFromAll = update.hasBroadcastMessagesFromEveryone();
    if (enoughTimeHasPassed && enoughBroadcastsReceived) {
      logger.info(
          "Advancing broadcast round because "
              + "enough messages were seen, and enough time has passed");
      update = update.advanceBroadcastRound(blockProductionTime);
    } else if (broadcastMessagesFromAll) {
      logger.info("Advancing broadcast round because everyone sent a message");
      update = update.advanceBroadcastRound(blockProductionTime);
    }
    return state.withLargeOracleUpdate(update);
  }

  /**
   * "Pokes" an ongoing large oracle key generation protocol to let it proceed.
   *
   * <p>It might happen that all producers broadcast their messages before a particular round ends.
   * In such cases, it may be needed to "manually" tell the large oracle key generation protocol
   * that it can proceed to the next round.
   *
   * @param context the contract context containing sender and chain information
   * @param state the current state of the contract
   * @return updated state
   */
  @Action(Invocations.POKE_ONGOING_UPDATE)
  public BpOrchestrationContractState pokeOngoingUpdate(
      SysContractContext context, BpOrchestrationContractState state) {
    LargeOracleUpdate update = state.getOracleUpdate();
    ensure(update != null, "No update to poke");
    ensure(update.getThresholdKey() == null, "Cannot poke finished update");
    long currentTime = context.getBlockProductionTime();
    if (update.enoughTimeHasPassed(currentTime, state.getBroadcastRoundDelay())) {
      if (update.hasReceivedEnoughMessages()) {
        return state.withLargeOracleUpdate(update.advanceBroadcastRound(currentTime));
      } else {
        update =
            LargeOracleUpdate.createWithRetryNonce(
                FixedList.create(update.getProducers()), update.getRetryNonce() + 1);
        return state.withLargeOracleUpdate(update);
      }
    } else {
      return state;
    }
  }

  /**
   * Returns information about a CONFIRMED producer through the context.
   *
   * @param context the contract context containing sender and chain information
   * @param state the current state of the contract
   * @param identity the address of the bp
   * @return the unchanged state of the contract
   */
  @Action(Invocations.GET_PRODUCER_INFO)
  public BpOrchestrationContractState getProducerInfo(
      SysContractContext context, BpOrchestrationContractState state, BlockchainAddress identity) {
    BlockProducer producer = state.getBlockProducers().getValue(identity);
    ensure(
        producer != null, "Producer with identity " + identity.writeAsString() + " does not exist");
    ensure(
        producer.getStatus() == BlockProducer.BlockProducerStatus.CONFIRMED,
        "Producer with identity " + identity.writeAsString() + " is not confirmed");
    context.setResult(producer);
    return state;
  }

  /**
   * Combines the actions of {@link #register} and {@link #confirmBp}.
   *
   * <p>Can only be called by a KYC contract.
   *
   * @param context the contract context containing sender and chain information
   * @param state the current state of the contract
   * @param producer the information used to register a new bp
   * @param popSignature the bp's signature on its own address, public key and bls public key
   * @return updated state
   */
  @Action(Invocations.ADD_CONFIRMED_BP)
  public BpOrchestrationContractState addConfirmedBp(
      SysContractContext context,
      BpOrchestrationContractState state,
      BlockProducerInformation producer,
      BlsSignature popSignature) {
    ensure(
        state.getKycAddresses().contains(context.getFrom()),
        "Only known KYC providers can add confirmed BPs");

    if (state.hasRegistered(producer.identity)) {
      BlockProducer existing = state.getBlockProducers().getValue(producer.identity);
      ensure(
          existing.getStatus() != BlockProducer.BlockProducerStatus.WAITING_FOR_CALLBACK,
          "Cannot update information for producer undergoing registration");
      BlockProducer updatedProducer =
          existing.updateInfo(
              producer.name,
              producer.website,
              producer.address,
              producer.entityJurisdiction,
              producer.serverJurisdiction);
      return state.confirmBlockProducer(updatedProducer).checkUpdateCommittee();
    } else {
      producer.validate(state, popSignature);
      BlockProducer updatedProducer = producer.asProducerWaitingForCallback();
      SystemEventManager eventManager = context.getRemoteCallsCreator();
      eventManager.registerCallbackWithCostFromRemaining(
          Callbacks.registerAndConfirm(updatedProducer));
      registerBpAssociatedTokens(updatedProducer.getIdentity(), eventManager, context);
      return state.setBlockProducer(updatedProducer);
    }
  }

  /**
   * Attempts to remove the caller if they have previously registered and are not currently waiting
   * for the token association callback.
   *
   * @param context the contract context containing sender and chain information
   * @param state the current state of the contract
   * @return updated state
   */
  @Action(Invocations.REMOVE_BP)
  public BpOrchestrationContractState removeBp(
      SysContractContext context, BpOrchestrationContractState state) {
    BlockchainAddress sender = context.getFrom();

    ensure(state.hasRegistered(sender), "Only existing block producers can be removed");

    BlockProducer blockProducer = state.getBlockProducers().getValue(sender);
    ensure(
        blockProducer.getStatus() != BlockProducer.BlockProducerStatus.WAITING_FOR_CALLBACK,
        "Unable to remove while waiting for callback");

    Consumer<BlockchainAddress> disassociateTokensConsumer =
        createDisassociateTokensConsumer(context);

    return state
        .removeBlockProducer(blockProducer, disassociateTokensConsumer)
        .checkUpdateCommittee();
  }

  /**
   * Triggers a committee update if the current committee has been active for more than one month.
   *
   * @param context the contract context containing sender and chain information
   * @param state the current state of the contract
   * @return updated state
   */
  @Action(Invocations.TRIGGER_NEW_COMMITTEE)
  public BpOrchestrationContractState triggerNewCommittee(
      SysContractContext context, BpOrchestrationContractState state) {
    long requiredCommitteeLifetime;
    if (context.getFrom().getType() == BlockchainAddress.Type.CONTRACT_GOV) {
      requiredCommitteeLifetime = ONE_DAY_MILLISECONDS;
    } else {
      requiredCommitteeLifetime = ONE_MONTH_MILLISECONDS;
    }
    long blockProductionTime = context.getBlockProductionTime();
    long committeeEnabledAt = state.getCommitteeEnabledTimeStamp();
    long timeSinceCommitteeCreation = blockProductionTime - committeeEnabledAt;

    ensure(
        timeSinceCommitteeCreation >= requiredCommitteeLifetime,
        "Not enough time has passed since the creation of the current committee.");

    return state.checkCommitteeUpdateConditions();
  }

  /**
   * Updates the version of the node software that the block producer is running. The node version
   * is required to be greater than or equal to the minimum node version listed in the state for the
   * block producer to be eligible to participate in future committees.
   *
   * @param context the contract context containing sender and chain information
   * @param state the current state of the contract
   * @param newVersion new semantic node version
   * @return updated state
   */
  @Action(Invocations.UPDATE_NODE_VERSION)
  public BpOrchestrationContractState updateNodeVersion(
      SysContractContext context, BpOrchestrationContractState state, SemanticVersion newVersion) {
    BlockProducer producer = state.getBlockProducers().getValue(context.getFrom());
    ensure(
        producer != null,
        "Producer with identity " + context.getFrom().writeAsString() + " does not exist");
    return state.setBlockProducer(producer.updateNodeVersion(newVersion));
  }

  /**
   * Sets the minimum node version required for being committee member. Only the system update
   * contract is allowed to update the minimum node version. Block producers are required to have a
   * node version greater than or equal to the minimum node version to participate in future
   * committees.
   *
   * @param context the contract context containing sender and chain information
   * @param state the current state of the contract
   * @param newVersion new minimum semantic node version
   * @return updated state
   */
  @Action(Invocations.SET_MINIMUM_NODE_VERSION)
  public BpOrchestrationContractState setMinimumNodeVersion(
      SysContractContext context, BpOrchestrationContractState state, SemanticVersion newVersion) {
    BlockchainAddress systemUpdateContract = state.getSystemUpdateContractAddress();
    ensure(
        context.getFrom().equals(systemUpdateContract),
        "Only the system update contract can update the minimum node version");
    return state.setMinimumNodeVersion(newVersion);
  }

  /**
   * Update a block producer that has been marked as {@link
   * BlockProducer.BlockProducerStatus#INACTIVE} to be active again. This invocation can be called
   * by a block producer to indicate that any problem with the node has been fixed and that it is
   * ready to participate actively in the network again. If the invocation succeeds the * block
   * producers status will again be {@link BlockProducer.BlockProducerStatus#CONFIRMED}.
   *
   * @param context the contract context containing sender and chain information
   * @param state the current state of the contract
   * @param popSignature the block producer's proof-of-possession signature of its own address,
   *     public key and bls public key
   * @return updated state
   */
  @Action(Invocations.MARK_AS_ACTIVE)
  public BpOrchestrationContractState markAsActive(
      SysContractContext context, BpOrchestrationContractState state, BlsSignature popSignature) {
    BlockProducer blockProducer = state.getBlockProducers().getValue(context.getFrom());

    ensure(
        blockProducer != null
            && blockProducer.getStatus().equals(BlockProducer.BlockProducerStatus.INACTIVE),
        "Only inactive block producers can be marked as active");

    Hash popMessage =
        state.createPopMessage(
            blockProducer.getIdentity(),
            blockProducer.getPublicKey(),
            blockProducer.getBlsPublicKey());
    ensure(
        BlsVerifier.verify(popMessage, popSignature, blockProducer.getBlsPublicKey()),
        "Invalid proof of possession of the BLS key");

    return state.markAsConfirmed(blockProducer);
  }

  /**
   * Returns the list of addresses of all active block producers to the caller.
   *
   * @param context the system context
   * @param state the current state of the contract
   * @return an unmodified contract state
   */
  @Action(Invocations.GET_PRODUCERS)
  public BpOrchestrationContractState getProducers(
      SysContractContext context, BpOrchestrationContractState state) {
    List<BlockchainAddress> activeProducers =
        state.getCommittee().stream().map(BlockProducer::getIdentity).toList();
    context.setResult(s -> BlockchainAddress.LIST_SERIALIZER.writeDynamic(s, activeProducers));
    return state;
  }

  private static Consumer<BlockchainAddress> createDisassociateTokensConsumer(
      SysContractContext context) {
    SystemEventCreator eventManager = context.getInvocationCreator();
    return (address) ->
        eventManager.updateLocalAccountPluginState(
            LocalPluginStateUpdate.create(
                address, AccountPluginRpc.disassociate(context.getContractAddress())));
  }

  private static void registerBpAssociatedTokens(
      BlockchainAddress identity, SystemEventManager eventManager, SysContractContext context) {
    eventManager.updateLocalAccountPluginState(
        LocalPluginStateUpdate.create(
            identity, AccountPluginRpc.associate(context.getContractAddress())));
  }

  /**
   * Callback for the {@link #register} invocation.
   *
   * <p>If the callback was successful then it was possible to lock the required amount of tokens
   * for the registering producer and the producer is then marked as PENDING by this method.
   *
   * <p>If the callback failed the block producer is removed.
   *
   * @param state the current state of the contract
   * @param callbackContext information about callbacks
   * @param blockProducerRpc producer to mark as PENDING or remove if callback failed
   * @return updated state
   */
  @Callback(Callbacks.REGISTER)
  public BpOrchestrationContractState registerCallback(
      BpOrchestrationContractState state,
      CallbackContext callbackContext,
      BlockProducerRpc blockProducerRpc) {
    BlockchainAddress blockProducerAddress = blockProducerRpc.identity();
    BlockProducer blockProducer = state.getBlockProducers().getValue(blockProducerAddress);
    if (callbackContext.isSuccess()) {
      return state.markAsPending(blockProducer);
    } else {
      return state.removeBlockProducer(blockProducer, FunctionUtility.noOpConsumer());
    }
  }

  /**
   * Callback for the {@link #register} and {@link #confirmBp} invocation.
   *
   * <p>If the callback was successful then it was possible to lock the required amount of tokens
   * for the producer being registered. In that case, this method marks the producer as confirmed.
   *
   * <p>If the callback failed, then the producer is marked as removed.
   *
   * @param state the current state of the contract
   * @param callbackContext information about callbacks
   * @param producer to be confirmed
   * @return updated state
   */
  @Callback(Callbacks.REGISTER_AND_CONFIRM)
  public BpOrchestrationContractState registerAndConfirm(
      BpOrchestrationContractState state,
      CallbackContext callbackContext,
      BlockProducerRpc producer) {
    BlockchainAddress blockProducerAddress = producer.identity();
    BlockProducer blockProducer = state.getBlockProducers().getValue(blockProducerAddress);
    if (callbackContext.isSuccess()) {
      // make sure we go PENDING -> CONFIRMED in order to correctly increment the number of new
      // producers since last committee change.
      return state
          .confirmBlockProducer(
              blockProducer.updateStatus(BlockProducer.BlockProducerStatus.PENDING))
          .checkUpdateCommittee();
    } else {
      return state.removeBlockProducer(blockProducer, FunctionUtility.noOpConsumer());
    }
  }

  private static void ensure(boolean predicate, String errorMessage) {
    if (!predicate) {
      throw new RuntimeException(errorMessage);
    }
  }

  private static void validateBroadcastMetadata(
      RoundData data, LargeOracleUpdate update, BpOrchestrationContractState state) {
    int expected = state.getSessionId() + 1;
    ensure(
        expected == data.sessionId(), "Invalid session: " + expected + " != " + data.sessionId());
    ensure(
        update.getRetryNonce() == data.retryNonce(),
        "Invalid retry nonce: " + update.getRetryNonce() + " != " + data.retryNonce());
    int currentBroadcastRound = update.getCurrentBroadcastRound();
    ensure(
        currentBroadcastRound == data.broadcastRound(),
        "Invalid broadcast round: " + currentBroadcastRound + " != " + data.broadcastRound());
  }

  /**
   * Send information about the new oracle to relevant contracts and plugins.
   *
   * @param context the system contract context for sending events
   * @param stateWithNewOracle the state with the updated oracle
   */
  private static void notifyContracts(
      SysContractContext context, BpOrchestrationContractState stateWithNewOracle) {
    List<BlockProducer> updatedCommittee = stateWithNewOracle.getCommittee();

    SystemEventCreator eventManager = context.getInvocationCreator();
    eventManager
        .invoke(stateWithNewOracle.getLargeOracleContract())
        .withPayload(
            LargeOracleRpc.replaceLargeOracle(
                updatedCommittee, stateWithNewOracle.getThresholdKey().getKey()))
        .sendFromContract();

    eventManager
        .invoke(stateWithNewOracle.getSystemUpdateContractAddress())
        .withPayload(VotingRpc.receiveCommitteeInformation(updatedCommittee))
        .sendFromContract();

    List<BlockchainAddress> addresses =
        updatedCommittee.stream().map(BlockProducer::getIdentity).collect(Collectors.toList());
    eventManager
        .invoke(stateWithNewOracle.getRewardsContractAddress())
        .withPayload(FeeDistributionRpc.updateCommittee(addresses))
        .sendFromContract();

    Hash hashId =
        Hash.create(
            hash -> {
              hash.writeInt(stateWithNewOracle.getSessionId());
              BlockchainAddress.LIST_SERIALIZER.writeDynamic(hash, addresses);
            });
    eventManager.updateGlobalConsensusPluginState(
        GlobalPluginStateUpdate.create(ConsensusPluginRpc.setCommittee(hashId, updatedCommittee)));
  }

  /**
   * Struct holding round data.
   *
   * @param sessionId session id
   * @param retryNonce retry nonce
   * @param broadcastRound broadcast round
   */
  public record RoundData(int sessionId, int retryNonce, int broadcastRound) {}

  /**
   * Struct identical to BlockProducerInformation, without identity field.
   *
   * @param name name of the producer
   * @param website website of the producer
   * @param address address of the producer
   * @param publicKey public key of the producer
   * @param blsPublicKey bls public key of the producer
   * @param entityJurisdiction entity jurisdiction
   * @param serverJurisdiction server jurisdiction
   */
  public record BlockProducerInformationRpc(
      String name,
      String website,
      String address,
      BlockchainPublicKey publicKey,
      BlsPublicKey blsPublicKey,
      int entityJurisdiction,
      int serverJurisdiction) {

    BlockProducerInformation convert(BlockchainAddress identity) {
      return new BlockProducerInformation(
          identity,
          name,
          website,
          address,
          publicKey,
          blsPublicKey,
          entityJurisdiction,
          serverJurisdiction);
    }
  }

  /**
   * Struct holding information about a block producer.
   *
   * @param identity blockchain address of the producer
   * @param name name of the producer
   * @param website website of the producer
   * @param address address of the producer
   * @param publicKey public key of the producer
   * @param blsPublicKey bls public key of the producer
   * @param entityJurisdiction entity jurisdiction
   * @param serverJurisdiction server jurisdiction
   */
  public record BlockProducerInformation(
      BlockchainAddress identity,
      String name,
      String website,
      String address,
      BlockchainPublicKey publicKey,
      BlsPublicKey blsPublicKey,
      int entityJurisdiction,
      int serverJurisdiction) {

    void validate(BpOrchestrationContractState state, BlsSignature popSignature) {
      boolean uniquePublicKeys = state.arePublicKeysUnique(publicKey, blsPublicKey);
      ensure(uniquePublicKeys, "Public key or BLS public key already in use");

      Hash popMessage = state.createPopMessage(identity, publicKey, blsPublicKey);
      ensure(
          BlsVerifier.verify(popMessage, popSignature, blsPublicKey),
          "Invalid proof of possession of the BLS key");
    }

    BlockProducer asProducerWaitingForCallback() {
      return new BlockProducer(
          name,
          website,
          address,
          identity,
          publicKey,
          blsPublicKey,
          1,
          entityJurisdiction,
          serverJurisdiction,
          BlockProducer.BlockProducerStatus.WAITING_FOR_CALLBACK,
          null);
    }
  }
}
