package com.partisiablockchain.governance.bporchestration;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.tree.AvlTree;
import com.secata.tools.immutable.FixedList;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** Container for information about a large oracle update. */
@Immutable
public final class LargeOracleUpdate implements StateSerializable {

  private static final Logger logger = LoggerFactory.getLogger(LargeOracleUpdate.class);

  private final FixedList<BlockProducer> producers;

  @SuppressWarnings("Immutable")
  private final ThresholdKey thresholdKey;

  @SuppressWarnings("Immutable")
  private final FixedList<CandidateKey> candidates;

  // Map of a particular party's
  private final AvlTree<BlockchainAddress, Bitmap> honestPartyViews;

  private final int retryNonce;
  private final Bitmap voters;
  private final Bitmap activeProducers;
  private final AvlTree<BlockchainAddress, Bitmap> heartbeats;

  private final Bitmap broadcasters;
  private final FixedList<BroadcastTracker> broadcastMessages;
  private final Hash signatureRandomization;

  /**
   * Read a large oracle update object from StateAccessor. The resulting update is either null, if
   * the accessor points to a null object, or a "fresh" update object with a bumped retry nonce but
   * the same producers as the old object.
   *
   * @param accessor the accessor
   * @return a large oracle update.
   */
  static LargeOracleUpdate createFromStateAccessorNullSafe(StateAccessor accessor) {
    if (accessor.isNull()) {
      return null;
    } else {
      return createFromStateAccessor(accessor);
    }
  }

  private static LargeOracleUpdate createFromStateAccessor(StateAccessor accessor) {
    FixedList<BlockProducer> producers =
        StateAccessorUtil.toFixedList(
            accessor.get("producers"), BlockProducer::createFromStateAccessor);
    ThresholdKey thresholdKey =
        ThresholdKey.createFromStateAccessorNullSafe(accessor.get("thresholdKey"));
    int retryNonce = accessor.get("retryNonce").intValue();
    FixedList<CandidateKey> candidates =
        StateAccessorUtil.toFixedList(
            accessor.get("candidates"), CandidateKey::createFromStateAccessor);
    Bitmap voters = Bitmap.createFromStateAccessor(accessor.get("voters"));
    Bitmap activeProducers = Bitmap.createFromStateAccessor(accessor.get("activeProducers"));
    AvlTree<BlockchainAddress, Bitmap> heartbeats =
        StateAccessorUtil.toAvlTree(
            accessor.get("heartbeats"), Bitmap::createFromStateAccessor, BlockchainAddress.class);
    FixedList<BroadcastTracker> broadcastMessages =
        StateAccessorUtil.toFixedList(
            accessor.get("broadcastMessages"),
            a -> BroadcastTracker.createFromStateAccessor(a, producers.size()));
    Bitmap broadcasters = Bitmap.createFromStateAccessor(accessor.get("broadcasters"));
    Hash signatureRandomization = accessor.get("signatureRandomization").hashValue();
    AvlTree<BlockchainAddress, Bitmap> honestPartyViews =
        StateAccessorUtil.toAvlTree(
            accessor.get("honestPartyViews"),
            Bitmap::createFromStateAccessor,
            BlockchainAddress.class);
    return new LargeOracleUpdate(
        producers,
        thresholdKey,
        candidates,
        voters,
        heartbeats,
        activeProducers,
        retryNonce,
        broadcasters,
        broadcastMessages,
        honestPartyViews,
        signatureRandomization);
  }

  static LargeOracleUpdate create(List<BlockProducer> initialProducers) {
    return createWithRetryNonce(FixedList.create(initialProducers), 0);
  }

  static LargeOracleUpdate createWithRetryNonce(
      FixedList<BlockProducer> initialProducers, int retryNonce) {
    return new LargeOracleUpdate(
        initialProducers,
        null,
        FixedList.create(),
        Bitmap.create(initialProducers.size()),
        AvlTree.create(),
        null,
        retryNonce,
        Bitmap.create(initialProducers.size()),
        FixedList.create(),
        AvlTree.create(),
        null);
  }

  @SuppressWarnings("unused")
  LargeOracleUpdate() {
    this.producers = null;
    this.thresholdKey = null;
    this.candidates = null;
    this.voters = null;
    this.heartbeats = null;
    this.activeProducers = null;
    this.retryNonce = 0;
    this.broadcasters = null;
    this.broadcastMessages = null;
    this.honestPartyViews = null;
    this.signatureRandomization = null;
  }

  LargeOracleUpdate(
      FixedList<BlockProducer> producers,
      ThresholdKey thresholdKey,
      FixedList<CandidateKey> candidates,
      Bitmap voters,
      AvlTree<BlockchainAddress, Bitmap> heartbeats,
      Bitmap activeProducers,
      int retryNonce,
      Bitmap broadcasters,
      FixedList<BroadcastTracker> broadcastMessages,
      AvlTree<BlockchainAddress, Bitmap> honestPartyViews,
      Hash signatureRandomization) {
    this.producers = producers;
    this.thresholdKey = thresholdKey;
    this.candidates = candidates;
    this.voters = voters;
    this.heartbeats = heartbeats;
    this.activeProducers = activeProducers;
    this.retryNonce = retryNonce;
    this.broadcasters = broadcasters;
    this.broadcastMessages = broadcastMessages;
    this.honestPartyViews = honestPartyViews;
    this.signatureRandomization = signatureRandomization;
  }

  /**
   * Get all block producers.
   *
   * @return list of block producer states
   */
  public List<BlockProducer> getProducers() {
    return new ArrayList<>(Objects.requireNonNull(producers));
  }

  /**
   * Get the list of producers determined to be active.
   *
   * @return a list of active producers.
   */
  public List<BlockProducer> getActiveProducers() {
    if (activeProducers == null) {
      return null;
    } else {
      return new ProducerActiveness(getProducers(), activeProducers).active();
    }
  }

  /**
   * Get all producers partitioned by their activeness in large oracle update. Producers are
   * considered active if they sent a heartbeat during the large oracle update protocol.
   *
   * @return Active and inactive producers.
   */
  public ProducerActiveness getProducerActiveness() {
    return new ProducerActiveness(getProducers(), activeProducers);
  }

  /**
   * Get the random hash for majority candidate key.
   *
   * @return random hash for chosen candidate key
   */
  public Hash getSignatureRandomization() {
    return signatureRandomization;
  }

  /**
   * Get the list of producers which successfully broadcast a message in some round.
   *
   * @param roundNumber the round number
   * @return a list of producers.
   */
  public List<BlockProducer> getBroadcasters(int roundNumber) {
    if (activeProducers == null) {
      return null;
    } else {
      List<BlockProducer> activeProducers = getActiveProducers();
      Bitmap current = getBroadcastTracker(roundNumber).getBroadcastersBitmap(activeProducers);
      Bitmap previous = Bitmap.allOnes(activeProducers.size());
      if (roundNumber > 1) {
        previous = getBroadcastTracker(roundNumber - 1).getBroadcastersBitmap(activeProducers);
      }
      return new ProducerActiveness(activeProducers, current.and(previous)).active();
    }
  }

  Hash getMerkleRootOfActiveProducers() {
    List<Hash> activeProducersHashes =
        getActiveProducers().stream()
            .map(bp -> Hash.create(s -> s.write(getEncodedWithoutHeader(bp.getPublicKey()))))
            .collect(Collectors.toList());
    Collections.sort(
        activeProducersHashes, (h1, h2) -> h1.toString().compareToIgnoreCase(h2.toString()));
    return MerkleTree.buildTreeFrom(activeProducersHashes);
  }

  /**
   * Add a heartbeat from a producer and attempt to select a large enough subset of active producers
   * if possible.
   *
   * @param sender the sender of the heartbeat
   * @param heartbeat the heartbeat
   * @param time current block time
   * @param minimumNodeVersion minimum version of node software to join committee
   * @return an updated large oracle update object.
   */
  public LargeOracleUpdate addHeartbeat(
      BlockchainAddress sender, byte[] heartbeat, long time, SemanticVersion minimumNodeVersion) {
    if (!heartbeatValid(heartbeat)) {
      throw new IllegalStateException("Malformed heartbeat message");
    }

    Bitmap heartbeatBitmap = Bitmap.fromBytes(heartbeat);
    if (heartbeats.containsKey(sender)) {
      throw new IllegalStateException(sender + " has already supplied a heartbeat");
    }

    AvlTree<BlockchainAddress, Bitmap> updated = heartbeats.set(sender, heartbeatBitmap);

    Bitmap attemptedSelection = attemptSelectionOfProducers(updated);
    if (attemptedSelection == null) {
      return new LargeOracleUpdate(
          producers,
          thresholdKey,
          candidates,
          voters,
          updated,
          null,
          retryNonce,
          broadcasters,
          broadcastMessages,
          honestPartyViews,
          signatureRandomization);
    }
    int threshold = 2 * getCorruptionThreshold() + 1;
    Bitmap upToDateProducers = getUpToDateProducers(minimumNodeVersion);
    Bitmap upToDateActiveProducers = upToDateProducers.and(attemptedSelection);
    int popCount = upToDateActiveProducers.popCount();
    if (popCount < threshold) {
      return new LargeOracleUpdate(
          producers,
          thresholdKey,
          candidates,
          voters,
          AvlTree.create(),
          null,
          retryNonce,
          broadcasters,
          broadcastMessages,
          honestPartyViews,
          signatureRandomization);
    } else {
      Bitmap echos = Bitmap.fromBytes(new byte[upToDateActiveProducers.sizeInBytes()]);
      return new LargeOracleUpdate(
          producers,
          thresholdKey,
          candidates,
          voters,
          updated,
          upToDateActiveProducers,
          retryNonce,
          Bitmap.allOnes(upToDateActiveProducers.popCount()),
          FixedList.create(List.of(BroadcastTracker.create(echos, time))),
          honestPartyViews,
          signatureRandomization);
    }
  }

  private boolean heartbeatValid(byte[] heartbeat) {
    boolean lengthValid = heartbeat.length == producers.size() / 8 + 1;
    byte lastHeartbeatBlock = heartbeat[heartbeat.length - 1];
    boolean lastBlockValid = lastHeartbeatBlock < (1 << (producers.size() % 8));
    return lengthValid && lastBlockValid;
  }

  /**
   * Filters producers based node version compared to minimumNodeVersion. Producer indices with a
   * node version greater or equal to minimumNodeVersion is set to 1, otherwise 0. If
   * minimumNodeVersion is null all producer is assumed to be up-to-date.
   *
   * @param minimumNodeVersion minimum version of node software required
   * @return indices of up-to-date producers
   */
  private Bitmap getUpToDateProducers(SemanticVersion minimumNodeVersion) {
    if (minimumNodeVersion == null) {
      return Bitmap.allOnes(producers.size());
    }

    Bitmap upToDateProducers = Bitmap.create(producers.size());
    for (int i = 0; i < producers.size(); i++) {
      BlockProducer blockProducer = producers.get(i);
      SemanticVersion producerVersion = blockProducer.getNodeVersion();
      boolean bpIsUpToDate =
          producerVersion != null && producerVersion.compareTo(minimumNodeVersion) >= 0;
      if (bpIsUpToDate) {
        upToDateProducers = upToDateProducers.setBit(i);
      }
    }
    return upToDateProducers;
  }

  private Bitmap findActiveProducers(AvlTree<BlockchainAddress, Bitmap> heartbeats) {
    int n = producers.size();
    Bitmap active = Bitmap.create(n);
    for (int i = 0; i < n; i++) {
      int producerIndex = n - i - 1;
      if (hasEnoughHeartbeats(heartbeats, producerIndex)) {
        active = active.setBit(producerIndex);
      }
    }
    return active;
  }

  private Bitmap attemptSelectionOfProducers(AvlTree<BlockchainAddress, Bitmap> heartbeats) {
    int threshold = 2 * getCorruptionThreshold() + 1;
    if (heartbeats.size() >= threshold) {
      return findActiveProducers(heartbeats);
    }
    return null;
  }

  private boolean hasEnoughHeartbeats(
      AvlTree<BlockchainAddress, Bitmap> heartbeats, int producerIndex) {
    int count = 0;
    for (Bitmap heartbeat : heartbeats.values()) {
      if (heartbeat.testBit(producerIndex)) {
        count++;
      }
    }
    return count > getCorruptionThreshold();
  }

  /**
   * Get voters.
   *
   * @return voters as array of bits
   */
  public Bitmap getVoters() {
    return voters;
  }

  /**
   * Get threshold key.
   *
   * @return threshold key
   */
  public ThresholdKey getThresholdKey() {
    return thresholdKey;
  }

  /**
   * Get the current retry nonce.
   *
   * @return retry nonce.
   */
  public int getRetryNonce() {
    return retryNonce;
  }

  /**
   * Get all candidate keys.
   *
   * @return list of candidate key states
   */
  public List<CandidateKey> getCandidates() {
    return new ArrayList<>(Objects.requireNonNull(candidates));
  }

  /**
   * Get the heartbeat published for a particular producer.
   *
   * @param address the address of the producer
   * @return the heartbeat published by a producer.
   */
  public Bitmap getHeartbeat(BlockchainAddress address) {
    return heartbeats.getValue(address);
  }

  /**
   * Add a proposal from one of the new oracle members as to what the new oracle public key should
   * look like. When proposals have been received from a majority of the producers, then the
   * threshold public key of this new oracle is set to the majority proposal.
   *
   * @param from the proposer
   * @param key the key
   * @param honestParties the parties who contributed to this key
   * @param signatureRandomization the random hash
   * @return the update with the new candidate.
   */
  public LargeOracleUpdate addCandidateKey(
      BlockchainAddress from, ThresholdKey key, Bitmap honestParties, Hash signatureRandomization) {
    int blockProducerIndex = getActiveBlockProducerIndex(from);

    if (blockProducerIndex == -1) {
      throw new IllegalStateException("Unknown producer " + from);
    }

    if (hasVoted(blockProducerIndex)) {
      throw new IllegalStateException("Proposer " + from + " already submitted a key");
    }

    // add vote for the proposed key
    FixedList<CandidateKey> candidates = Objects.requireNonNull(this.candidates);
    int candidateIndex = getCandidateIndex(key);
    if (candidateIndex == -1) {
      candidates = candidates.addElement(new CandidateKey(key, 1));
    } else {
      CandidateKey candidate = candidates.get(candidateIndex);
      candidates = candidates.setElement(candidateIndex, candidate.addVote());
    }
    AvlTree<BlockchainAddress, Bitmap> updatedHonestParties =
        honestPartyViews.set(from, honestParties);

    // register the voter as having voted.
    Bitmap updated = voters.setBit(blockProducerIndex);
    if (hasEnoughParticipants(updated.popCount(), getActiveProducers().size())) {
      ThresholdKey majorityKey = findMajorityKey(candidates);
      logger.info("Incoming oracle confirmed threshold key: " + majorityKey);
      return new LargeOracleUpdate(
          producers,
          majorityKey,
          candidates,
          updated,
          heartbeats,
          activeProducers,
          retryNonce,
          broadcasters,
          broadcastMessages,
          updatedHonestParties,
          signatureRandomization);
    } else {
      return new LargeOracleUpdate(
          producers,
          null,
          candidates,
          updated,
          heartbeats,
          activeProducers,
          retryNonce,
          broadcasters,
          broadcastMessages,
          updatedHonestParties,
          null);
    }
  }

  private int getCandidateIndex(ThresholdKey key) {
    int index = 0;
    for (CandidateKey candidate : getCandidates()) {
      if (candidate.getThresholdKey().equals(key)) {
        return index;
      }
      index++;
    }
    return -1;
  }

  /**
   * Finds the majority among a set of keys. If there's a tie, then the first candidate is returned.
   *
   * @param finalCandidates the candidates to search through
   * @return the key with the highest amount of votes.
   */
  static ThresholdKey findMajorityKey(FixedList<CandidateKey> finalCandidates) {
    int majorityVote = 0;
    ThresholdKey thresholdKey = null;
    for (CandidateKey candidate : finalCandidates) {
      int votes = candidate.getVotes();
      if (votes > majorityVote) {
        majorityVote = votes;
        thresholdKey = candidate.getThresholdKey();
      }
    }
    // guaranteed to not be null.
    return thresholdKey;
  }

  boolean hasVoted(int blockProducerIndex) {
    return Objects.requireNonNull(voters).testBit(blockProducerIndex);
  }

  int getActiveBlockProducerIndex(BlockchainAddress address) {
    return getIndexOf(address, getActiveProducers());
  }

  int getBlockProducerIndex(BlockchainAddress address) {
    return getIndexOf(address, getProducers());
  }

  static int getIndexOf(BlockchainAddress address, List<BlockProducer> list) {
    int index = 0;
    for (BlockProducer producer : list) {
      if (producer.getIdentity().equals(address)) {
        return index;
      }
      index++;
    }
    return -1;
  }

  /**
   * Write a public key as an uncompressed point without any additional encoding information.
   * Bouncycastle will include the byte 0x04 when serializing uncompressed points. This method
   * throws away that extra byte.
   *
   * @param publicKey the publicKey to be serialized
   * @return exactly 64 bytes representing an uncompressed point.
   */
  private static byte[] getEncodedWithoutHeader(BlockchainPublicKey publicKey) {
    byte[] encoded = publicKey.getEcPointBytes();
    byte[] headless = new byte[encoded.length - 1];
    System.arraycopy(encoded, 1, headless, 0, headless.length);
    return headless;
  }

  /**
   * Create the hash that current oracle needs to sign in order to validate an incoming oracle.
   *
   * @param nextSessionId the identifier for the next session
   * @param domainSeparator a domain separator
   * @return a hash to be signed by the large oracle.
   */
  public Hash getMessageToBeSigned(int nextSessionId, Hash domainSeparator) {
    Hash merkleRootOfActiveProducers = getMerkleRootOfActiveProducers();
    return Hash.create(
        s -> {
          domainSeparator.write(s);
          s.writeLong(nextSessionId);
          s.writeInt(getActiveProducers().size());
          merkleRootOfActiveProducers.write(s);
          s.write(getEncodedWithoutHeader(getThresholdKey().getKey()));
        });
  }

  /**
   * Add a broadcast message hash from a sender.
   *
   * @param sender the sender
   * @param broadcastHash the hash
   * @return updated state.
   */
  public LargeOracleUpdate addBroadcast(BlockchainAddress sender, Hash broadcastHash) {
    int index = getCurrentBroadcastRound() - 1;
    BroadcastTracker broadcastTracker = broadcastMessages.get(index);
    BroadcastTracker updated =
        broadcastTracker.addMessage(sender, broadcastHash, activeProducers.popCount());
    return new LargeOracleUpdate(
        producers,
        thresholdKey,
        candidates,
        voters,
        heartbeats,
        activeProducers,
        retryNonce,
        broadcasters,
        broadcastMessages.setElement(index, updated),
        honestPartyViews,
        signatureRandomization);
  }

  /**
   * Update the bitmap of a producer indicating which broadcast messages the producer has.
   *
   * @param keeper the producer
   * @param bits the bitmap
   * @return updated state.
   */
  public LargeOracleUpdate updateBroadcastBits(BlockchainAddress keeper, byte[] bits) {
    int index = getCurrentBroadcastRound() - 1;
    BroadcastTracker broadcastTracker = broadcastMessages.get(index);
    List<BlockchainAddress> activeProducerIdentities =
        getActiveProducers().stream().map(BlockProducer::getIdentity).toList();
    broadcastTracker = broadcastTracker.updateBitmap(keeper, bits, activeProducerIdentities);

    FixedList<BroadcastTracker> updated = broadcastMessages.setElement(index, broadcastTracker);
    return new LargeOracleUpdate(
        producers,
        thresholdKey,
        candidates,
        voters,
        heartbeats,
        activeProducers,
        retryNonce,
        broadcasters,
        updated,
        honestPartyViews,
        signatureRandomization);
  }

  /**
   * Advance the update to the next broadcast round.
   *
   * @param time the start time of the next round
   * @return update that has advanced to the next broadcast round.
   */
  public LargeOracleUpdate advanceBroadcastRound(long time) {
    BroadcastTracker tracker = getBroadcastTracker(getCurrentBroadcastRound());
    Bitmap echos = Bitmap.fromBytes(new byte[activeProducers.sizeInBytes()]);
    Bitmap broadcastersBitmap = tracker.getBroadcastersBitmap(getActiveProducers());
    return new LargeOracleUpdate(
        producers,
        thresholdKey,
        candidates,
        voters,
        heartbeats,
        activeProducers,
        retryNonce,
        broadcastersBitmap.and(broadcasters),
        broadcastMessages.addElement(BroadcastTracker.create(echos, time)),
        honestPartyViews,
        signatureRandomization);
  }

  boolean hasReceivedEnoughMessages() {
    BroadcastTracker broadcastTracker = getBroadcastTracker(getCurrentBroadcastRound());
    return hasEnoughBroadcastMessagesToAdvance(
        broadcastTracker, getActiveProducers(), broadcasters);
  }

  /**
   * Determine if it's possible to advance to the next broadcast round. Whether this is possible, is
   * determined by two factors: (1) whether there are at least 2.3t+1 messages that have been echoed
   * by at least t+1 parties, and (2) whether at least 2t+1 echos have been sent by distinct
   * parties.
   *
   * @param tracker the broadcast tracker
   * @param producers the list of parties
   * @param currentBroadcasters the broadcasters of the current round
   * @return true if the tracker has enough messages to advance to the next round, otherwise false.
   */
  static boolean hasEnoughBroadcastMessagesToAdvance(
      BroadcastTracker tracker, List<BlockProducer> producers, Bitmap currentBroadcasters) {
    Bitmap broadcastersBitmap = tracker.getBroadcastersBitmap(producers).and(currentBroadcasters);
    return hasEnoughParticipants(broadcastersBitmap.popCount(), producers.size())
        && hasEnoughEchoes(tracker, producers.size());
  }

  /**
   * Return true if we have received a broadcast message from everyone that was a broadcaster in the
   * previous round (or selected as active producer in case we're in the initial broadcast round).
   *
   * @return true if we have broadcast messages from everyone and false otherwise.
   */
  boolean hasBroadcastMessagesFromEveryone() {
    int currentBroadcastRound = getCurrentBroadcastRound();
    BroadcastTracker tracker = getBroadcastTracker(currentBroadcastRound);
    int currentNumberOfBroadcasters = getBroadcasters(currentBroadcastRound).size();
    return broadcasters.popCount() == currentNumberOfBroadcasters
        && hasEnoughEchoes(tracker, activeProducers.popCount());
  }

  /**
   * Checks if a broadcast tracker has more than 2t+1 echos on it.
   *
   * @param tracker the tracker
   * @param numberOfProducers the total number of producers, used to compute the threshold t
   * @return true if the number of echos is greater than 2t+1.
   */
  static boolean hasEnoughEchoes(BroadcastTracker tracker, int numberOfProducers) {
    int threshold = (numberOfProducers - 1) / 3;
    return tracker.getEchos().popCount() > 2 * threshold;
  }

  /**
   * Checks if the current broadcast round has been running for a specific amount of time.
   *
   * @param currentTime the current time
   * @param delta the amount of time that should have elapsed
   * @return true if the current broadcast round is more than delta time from currentTime.
   */
  boolean enoughTimeHasPassed(long currentTime, long delta) {
    int currentBroadcastRound = getCurrentBroadcastRound();
    long startTime = getBroadcastTracker(currentBroadcastRound).getStartTime();
    return currentTime - startTime >= delta;
  }

  /**
   * The threshold is set to 2.3t to ensure that we do not generate a key with only 2t+1
   * participants as this does not give us any room to lose producers afterwards and still be able
   * to generate signatures.
   *
   * @param participants the number of participants
   * @param producers the number of producers
   * @return threshold
   */
  static boolean hasEnoughParticipants(int participants, int producers) {
    int threshold = (producers - 1) / 3;
    int participantThreshold = 23 * threshold / 10;
    return participants > participantThreshold;
  }

  /**
   * Get the current broadcast round.
   *
   * @return the broadcast round.
   */
  public int getCurrentBroadcastRound() {
    return broadcastMessages.size();
  }

  /**
   * Get the broadcast tracker object for a particular round. Trackers are 1 indexed.
   *
   * @param roundNumber the round number
   * @return the round number.
   */
  public BroadcastTracker getBroadcastTracker(int roundNumber) {
    return broadcastMessages.get(roundNumber - 1);
  }

  private int getCorruptionThreshold() {
    // (n - 1)/3 is the amount of producers which are assumed to be dishonest
    return (producers.size() - 1) / 3;
  }
}
