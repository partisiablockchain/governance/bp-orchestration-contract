package com.partisiablockchain.governance.bporchestration;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.contract.StateSerializableEquality;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.serialization.StateAccessor;
import com.secata.tools.immutable.FixedList;
import java.math.BigInteger;
import java.util.List;
import org.junit.jupiter.api.Test;

/** Test. */
public final class CommitteeSignatureTest {

  @Test
  void fromStateAccessor() {
    KeyPair keyPair = new KeyPair(BigInteger.valueOf(123));
    Signature sign = keyPair.sign(Hash.create(s -> s.writeInt(444)));
    List<BlockProducer> producers = BpOrchestrationContractTest.createConfirmedProducers(4);
    CommitteeSignature committeeSignature =
        CommitteeSignature.create(FixedList.create(producers), sign, keyPair.getPublic());
    CommitteeSignature fromStateAccessor =
        CommitteeSignature.createFromStateAccessor(StateAccessor.create(committeeSignature));
    StateSerializableEquality.assertStatesEqual(fromStateAccessor, committeeSignature);
  }
}
