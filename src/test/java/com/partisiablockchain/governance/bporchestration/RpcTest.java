package com.partisiablockchain.governance.bporchestration;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.Hash;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

final class RpcTest {

  @Test
  void accountPlugin_associate() {
    BlockchainAddress contractAddress =
        BlockchainAddress.fromString("000004000000000000000000000000000000000001");

    byte[] rpc = AccountPluginRpc.associate(contractAddress);

    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    Assertions.assertThat(stream.readUnsignedByte())
        .isEqualTo(AccountPluginRpc.ASSOCIATE_INVOCATION_BYTE);
    Assertions.assertThat(stream.readLong())
        .isEqualTo(AccountPluginRpc.REGISTER_BP_ASSOCIATED_TOKENS);
    Assertions.assertThat(BlockchainAddress.read(stream)).isEqualTo(contractAddress);
  }

  @Test
  void accountPlugin_disassociate() {
    BlockchainAddress contractAddress =
        BlockchainAddress.fromString("000004000000000000000000000000000000000001");

    byte[] rpc = AccountPluginRpc.disassociate(contractAddress);

    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    Assertions.assertThat(stream.readUnsignedByte())
        .isEqualTo(AccountPluginRpc.DISASSOCIATE_ALL_TOKENS_FOR_CONTRACT_INVOCATION_BYTE);

    Assertions.assertThat(BlockchainAddress.read(stream)).isEqualTo(contractAddress);
  }

  @Test
  void feeDistribution_updateCommittee() {
    BlockchainAddress accountA =
        BlockchainAddress.fromString("000000000000000000000000000000000000000001");
    BlockchainAddress accountB =
        BlockchainAddress.fromString("000000000000000000000000000000000000000002");
    List<BlockchainAddress> committee = List.of(accountA, accountB);

    byte[] rpc = SafeDataOutputStream.serialize(FeeDistributionRpc.updateCommittee(committee));

    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    Assertions.assertThat(stream.readUnsignedByte())
        .isEqualTo(FeeDistributionRpc.UPDATE_COMMITTEE_INVOCATION_BYTE);
    Assertions.assertThat(BlockchainAddress.LIST_SERIALIZER.readDynamic(stream))
        .isEqualTo(committee);
  }

  @Test
  void globalConsensus_setCommittee() {
    BlockProducer producerA =
        BpOrchestrationContractTest.createProducer(
            "A", BlockProducer.BlockProducerStatus.PENDING_UPDATE);
    BlockProducer producerB =
        BpOrchestrationContractTest.createProducer(
            "B", BlockProducer.BlockProducerStatus.CONFIRMED);
    List<BlockProducer> producers = List.of(producerA, producerB);
    List<BlockchainAddress> producerAddresses =
        List.of(producerA.getIdentity(), producerB.getIdentity());

    Hash hashId =
        Hash.create(
            hash -> {
              hash.writeInt(0);
              BlockchainAddress.LIST_SERIALIZER.writeDynamic(hash, producerAddresses);
            });
    byte[] rpc = ConsensusPluginRpc.setCommittee(hashId, producers);

    Hash expectedHash =
        Hash.create(
            hash -> {
              hash.writeInt(0);
              BlockchainAddress.LIST_SERIALIZER.writeDynamic(hash, producerAddresses);
            });
    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    Assertions.assertThat(stream.readUnsignedByte())
        .isEqualTo(ConsensusPluginRpc.SET_COMMITTEE_INVOCATION_BYTE);
    Assertions.assertThat(Hash.read(stream)).isEqualTo(expectedHash);
    Assertions.assertThat(stream.readInt()).isEqualTo(producerAddresses.size());
    for (BlockProducer bp : producers) {
      Assertions.assertThat(BlockchainAddress.read(stream)).isEqualTo(bp.getIdentity());
      Assertions.assertThat(BlockchainPublicKey.read(stream)).isEqualTo(bp.getPublicKey());
      Assertions.assertThat(BlsPublicKey.read(stream)).isEqualTo(bp.getBlsPublicKey());
    }
  }

  @Test
  void registerCallback() {
    BlockProducer producerA =
        BpOrchestrationContractTest.createProducer("A", BlockProducer.BlockProducerStatus.PENDING);

    byte[] rpc =
        SafeDataOutputStream.serialize(
            BpOrchestrationContract.Callbacks.registerCallback(producerA));

    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    Assertions.assertThat(stream.readUnsignedByte())
        .isEqualTo((byte) BpOrchestrationContract.Callbacks.REGISTER);
    Assertions.assertThat(BpOrchestrationContractTest.readBlockproducer(stream))
        .isEqualTo(producerA);
  }

  @Test
  void registerAndConfirm() {
    BlockProducer producerA =
        BpOrchestrationContractTest.createProducer("A", BlockProducer.BlockProducerStatus.PENDING);
    byte[] rpc =
        SafeDataOutputStream.serialize(
            BpOrchestrationContract.Callbacks.registerAndConfirm(producerA));

    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    Assertions.assertThat(stream.readUnsignedByte())
        .isEqualTo((byte) BpOrchestrationContract.Callbacks.REGISTER_AND_CONFIRM);
    Assertions.assertThat(BpOrchestrationContractTest.readBlockproducer(stream))
        .isEqualTo(producerA);
  }

  @Test
  void largeOracle_replaceLargeOracle() {
    BlockProducer producerA =
        BpOrchestrationContractTest.createProducer(
            "A", BlockProducer.BlockProducerStatus.PENDING_UPDATE);
    BlockProducer producerB =
        BpOrchestrationContractTest.createProducer(
            "B", BlockProducer.BlockProducerStatus.CONFIRMED);
    List<BlockProducer> producers = List.of(producerA, producerB);

    byte[] rpc =
        SafeDataOutputStream.serialize(
            LargeOracleRpc.replaceLargeOracle(producers, producerA.getPublicKey()));

    List<BlockchainPublicKey> expectedPublicKeys =
        List.of(producerA.getPublicKey(), producerB.getPublicKey());
    List<BlockchainAddress> expectedAddresses =
        List.of(producerA.getIdentity(), producerB.getIdentity());
    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    Assertions.assertThat(stream.readUnsignedByte())
        .isEqualTo(LargeOracleRpc.REPLACE_LARGE_ORACLE_INVOCATION_BYTE);
    Assertions.assertThat(BlockchainPublicKey.LIST_SERIALIZER.readDynamic(stream))
        .isEqualTo(expectedPublicKeys);
    Assertions.assertThat(BlockchainAddress.LIST_SERIALIZER.readDynamic(stream))
        .isEqualTo(expectedAddresses);
    Assertions.assertThat(BlockchainPublicKey.read(stream)).isEqualTo(producerA.getPublicKey());
  }

  @Test
  void voting_receiveCommitteeInformation() {
    BlockProducer producerA =
        BpOrchestrationContractTest.createProducer(
            "A", BlockProducer.BlockProducerStatus.PENDING_UPDATE);
    BlockProducer producerB =
        BpOrchestrationContractTest.createProducer(
            "B", BlockProducer.BlockProducerStatus.CONFIRMED);
    List<BlockProducer> committee = List.of(producerA, producerB);

    byte[] rpc = SafeDataOutputStream.serialize(VotingRpc.receiveCommitteeInformation(committee));
    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    Assertions.assertThat(stream.readUnsignedByte())
        .isEqualTo(VotingRpc.RECEIVE_COMMITTEE_INFORMATION_INVOCATION_BYTE);
    Assertions.assertThat(stream.readInt()).isEqualTo(committee.size());
    for (BlockProducer bp : committee) {
      Assertions.assertThat(BlockchainAddress.read(stream)).isEqualTo(bp.getIdentity());
      Assertions.assertThat(stream.readLong()).isEqualTo(bp.getNumberOfVotes());
    }
  }
}
