package com.partisiablockchain.governance.bporchestration;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

final class SemanticVersionTest {
  /**
   * Version is considered greater than another if its major, minor, or patch version is higher in
   * that order of precedence.
   */
  @Test
  void compareToIsGreater() {
    SemanticVersion version = new SemanticVersion(2, 2, 2);
    assertThat(version.compareTo(new SemanticVersion(1, 1, 1)) > 0).isTrue();
    assertThat(version.compareTo(new SemanticVersion(1, 1, 2)) > 0).isTrue();
    assertThat(version.compareTo(new SemanticVersion(1, 1, 3)) > 0).isTrue();
    assertThat(version.compareTo(new SemanticVersion(1, 2, 1)) > 0).isTrue();
    assertThat(version.compareTo(new SemanticVersion(1, 2, 2)) > 0).isTrue();
    assertThat(version.compareTo(new SemanticVersion(1, 2, 3)) > 0).isTrue();
    assertThat(version.compareTo(new SemanticVersion(1, 3, 1)) > 0).isTrue();
    assertThat(version.compareTo(new SemanticVersion(1, 3, 2)) > 0).isTrue();
    assertThat(version.compareTo(new SemanticVersion(1, 3, 3)) > 0).isTrue();
    assertThat(version.compareTo(new SemanticVersion(2, 1, 1)) > 0).isTrue();
    assertThat(version.compareTo(new SemanticVersion(2, 1, 2)) > 0).isTrue();
    assertThat(version.compareTo(new SemanticVersion(2, 1, 3)) > 0).isTrue();
    assertThat(version.compareTo(new SemanticVersion(2, 2, 1)) > 0).isTrue();
  }

  /**
   * Version is considered less than another if the another versions major, minor, or patch version
   * is higher in that order of precedence.
   */
  @Test
  void compareToIsLess() {
    SemanticVersion version = new SemanticVersion(2, 2, 2);
    assertThat(version.compareTo(new SemanticVersion(2, 2, 3)) < 0).isTrue();
    assertThat(version.compareTo(new SemanticVersion(2, 3, 1)) < 0).isTrue();
    assertThat(version.compareTo(new SemanticVersion(2, 3, 2)) < 0).isTrue();
    assertThat(version.compareTo(new SemanticVersion(2, 3, 3)) < 0).isTrue();
    assertThat(version.compareTo(new SemanticVersion(3, 1, 1)) < 0).isTrue();
    assertThat(version.compareTo(new SemanticVersion(3, 1, 2)) < 0).isTrue();
    assertThat(version.compareTo(new SemanticVersion(3, 1, 3)) < 0).isTrue();
    assertThat(version.compareTo(new SemanticVersion(3, 2, 1)) < 0).isTrue();
    assertThat(version.compareTo(new SemanticVersion(3, 2, 2)) < 0).isTrue();
    assertThat(version.compareTo(new SemanticVersion(3, 2, 3)) < 0).isTrue();
    assertThat(version.compareTo(new SemanticVersion(3, 3, 1)) < 0).isTrue();
    assertThat(version.compareTo(new SemanticVersion(3, 3, 2)) < 0).isTrue();
    assertThat(version.compareTo(new SemanticVersion(3, 3, 3)) < 0).isTrue();
  }

  /** Versions are considered equal if major, minor and patch is the same. */
  @Test
  void compareToIsEqual() {
    SemanticVersion version = new SemanticVersion(2, 2, 2);
    assertThat(version.compareTo(new SemanticVersion(2, 2, 2)) == 0).isTrue();
  }
}
