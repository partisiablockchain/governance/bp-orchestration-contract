package com.partisiablockchain.governance.bporchestration;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.serialization.StateAccessor;
import java.util.List;
import nl.jqno.equalsverifier.EqualsVerifier;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class BitmapTest {

  @Test
  void noUnnecessaryTrailingBytes() {
    Assertions.assertThat(Bitmap.create(0).sizeInBytes()).isEqualTo(1);
    Assertions.assertThat(Bitmap.create(7).sizeInBytes()).isEqualTo(1);
    Assertions.assertThat(Bitmap.create(8).sizeInBytes()).isEqualTo(1);
    Assertions.assertThat(Bitmap.create(9).sizeInBytes()).isEqualTo(2);
  }

  @Test
  void equalsTest() {
    EqualsVerifier.forClass(Bitmap.class).withNonnullFields("bits").verify();
  }

  @Test
  void toStringTest() {
    Bitmap bitmap = Bitmap.fromBytes(new byte[] {0b1101, 0b110011});
    Assertions.assertThat(bitmap.toString()).isEqualTo("1011000011001100");
  }

  @Test
  void fromNullStateAccessor() {
    LargeOracleUpdate update = LargeOracleUpdate.create(List.of());
    StateAccessor stateAccessor = StateAccessor.create(update);
    Bitmap fromStateAccessor = Bitmap.createFromStateAccessor(stateAccessor.get("activeProducers"));
    Assertions.assertThat(fromStateAccessor).isNull();
  }

  @Test
  void allOnes() {
    Bitmap bitmap = Bitmap.allOnes(3);
    Assertions.assertThat(bitmap.testBit(0)).isTrue();
    Assertions.assertThat(bitmap.testBit(1)).isTrue();
    Assertions.assertThat(bitmap.testBit(2)).isTrue();
    Assertions.assertThat(bitmap.testBit(3)).isFalse();
    Assertions.assertThat(bitmap.testBit(4)).isFalse();
    Assertions.assertThat(bitmap.testBit(5)).isFalse();
    Assertions.assertThat(bitmap.testBit(6)).isFalse();
    Assertions.assertThat(bitmap.testBit(7)).isFalse();
  }
}
