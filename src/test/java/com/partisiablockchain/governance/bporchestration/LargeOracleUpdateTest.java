package com.partisiablockchain.governance.bporchestration;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.contract.StateSerializableEquality;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.tree.AvlTree;
import com.secata.tools.immutable.FixedList;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import org.assertj.core.api.Assertions;
import org.bouncycastle.util.encoders.Base64;
import org.junit.jupiter.api.Test;

/** Test. */
public final class LargeOracleUpdateTest {

  List<BlockProducer> producers = BpOrchestrationContractTest.createConfirmedProducers(4);

  static FixedList<BroadcastTracker> getEmptyBroadcastList() {
    return FixedList.create();
  }

  private Bitmap producerAddresses() {
    return Bitmap.allOnes(producers.size());
  }

  @Test
  public void votingOnSameKeyButDifferentKeyId() {
    LargeOracleUpdate largeOracleUpdate = LargeOracleUpdate.create(producers);
    BlockchainPublicKey key = new KeyPair(BigInteger.TEN).getPublic();
    largeOracleUpdate = markAllActive(largeOracleUpdate, producers);
    String keyId = "keyId";
    Bitmap honestParties = producerAddresses();
    LargeOracleUpdate oneCandidateKeyOracleUpdate =
        largeOracleUpdate.addCandidateKey(
            producers.get(0).getIdentity(), new ThresholdKey(key, keyId), honestParties, null);
    LargeOracleUpdate differentKeyIdOracleUpdate =
        oneCandidateKeyOracleUpdate.addCandidateKey(
            producers.get(1).getIdentity(), new ThresholdKey(key, "notKeyId"), honestParties, null);
    Assertions.assertThat(differentKeyIdOracleUpdate.getThresholdKey()).isNull();
    Assertions.assertThat(differentKeyIdOracleUpdate.getCandidates()).hasSize(2);
    LargeOracleUpdate twoCandidateKeysOracleUpdate =
        differentKeyIdOracleUpdate.addCandidateKey(
            producers.get(2).getIdentity(), new ThresholdKey(key, keyId), honestParties, null);
    Assertions.assertThat(twoCandidateKeysOracleUpdate.getThresholdKey().getKey()).isEqualTo(key);
    Assertions.assertThat(twoCandidateKeysOracleUpdate.getThresholdKey().getKeyId())
        .isEqualTo(keyId);
  }

  @Test
  void votingOnSameKeyButDifferentSetOfHonestParties() {
    LargeOracleUpdate oracleUpdate = LargeOracleUpdate.create(producers);
    BlockchainPublicKey key = new KeyPair(BigInteger.valueOf(123)).getPublic();
    String keyId = "keyId";
    ThresholdKey thresholdKey = new ThresholdKey(key, keyId);

    LargeOracleUpdate update = markAllActive(oracleUpdate, producers);
    Bitmap allParties = producerAddresses();
    Hash message = Hash.create(s -> s.writeString("123"));
    update =
        update.addCandidateKey(producers.get(0).getIdentity(), thresholdKey, allParties, message);
    Bitmap withoutFirst = unsetBit(allParties, 0);
    update =
        update.addCandidateKey(producers.get(1).getIdentity(), thresholdKey, withoutFirst, message);
    Assertions.assertThat(update.getThresholdKey()).isNull();
    Assertions.assertThat(update.getCandidates()).hasSize(1);
    update =
        update.addCandidateKey(producers.get(2).getIdentity(), thresholdKey, allParties, message);
    Assertions.assertThat(update.getThresholdKey()).isNotNull();
    ThresholdKey oracleKey = update.getThresholdKey();
    Assertions.assertThat(oracleKey.getKey()).isEqualTo(key);
    Assertions.assertThat(oracleKey.getKeyId()).isEqualTo(keyId);
  }

  private Bitmap unsetBit(Bitmap bitmap, int index) {
    byte[] bytes = bitmap.asBytes();
    int blockIndex = index / 8;
    int blockOffset = index % 8;
    bytes[index] = (byte) (bytes[blockIndex] & ~(1 << blockOffset));
    return Bitmap.fromBytes(bytes);
  }

  @Test
  public void addCandidateKey() {
    LargeOracleUpdate largeOracleUpdate = LargeOracleUpdate.create(producers);
    BlockchainAddress invalid =
        BlockchainAddress.fromString("000000000000000000000000000000000000000001");
    // alternative form of testing for exceptions here to silence SpotBugs.
    LargeOracleUpdate update = markAllActive(largeOracleUpdate, producers);
    try {
      LargeOracleUpdate ignored =
          update.addCandidateKey(invalid, new ThresholdKey(null, ""), producerAddresses(), null);
      Assertions.assertThat(ignored).isNull();
    } catch (IllegalStateException e) {
      Assertions.assertThat(e.getMessage()).isEqualTo("Unknown producer " + invalid);
    }
  }

  @Test
  public void producerCannotAddProposalMultipleTimes() {
    LargeOracleUpdate update = LargeOracleUpdate.create(producers);
    update = markAllActive(update, producers);
    // the key isn't checked, so it's fine to just add something bogus
    BlockchainAddress producer = producers.get(0).getIdentity();
    Bitmap honestParties = producerAddresses();
    LargeOracleUpdate largeOracleUpdate =
        update.addCandidateKey(producer, new ThresholdKey(null, ""), honestParties, null);
    try {
      LargeOracleUpdate ignored =
          largeOracleUpdate.addCandidateKey(
              producer, new ThresholdKey(null, ""), honestParties, null);
      Assertions.assertThat(ignored).isNull();
    } catch (IllegalStateException e) {
      Assertions.assertThat(e.getMessage())
          .isEqualTo("Proposer " + producer + " already submitted a key");
    }
  }

  static LargeOracleUpdate markAllActive(LargeOracleUpdate update, List<BlockProducer> producers) {
    byte[] bitmap = createAllOnesBitmap(producers);
    for (BlockProducer producer : producers) {
      update = update.addHeartbeat(producer.getIdentity(), bitmap, 0, null);
    }
    return update;
  }

  private LargeOracleUpdate markFirstActive(
      int toMark, LargeOracleUpdate update, List<BlockProducer> producers) {
    Bitmap bitmap = Bitmap.create(producers.size());
    for (int i = 0; i < toMark; i++) {
      bitmap = bitmap.setBit(i);
    }
    for (BlockProducer producer : producers) {
      update = update.addHeartbeat(producer.getIdentity(), bitmap.asBytes(), 0, null);
    }
    return update;
  }

  static byte[] createAllOnesBitmap(List<BlockProducer> producers) {
    int n = producers.size();
    int m = n / 8 + 1;
    byte[] bitmap = new byte[m];
    for (int i = 0; i < m; i++) {
      bitmap[i] = -1;
    }
    bitmap[bitmap.length - 1] = (byte) ((1 << n % 8) - 1);
    return bitmap;
  }

  @Test
  public void findMajorityWithLargeAmountOfProducers() {
    BlockchainPublicKey good = new KeyPair(BigInteger.TEN).getPublic();
    BlockchainPublicKey bad = new KeyPair(BigInteger.TWO).getPublic();
    findMajority(
        List.of(
            good, good, good, good, good, good, bad, good, good, good, good, good, good, good, good,
            good, good, good, good, good, good),
        good,
        30);
  }

  @Test
  public void findMajority() {
    BlockchainPublicKey good = new KeyPair(BigInteger.TEN).getPublic();
    BlockchainPublicKey bad = new KeyPair(BigInteger.TWO).getPublic();
    findMajority(List.of(bad, good, good), good, 4);
    findMajority(List.of(good, bad, good), good, 4);
    findMajority(List.of(good, good, bad), good, 4);
    findMajority(List.of(good, good, good), good, 4);
  }

  private void findMajority(
      List<BlockchainPublicKey> candidates, BlockchainPublicKey expected, int numberOfProducers) {
    List<BlockProducer> producers =
        BpOrchestrationContractTest.createConfirmedProducers(numberOfProducers);
    LargeOracleUpdate update = LargeOracleUpdate.create(producers);
    update = markAllActive(update, producers);
    for (int i = 0; i < candidates.size(); i++) {
      BlockProducer producer = producers.get(i);
      Assertions.assertThat(update.getThresholdKey()).isNull();
      update =
          update.addCandidateKey(
              producer.getIdentity(),
              new ThresholdKey(candidates.get(i), "candidates.get(i).toString()"),
              producerAddresses(),
              null);
    }
    Assertions.assertThat(update.getThresholdKey().getKey()).isEqualTo(expected);
    Assertions.assertThat(update.getVoters()).isNotNull();
    Assertions.assertThat(update.getVoters().sizeInBytes()).isEqualTo(producers.size() / 8 + 1);
  }

  private BlockchainPublicKey hexToKey(String hex) {
    // 04 is added to make the key decode correctly
    return BlockchainPublicKey.fromEncodedEcPoint(HexFormat.of().parseHex("04" + hex));
  }

  private List<BlockProducer> getProducersWithDifferentKeys(List<String> hexPublicKeys) {
    List<BlockProducer> updated = new ArrayList<>();
    for (int i = 0; i < hexPublicKeys.size(); i++) {
      BlockchainPublicKey ecPoint = hexToKey(hexPublicKeys.get(i));
      BlockProducer producer = producers.get(i);
      updated.add(
          new BlockProducer(
              "",
              "",
              "",
              producer.getIdentity(),
              ecPoint,
              producer.getBlsPublicKey(),
              0,
              0,
              0,
              producer.getStatus(),
              producer.getNodeVersion()));
    }
    return updated;
  }

  @Test
  void testBitWithSparseBits() {
    byte[] decode = Base64.decode("1rv9//fd/Pb3AA==");
    Bitmap bitmap = Bitmap.fromBytes(decode);
    Assertions.assertThat(bitmap.popCount()).isEqualTo(58);
  }

  @Test
  void manyProducersProducingManyHeartbeats() {
    byte[] heartbeat = Base64.decode("1rv9//fd//b3AA==");
    Bitmap heartbeatBitmap = Bitmap.fromBytes(heartbeat);
    List<BlockProducer> producers = BpOrchestrationContractTest.createConfirmedProducers(75);
    LargeOracleUpdate update = LargeOracleUpdate.create(producers);
    int producerIdx = 0;
    for (BlockProducer producer : producers) {
      // We only add a heartbeat if we ourselves are part of it. I.e., don't add heartbeats that
      // make us seem dead.
      if (heartbeatBitmap.testBit(producerIdx)) {
        update = update.addHeartbeat(producer.getIdentity(), heartbeat, 0, null);
      }
      producerIdx++;
    }
    Assertions.assertThat(update.getActiveProducers()).hasSize(60);
  }

  @Test
  void messageForSigningIsSameAsOnEth() {
    List<BlockProducer> referenceNodes =
        getProducersWithDifferentKeys(
            List.of(
                "94110db99c8b65f046b7bffb47cf64416be16faff279498af9dbf23cd996848573459cf3b7c9ecd7"
                    + "2bcd961e4630b3dda08f17472861cc0ec3afa7b95478589d",
                "ff35f4cd2e3afdced67ce0423667947dd419c3745a55dda2e879c34c919601dc67010ecb67f288ea"
                    + "17317e5446533e793b4e2d30d1ee594363408ea2a695a858",
                "e6d42c0f9357c1918f764cac9286b0e9b7131fb3dcc54cf33049a6b8f6f23f8ebfb922b8bf6feba6"
                    + "5e7b05e17152f083a5055300032b598f3133fc5f58ac611e",
                "a670c71063f0e6ab2808b7efe25de45e0fe644abc70af74f8b4d0f9a1c24a8e1db6e2fe1775a48e9"
                    + "5d635a9f3080427665f5804a72c6b9719e2adc5bd9e955fa"));
    ThresholdKey largeOracleKey =
        new ThresholdKey(
            hexToKey(
                "c1144469c993c1a0382e7e51d361d5844c764ebcdf3774c70721756668b08ac19eb717772fdbd7d6"
                    + "6c6cdc7a515d90ceec20bf2e4488a5365e61329cb69b6513"),
            "keyId");
    LargeOracleUpdate update =
        new LargeOracleUpdate(
            FixedList.create(referenceNodes),
            largeOracleKey,
            null,
            null,
            null,
            Bitmap.fromBytes(new byte[] {0b1111}),
            0,
            Bitmap.fromBytes(new byte[] {0b1111}),
            getEmptyBroadcastList(),
            AvlTree.create(),
            null);
    Hash domainSeparator =
        Hash.fromString("5f02a327a959a08bee16235910ad712c36507992cb310c9e064c96f27d92a3e7");
    // for ethereum, the extraData is the address of the large oracle contract on ethereum.
    Hash computed = update.getMessageToBeSigned(1, domainSeparator);
    String expected = "c678b45a327b6b8465fef43d06f679ebc73ba74d07f9223e19eff610b5eebcba";
    Assertions.assertThat(computed.toString()).isEqualTo(expected);
  }

  @Test
  public void findMajorityConditionals() {
    // this tests some edge-cases for the findMajority method, which in reality would never be hit
    // as they would violate our security model (and so, the output of findMajority is meaningless).
    BlockchainPublicKey point0 = new KeyPair(BigInteger.valueOf(123)).getPublic();
    BlockchainPublicKey point1 = new KeyPair(BigInteger.valueOf(456)).getPublic();
    FixedList<CandidateKey> candidates =
        FixedList.create(
            List.of(
                new CandidateKey(new ThresholdKey(point0, ""), 1),
                new CandidateKey(new ThresholdKey(point1, "2"), 1)));
    ThresholdKey thresholdKey = LargeOracleUpdate.findMajorityKey(candidates);
    Assertions.assertThat(thresholdKey.getKey()).isEqualTo(point0);
  }

  @Test
  public void hasVoted() {
    Bitmap votes = Bitmap.fromBytes(new byte[] {0b10});
    LargeOracleUpdate update =
        new LargeOracleUpdate(
            FixedList.create(producers),
            null,
            null,
            votes,
            null,
            null,
            0,
            null,
            getEmptyBroadcastList(),
            AvlTree.create(),
            null);
    // producer b has cast a vote
    Assertions.assertThat(update.hasVoted(1)).isTrue();
    // producer c did not vote
    Assertions.assertThat(update.hasVoted(2)).isFalse();
  }

  @Test
  public void attemptSelectionOfProducers() {
    LargeOracleUpdate update = LargeOracleUpdate.create(producers);
    update = update.addHeartbeat(producers.get(0).getIdentity(), new byte[] {0b1011}, 0, null);
    Assertions.assertThat(update.getActiveProducers()).isNull();
    Assertions.assertThat(update.getHeartbeat(producers.get(0).getIdentity()))
        .isEqualTo(Bitmap.fromBytes(new byte[] {0b1011}));
    update = update.addHeartbeat(producers.get(1).getIdentity(), new byte[] {0b1011}, 0, null);
    Assertions.assertThat(update.getActiveProducers()).isNull();
    Assertions.assertThat(update.getHeartbeat(producers.get(1).getIdentity()))
        .isEqualTo(Bitmap.fromBytes(new byte[] {0b1011}));
    Assertions.assertThat(update).isNotNull();
    update = update.addHeartbeat(producers.get(3).getIdentity(), new byte[] {0b1011}, 0, null);
    Assertions.assertThat(update.getActiveProducers())
        .containsExactlyInAnyOrder(producers.get(0), producers.get(1), producers.get(3));
  }

  @Test
  public void sevenPartiesVotes() {
    List<BlockProducer> producers = BpOrchestrationContractTest.createConfirmedProducers(7);
    LargeOracleUpdate update = LargeOracleUpdate.create(producers);
    update = update.addHeartbeat(producers.get(0).getIdentity(), new byte[] {0b1110110}, 0, null);
    update = update.addHeartbeat(producers.get(1).getIdentity(), new byte[] {0b1110110}, 0, null);
    update = update.addHeartbeat(producers.get(2).getIdentity(), new byte[] {0b1110110}, 0, null);
    update = update.addHeartbeat(producers.get(3).getIdentity(), new byte[] {0b1110110}, 0, null);
    update = update.addHeartbeat(producers.get(4).getIdentity(), new byte[] {0b1110110}, 0, null);
    Assertions.assertThat(update.getActiveProducers())
        .containsExactlyInAnyOrder(
            producers.get(1),
            producers.get(2),
            producers.get(4),
            producers.get(5),
            producers.get(6));
    Assertions.assertThat(update.getHeartbeat(producers.get(4).getIdentity()))
        .isEqualTo(Bitmap.fromBytes(new byte[] {0b1110110}));
  }

  @Test
  public void notEnoughVotes() {
    List<BlockProducer> producers = BpOrchestrationContractTest.createConfirmedProducers(7);
    LargeOracleUpdate update = LargeOracleUpdate.create(producers);
    update = update.addHeartbeat(producers.get(0).getIdentity(), new byte[] {0b1111}, 0, null);
    update = update.addHeartbeat(producers.get(1).getIdentity(), new byte[] {0b1111}, 0, null);
    update = update.addHeartbeat(producers.get(2).getIdentity(), new byte[] {0b1111}, 0, null);
    update = update.addHeartbeat(producers.get(3).getIdentity(), new byte[] {0b1111}, 0, null);
    update = update.addHeartbeat(producers.get(4).getIdentity(), new byte[] {0b1111}, 0, null);
    Assertions.assertThat(update.getActiveProducers()).isNull();
  }

  @Test
  public void elevenPartiesVotes() {
    byte[] bytes = {(byte) 0b11111110, (byte) 0b0000001};

    List<BlockProducer> producers = BpOrchestrationContractTest.createConfirmedProducers(11);
    LargeOracleUpdate update = LargeOracleUpdate.create(producers);
    for (int i = 0; i < 4; i++) {
      update = update.addHeartbeat(producers.get(i).getIdentity(), bytes, 0, null);
    }
    for (int i = 4; i < 7; i++) {
      update = update.addHeartbeat(producers.get(i).getIdentity(), new byte[] {1, 0}, 0, null);
    }
    Assertions.assertThat(update.getActiveProducers())
        .containsExactlyInAnyOrder(
            producers.get(1),
            producers.get(2),
            producers.get(3),
            producers.get(4),
            producers.get(5),
            producers.get(6),
            producers.get(7),
            producers.get(8));
  }

  @Test
  public void retryHeartbeats() {
    LargeOracleUpdate update = LargeOracleUpdate.create(producers);
    update = update.addHeartbeat(producers.get(0).getIdentity(), new byte[] {0b1}, 0, null);
    update = update.addHeartbeat(producers.get(1).getIdentity(), new byte[] {0b10}, 0, null);
    update = update.addHeartbeat(producers.get(2).getIdentity(), new byte[] {0b100}, 0, null);
    Assertions.assertThat(update.getActiveProducers()).isNull();
    update = update.addHeartbeat(producers.get(0).getIdentity(), new byte[] {0b1111}, 0, null);
    update = update.addHeartbeat(producers.get(2).getIdentity(), new byte[] {0b1111}, 0, null);
    update = update.addHeartbeat(producers.get(3).getIdentity(), new byte[] {0b1111}, 0, null);
    Assertions.assertThat(update.getActiveProducers())
        .containsExactlyInAnyOrderElementsOf(producers);
  }

  @Test
  public void selfishHeartbeats() {
    LargeOracleUpdate update = LargeOracleUpdate.create(producers);
    update = update.addHeartbeat(producers.get(0).getIdentity(), new byte[] {0b1}, 0, null);
    update = update.addHeartbeat(producers.get(1).getIdentity(), new byte[] {0b10}, 0, null);
    update = update.addHeartbeat(producers.get(2).getIdentity(), new byte[] {0b100}, 0, null);
    update = update.addHeartbeat(producers.get(3).getIdentity(), new byte[] {0b1000}, 0, null);
    Assertions.assertThat(update.getActiveProducers()).isNull();
  }

  @Test
  public void heartbeatsFromEveryone() {
    LargeOracleUpdate update = LargeOracleUpdate.create(producers);
    update = update.addHeartbeat(producers.get(0).getIdentity(), new byte[] {0b1111}, 0, null);
    update = update.addHeartbeat(producers.get(1).getIdentity(), new byte[] {0b1111}, 0, null);
    update = update.addHeartbeat(producers.get(2).getIdentity(), new byte[] {0b1111}, 0, null);
    // even though p3 hasn't said anything yet, everyone has heard from them, so they're
    // included.
    List<BlockProducer> activeProducers = update.getActiveProducers();
    Assertions.assertThat(activeProducers).isNotNull();
    Assertions.assertThat(activeProducers).containsExactlyInAnyOrderElementsOf(producers);
  }

  @Test
  public void addMalformedHeartbeat() {
    LargeOracleUpdate update =
        new LargeOracleUpdate(
            FixedList.create(producers),
            null,
            null,
            null,
            AvlTree.create(),
            null,
            0,
            null,
            getEmptyBroadcastList(),
            AvlTree.create(),
            null);
    // heartbeat is 2 bytes. Should only be 1.
    Assertions.assertThatThrownBy(() -> update.addHeartbeat(null, new byte[] {0b0, 0b0}, 0, null))
        .isInstanceOf(IllegalStateException.class)
        .hasMessage("Malformed heartbeat message");
    // heartbeat has a
    Assertions.assertThatThrownBy(() -> update.addHeartbeat(null, new byte[] {0b10000}, 0, null))
        .isInstanceOf(IllegalStateException.class)
        .hasMessage("Malformed heartbeat message");
  }

  @Test
  public void cannotAddMultipleHeartbeats() {
    LargeOracleUpdate update =
        new LargeOracleUpdate(
            FixedList.create(producers),
            null,
            null,
            null,
            AvlTree.create(),
            null,
            0,
            null,
            getEmptyBroadcastList(),
            AvlTree.create(),
            null);
    BlockchainAddress identity = producers.get(0).getIdentity();
    LargeOracleUpdate update1 = update.addHeartbeat(identity, new byte[] {0b1011}, 0, null);
    Assertions.assertThatThrownBy(
            () -> update1.addHeartbeat(identity, new byte[] {0b1101}, 0, null))
        .isInstanceOf(IllegalStateException.class)
        .hasMessage(identity + " has already supplied a heartbeat");
  }

  @Test
  void addBroadcastMessage() {
    LargeOracleUpdate update = markAllActive(LargeOracleUpdate.create(producers), producers);
    Assertions.assertThat(update.getCurrentBroadcastRound()).isEqualTo(1);
    BroadcastTracker broadcastTracker = update.getBroadcastTracker(1);
    Assertions.assertThat(broadcastTracker.getMessageCount()).isEqualTo(0);
    Hash message = Hash.create(s -> s.writeString("123"));
    BlockchainAddress producer = producers.get(0).getIdentity();
    LargeOracleUpdate oracleUpdate = update.addBroadcast(producer, message);
    broadcastTracker = oracleUpdate.getBroadcastTracker(1);
    Assertions.assertThat(broadcastTracker.getMessageCount()).isEqualTo(1);
    Assertions.assertThat(broadcastTracker.getMessage(producer)).isEqualTo(message);

    // same producer cannot add another message in this round
    Assertions.assertThatThrownBy(
            () ->
                oracleUpdate.addBroadcast(
                    producer, Hash.create(s -> s.writeString("another message"))))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage(producer + " has already sent a message");
  }

  @Test
  void willAdvanceWhenEnoughMessagesHaveBeenSeen() {
    LargeOracleUpdate update = markAllActive(LargeOracleUpdate.create(producers), producers);

    update =
        update.addBroadcast(producers.get(0).getIdentity(), Hash.create(s -> s.writeString("1")));
    Assertions.assertThat(update.getBroadcastTracker(1).getMessageCount()).isEqualTo(1);
    update = update.updateBroadcastBits(producers.get(0).getIdentity(), new byte[] {0b1});
    Assertions.assertThat(update.getCurrentBroadcastRound()).isEqualTo(1);
    update =
        update.addBroadcast(producers.get(1).getIdentity(), Hash.create(s -> s.writeString("2")));
    Assertions.assertThat(update.getBroadcastTracker(1).getMessageCount()).isEqualTo(2);
    update =
        update.addBroadcast(producers.get(2).getIdentity(), Hash.create(s -> s.writeString("3")));
    Assertions.assertThat(update.getBroadcastTracker(1).getMessageCount()).isEqualTo(3);
    update =
        update.addBroadcast(producers.get(3).getIdentity(), Hash.create(s -> s.writeString("4")));

    Assertions.assertThat(update.getCurrentBroadcastRound()).isEqualTo(1);
    Assertions.assertThat(update.getBroadcastTracker(1).getMessageCount()).isEqualTo(4);

    update = update.updateBroadcastBits(producers.get(0).getIdentity(), new byte[] {0b1111});
    update = update.updateBroadcastBits(producers.get(1).getIdentity(), new byte[] {0b1111});
    update = update.updateBroadcastBits(producers.get(3).getIdentity(), new byte[] {0b1111});
    update = update.advanceBroadcastRound(0);

    Assertions.assertThat(update.getCurrentBroadcastRound()).isEqualTo(2);
    Assertions.assertThat(update.getBroadcasters(1)).isEqualTo(producers);
    Assertions.assertThat(update.getBroadcasters(2)).isEmpty();

    // it's possible for parties to add new messages now
    update =
        update.addBroadcast(producers.get(0).getIdentity(), Hash.create(s -> s.writeString("123")));
    Assertions.assertThat(update.getBroadcastTracker(2).getMessageCount()).isEqualTo(1);
  }

  @Test
  void getBroadcasterWithInactiveNodes() {
    List<BlockProducer> producers = BpOrchestrationContractTest.createConfirmedProducers(25);
    LargeOracleUpdate update = markFirstActive(20, LargeOracleUpdate.create(producers), producers);

    Assertions.assertThat(update.getActiveProducers()).isNotNull();

    for (int i = 0; i < 25; i++) {
      int finalI = i;
      update =
          update.addBroadcast(producers.get(i).getIdentity(), Hash.create(s -> s.writeInt(finalI)));
    }
    Assertions.assertThat(update.getBroadcasters(1)).isNotNull();
  }

  @Test
  void willProceedWithLessThanAllParties() {
    LargeOracleUpdate update = markAllActive(LargeOracleUpdate.create(producers), producers);
    for (BlockProducer producer : producers.subList(0, 3)) {
      BlockchainAddress identity = producer.getIdentity();
      update = update.addBroadcast(identity, Hash.create(identity::write));
    }

    update = update.updateBroadcastBits(producers.get(0).getIdentity(), new byte[] {0b111});
    Assertions.assertThat(update.hasReceivedEnoughMessages()).isFalse();
    update = update.updateBroadcastBits(producers.get(1).getIdentity(), new byte[] {0b111});
    Assertions.assertThat(update.hasReceivedEnoughMessages()).isFalse();
    Assertions.assertThat(update.getCurrentBroadcastRound()).isEqualTo(1);
    // the extra bit here is ignored since it points to a non-existing message.
    update = update.updateBroadcastBits(producers.get(2).getIdentity(), new byte[] {0b1111});
    Assertions.assertThat(update.hasReceivedEnoughMessages()).isTrue();
    update = update.advanceBroadcastRound(0);

    Assertions.assertThat(update.getCurrentBroadcastRound()).isEqualTo(2);
    Assertions.assertThat(update.getBroadcasters(1)).isEqualTo(producers.subList(0, 3));

    for (BlockProducer producer : producers) {
      BlockchainAddress identity = producer.getIdentity();
      update = update.addBroadcast(identity, Hash.create(identity::write));
    }

    update = update.updateBroadcastBits(producers.get(0).getIdentity(), new byte[] {0b1111});
    update = update.updateBroadcastBits(producers.get(1).getIdentity(), new byte[] {0b1111});
    update = update.updateBroadcastBits(producers.get(2).getIdentity(), new byte[] {0b1111});
    update = update.advanceBroadcastRound(0);

    Assertions.assertThat(update.getCurrentBroadcastRound()).isEqualTo(3);
    Assertions.assertThat(update.getBroadcasters(2)).isEqualTo(producers.subList(0, 3));
  }

  @Test
  void noBroadcastersWithoutActiveProducers() {
    LargeOracleUpdate oracleUpdate = LargeOracleUpdate.create(producers);
    Assertions.assertThat(oracleUpdate.getBroadcasters(1)).isNull();
  }

  @Test
  void calculateThreshold() {
    Assertions.assertThat(LargeOracleUpdate.hasEnoughParticipants(2, 4)).isFalse();
    Assertions.assertThat(LargeOracleUpdate.hasEnoughParticipants(3, 4)).isTrue();
  }

  @Test
  void onUpgrade() {
    ThresholdKey key = new ThresholdKey(producers.get(1).getPublicKey(), "123");
    List<CandidateKey> candidates = List.of(new CandidateKey(key, 2));
    Map<BlockchainAddress, Bitmap> heartbeats =
        Map.of(
            producers.get(0).getIdentity(), Bitmap.fromBytes(new byte[] {0b1111}),
            producers.get(1).getIdentity(), Bitmap.fromBytes(new byte[] {0b1111}),
            producers.get(2).getIdentity(), Bitmap.fromBytes(new byte[] {0b1111}));
    BroadcastTracker tracker = BroadcastTracker.create(Bitmap.create(4), 3);
    List<BroadcastTracker> trackers = List.of(tracker);
    Hash signatureRandomization = Hash.create(s -> s.writeInt(1234));
    LargeOracleUpdate update =
        new LargeOracleUpdate(
            FixedList.create(producers),
            key,
            FixedList.create(candidates),
            Bitmap.fromBytes(new byte[] {0b11}),
            AvlTree.create(heartbeats),
            Bitmap.fromBytes(new byte[] {0b1111}),
            3,
            Bitmap.fromBytes(new byte[] {0b1111}),
            FixedList.create(trackers),
            AvlTree.create(),
            signatureRandomization);
    StateAccessor accessor = StateAccessor.create(update);
    LargeOracleUpdate fromStateAccessorNullSafe =
        LargeOracleUpdate.createFromStateAccessorNullSafe(accessor);

    StateSerializableEquality.assertStatesEqual(update, fromStateAccessorNullSafe);
  }
}
