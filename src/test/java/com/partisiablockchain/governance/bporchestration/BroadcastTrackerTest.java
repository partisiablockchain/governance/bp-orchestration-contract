package com.partisiablockchain.governance.bporchestration;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.Hash;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class BroadcastTrackerTest {

  @Test
  void broadcasterBitmap() {
    List<BlockProducer> producers = BpOrchestrationContractTest.createConfirmedProducers(8);
    List<BlockchainAddress> idents = producers.stream().map(BlockProducer::getIdentity).toList();
    BroadcastTracker tracker = BroadcastTracker.create(Bitmap.create(8), 0);
    tracker = tracker.addMessage(idents.get(0), Hash.create(s -> s.writeString("0")), 8);
    tracker = tracker.addMessage(idents.get(1), Hash.create(s -> s.writeString("1")), 8);
    tracker = tracker.updateBitmap(idents.get(3), new byte[] {0b11, 0b0}, idents);
    tracker = tracker.updateBitmap(idents.get(4), new byte[] {0b11, 0b0}, idents);
    tracker = tracker.updateBitmap(idents.get(5), new byte[] {0b11, 0b0}, idents);
    tracker = tracker.updateBitmap(idents.get(6), new byte[] {0b11, 0b0}, idents);
    Bitmap currentBroadcasters = Bitmap.fromBytes(new byte[] {-1, 0});
    Assertions.assertThat(tracker.getBroadcastersBitmap(producers))
        .isEqualTo(Bitmap.fromBytes(new byte[] {0b11}));
    Assertions.assertThat(
            LargeOracleUpdate.hasEnoughBroadcastMessagesToAdvance(
                tracker, producers, currentBroadcasters))
        .isFalse();

    tracker = tracker.addMessage(idents.get(3), Hash.create(s -> s.writeString("3")), 8);
    tracker = tracker.addMessage(idents.get(4), Hash.create(s -> s.writeString("4")), 8);
    tracker = tracker.updateBitmap(idents.get(0), new byte[] {0b11000, 0b0}, idents);
    tracker = tracker.updateBitmap(idents.get(1), new byte[] {0b11000, 0b0}, idents);
    tracker = tracker.updateBitmap(idents.get(5), new byte[] {0b11000, 0b0}, idents);
    tracker = tracker.updateBitmap(idents.get(6), new byte[] {0b11000, 0b0}, idents);

    Assertions.assertThat(tracker.getBroadcastersBitmap(producers))
        .isEqualTo(Bitmap.fromBytes(new byte[] {0b11011}));
    Assertions.assertThat(
            LargeOracleUpdate.hasEnoughBroadcastMessagesToAdvance(
                tracker, producers, currentBroadcasters))
        .isFalse();

    tracker = tracker.addMessage(idents.get(2), Hash.create(s -> s.writeString("2")), 8);
    tracker = tracker.updateBitmap(idents.get(0), new byte[] {0b100, 0b0}, idents);
    tracker = tracker.updateBitmap(idents.get(1), new byte[] {0b100, 0b0}, idents);
    tracker = tracker.updateBitmap(idents.get(3), new byte[] {0b100, 0b0}, idents);
    tracker = tracker.updateBitmap(idents.get(4), new byte[] {0b100, 0b0}, idents);

    Assertions.assertThat(tracker.getBroadcastersBitmap(producers))
        .isEqualTo(Bitmap.fromBytes(new byte[] {0b11111}));
    Assertions.assertThat(
            LargeOracleUpdate.hasEnoughBroadcastMessagesToAdvance(
                tracker, producers, currentBroadcasters))
        .isTrue();
  }

  @Test
  void getMessageBitmap() {
    List<BlockProducer> producers = BpOrchestrationContractTest.createConfirmedProducers(1);
    BroadcastTracker broadcastTracker = BroadcastTracker.create(Bitmap.create(8), 0);
    Bitmap messageBitmap = broadcastTracker.getMessageBitmap(producers.get(0).getIdentity());
    Assertions.assertThat(messageBitmap).isNull();
  }

  @Test
  void bitmapsAreUpdatedCorrectly() {
    List<BlockProducer> producers = BpOrchestrationContractTest.createConfirmedProducers(9);
    BroadcastTracker tracker = BroadcastTracker.create(Bitmap.create(9), 0);
    BlockchainAddress sender = producers.get(0).getIdentity();
    tracker = tracker.addMessage(sender, Hash.create(s -> s.writeInt(123)), 9);
    List<BlockchainAddress> parties = producers.stream().map(BlockProducer::getIdentity).toList();
    tracker = tracker.updateBitmap(parties.get(1), new byte[] {1, 0}, parties);
    tracker = tracker.updateBitmap(parties.get(2), new byte[] {1, 0}, parties);
    Assertions.assertThat(tracker.getBroadcastersBitmap(producers))
        .isEqualTo(Bitmap.fromBytes(new byte[] {0, 0}));
    tracker = tracker.updateBitmap(parties.get(3), new byte[] {1, 0}, parties);
    //    tracker = tracker.updateBitmap(parties.get(4), new byte[] {1, 0}, parties);
    Assertions.assertThat(tracker.getBroadcastersBitmap(producers))
        .isEqualTo(Bitmap.fromBytes(new byte[] {1, 0}));
  }

  @Test
  void needsEnoughBroadcastersToAdvance() {
    List<BlockProducer> producers = BpOrchestrationContractTest.createConfirmedProducers(9);
    BroadcastTracker tracker = BroadcastTracker.create(Bitmap.create(9), 0);
    List<BlockchainAddress> parties = producers.stream().map(BlockProducer::getIdentity).toList();
    tracker = tracker.addMessage(parties.get(0), Hash.create(s -> s.writeInt(0)), 9);
    tracker = tracker.addMessage(parties.get(1), Hash.create(s -> s.writeInt(1)), 9);
    tracker = tracker.addMessage(parties.get(2), Hash.create(s -> s.writeInt(2)), 9);
    tracker = tracker.addMessage(parties.get(3), Hash.create(s -> s.writeInt(2)), 9);
    tracker = tracker.addMessage(parties.get(4), Hash.create(s -> s.writeInt(2)), 9);

    tracker = tracker.updateBitmap(parties.get(1), new byte[] {0b11111, 0b0}, parties);
    tracker = tracker.updateBitmap(parties.get(2), new byte[] {0b11111, 0b0}, parties);
    tracker = tracker.updateBitmap(parties.get(3), new byte[] {0b11111, 0b0}, parties);
    Bitmap currentBroadcasters = Bitmap.fromBytes(new byte[] {-1, 0b1});
    // enough messages, but not enough echos.
    Assertions.assertThat(
            LargeOracleUpdate.hasEnoughBroadcastMessagesToAdvance(
                tracker, producers, currentBroadcasters))
        .isFalse();
    tracker = tracker.updateBitmap(parties.get(4), new byte[] {0, 0}, parties);
    // still not enough echos
    Assertions.assertThat(
            LargeOracleUpdate.hasEnoughBroadcastMessagesToAdvance(
                tracker, producers, currentBroadcasters))
        .isFalse();
    tracker = tracker.updateBitmap(parties.get(5), new byte[] {0, 0}, parties);
    // enough echos now
    Assertions.assertThat(
            LargeOracleUpdate.hasEnoughBroadcastMessagesToAdvance(
                tracker, producers, currentBroadcasters))
        .isTrue();
  }
}
