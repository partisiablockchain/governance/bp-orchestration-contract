package com.partisiablockchain.governance.bporchestration;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static com.partisiablockchain.governance.bporchestration.BpOrchestrationContract.ONE_DAY_MILLISECONDS;
import static com.partisiablockchain.governance.bporchestration.BpOrchestrationContract.ONE_MONTH_MILLISECONDS;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.contract.ContractEventInteraction;
import com.partisiablockchain.contract.StateSerializableEquality;
import com.partisiablockchain.contract.SysContractContextTest;
import com.partisiablockchain.contract.SysContractSerialization;
import com.partisiablockchain.contract.sys.LocalPluginStateUpdate;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsKeyPair;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.tree.AvlTree;
import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.FunctionUtility;
import com.secata.tools.immutable.FixedList;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import org.assertj.core.api.Assertions;
import org.assertj.core.api.ThrowableAssert;
import org.junit.jupiter.api.Test;

/** Test. */
public final class BpOrchestrationContractTest {
  private final List<BlockchainAddress> kycAddresses =
      List.of(BlockchainAddress.fromString("000000000000000000000000000000000000000009"));
  private final BlockchainAddress firstAddress =
      BlockchainAddress.fromString("000000000000000000000000000000000000000001");
  private final BlockchainAddress systemUpdateContractAddress =
      createContractAddress(BlockchainAddress.Type.CONTRACT_GOV, "SystemUpdateContract");
  private final BlockchainAddress rewardsContractAddress =
      createContractAddress(BlockchainAddress.Type.CONTRACT_GOV, "RewardsContract");
  private final BlockchainAddress largeOracleContract =
      BlockchainAddress.fromString("000000000000000000000000000000000000000AAA");
  // Hash("Partisiablockchain")
  private static final Hash domainSeparator =
      Hash.fromString("5f02a327a959a08bee16235910ad712c36507992cb310c9e064c96f27d92a3e7");

  private final SysContractSerialization<BpOrchestrationContractState> serialization =
      new SysContractSerialization<>(
          BpOrchestrationContractInvoker.class, BpOrchestrationContractState.class);
  private SysContractContextTest context = new SysContractContextTest(1, 100, firstAddress);
  private List<BlockProducer> initialProducers = createConfirmedProducers(4);

  private final KeyPair thresholdSecretKey = new KeyPair(BigInteger.valueOf(42));
  private final BlockchainPublicKey thresholdKey = thresholdSecretKey.getPublic();
  private final String thresholdKeyId = "Magical identifier";
  private static final byte INVALID_INVOCATION_BYTE = 100;
  private final Signature signature =
      thresholdSecretKey.sign(Hash.create(s -> s.writeString("Dummy")));
  private final AvlTree<Integer, CommitteeSignature> initialCommitteeLog = AvlTree.create();

  private BpOrchestrationContractState getInitial(
      long broadcastRoundDelay, List<BlockProducer> producers) {
    return serialization.create(
        context,
        rpc -> {
          BlockchainAddress.LIST_SERIALIZER.writeDynamic(rpc, kycAddresses);
          BlockProducer.LIST_SERIALIZER.writeDynamic(rpc, producers);
          thresholdKey.write(rpc);
          rpc.writeString(thresholdKeyId);
          largeOracleContract.write(rpc);
          systemUpdateContractAddress.write(rpc);
          rewardsContractAddress.write(rpc);
          domainSeparator.write(rpc);
          rpc.writeLong(broadcastRoundDelay);
        });
  }

  private BpOrchestrationContractState getInitial(long broadcastRoundDelay) {
    return getInitial(broadcastRoundDelay, initialProducers);
  }

  private BpOrchestrationContractState getInitial() {
    return getInitial(0);
  }

  @Test
  public void onCreate() {
    BpOrchestrationContractState initial = getInitial();
    Assertions.assertThat(initial.getCommittee()).isEqualTo(initialProducers);
    Assertions.assertThat(initial.getThresholdKey().getKey()).isEqualTo(thresholdKey);
    Assertions.assertThat(initial.getLargeOracleContract()).isEqualTo(largeOracleContract);
    Assertions.assertThat(initial.getDomainSeparator()).isEqualTo(domainSeparator);
  }

  @Test
  public void invokeInvalidInvocation() {
    Assertions.assertThatThrownBy(
            () ->
                serialization.invoke(context, null, rpc -> rpc.writeByte(INVALID_INVOCATION_BYTE)))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Invalid shortname: 100");
  }

  @Test
  public void invokeRegister() {
    String identity = "registeringBp";
    Hash hash = Hash.create(s -> s.writeString(identity));
    SecureRandom secureRandom = new SecureRandom(hash.getBytes());
    KeyPair keyPair = new KeyPair(new BigInteger(32, secureRandom));
    BlsKeyPair blsKeyPair = new BlsKeyPair(new BigInteger(32, secureRandom));
    BlockProducer blockProducer =
        createProducerWithBls(
            identity, blsKeyPair, keyPair, BlockProducer.BlockProducerStatus.WAITING_FOR_CALLBACK);
    from(blockProducer.getIdentity());
    BpOrchestrationContractState state = getInitial();
    Hash message =
        state.createPopMessage(
            blockProducer.getIdentity(),
            blockProducer.getPublicKey(),
            blockProducer.getBlsPublicKey());
    BlsSignature signature = blsKeyPair.sign(message);

    BpOrchestrationContractState sameState =
        serialization.invoke(context, state, createRegisterRpc(blockProducer, signature));

    Assertions.assertThat(sameState).isNotNull();

    Assertions.assertThat(context.getRemoteCalls().callbackCost).isNull();
    byte[] registerCallbackRpc =
        SafeDataOutputStream.serialize(
            BpOrchestrationContract.Callbacks.registerCallback(blockProducer));
    Assertions.assertThat(context.getRemoteCalls().callbackRpc).isEqualTo(registerCallbackRpc);

    LocalPluginStateUpdate localPluginStateUpdate =
        context.getUpdateLocalAccountPluginStates().get(0);
    Assertions.assertThat(localPluginStateUpdate.getContext())
        .isEqualTo(blockProducer.getIdentity());
    byte[] updatePluginStateRpc = AccountPluginRpc.associate(context.getContractAddress());
    Assertions.assertThat(localPluginStateUpdate.getRpc()).isEqualTo(updatePluginStateRpc);
  }

  @Test
  public void invokeRegister_duplicateIdentity() {
    String identity = "Bp";
    Hash hash = Hash.create(s -> s.writeString(identity));
    SecureRandom secureRandom = new SecureRandom(hash.getBytes());
    KeyPair keyPair = new KeyPair(new BigInteger(32, secureRandom));
    BlsKeyPair blsKeyPair = new BlsKeyPair(new BigInteger(32, secureRandom));
    BlockProducer blockProducer =
        createProducerWithBls(
            identity, blsKeyPair, keyPair, BlockProducer.BlockProducerStatus.WAITING_FOR_CALLBACK);

    from(blockProducer.getIdentity());

    BpOrchestrationContractState waitingForCallback = getInitial().setBlockProducer(blockProducer);
    BpOrchestrationContractState tokensAssociatedState =
        receiveTokensAssociatedCallback(
            (byte) BpOrchestrationContract.Callbacks.REGISTER, blockProducer, waitingForCallback);
    assertBpStatus(
        blockProducer.getIdentity(),
        tokensAssociatedState,
        BlockProducer.BlockProducerStatus.PENDING);

    BpOrchestrationContractState confirmPendingState =
        tokensAssociatedState.confirmBlockProducer(blockProducer);
    assertBpStatus(
        blockProducer.getIdentity(),
        confirmPendingState,
        BlockProducer.BlockProducerStatus.CONFIRMED);

    // Create "new" block producer with same identity as first block producer but different keys
    Hash newHash = Hash.create(s -> s.writeString("someRandomSeed"));
    SecureRandom newSecureRandom = new SecureRandom(newHash.getBytes());
    KeyPair newKeyPair = new KeyPair(new BigInteger(32, newSecureRandom));
    BlsKeyPair newBlsKeyPair = new BlsKeyPair(new BigInteger(32, newSecureRandom));
    BlockProducer newBlockProducer =
        createProducerWithBls(
            identity,
            newBlsKeyPair,
            newKeyPair,
            BlockProducer.BlockProducerStatus.WAITING_FOR_CALLBACK);

    from(newBlockProducer.getIdentity());

    // "New" block producer signs a new proof-of-possession message
    Hash newMessage =
        confirmPendingState.createPopMessage(
            newBlockProducer.getIdentity(),
            newBlockProducer.getPublicKey(),
            newBlockProducer.getBlsPublicKey());
    BlsSignature newSignature = newBlsKeyPair.sign(newMessage);

    // "New" block producer tries to register with the same identity as the original block producer,
    // while the original block producer is waiting for a callback
    ThrowableAssert.ThrowingCallable sameIdentityRegister =
        () ->
            serialization.invoke(
                context, waitingForCallback, createRegisterRpc(newBlockProducer, newSignature));
    Assertions.assertThatThrownBy(sameIdentityRegister)
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Block producer has already registered");
  }

  private void assertBpStatus(
      BlockchainAddress bpAddress,
      BpOrchestrationContractState state,
      BlockProducer.BlockProducerStatus expectedStatus) {
    Assertions.assertThat(state.getBlockProducers().getValue(bpAddress).getStatus())
        .isEqualTo(expectedStatus);
  }

  @Test
  public void declineMultipleUsesOfPublicKeyIfPending() {
    String identity = "identity";
    Hash hash = Hash.create(s -> s.writeString(identity));
    SecureRandom secureRandom = new SecureRandom(hash.getBytes());
    KeyPair keyPair = new KeyPair(new BigInteger(32, secureRandom));
    BlsKeyPair blsKeyPair = new BlsKeyPair(new BigInteger(32, secureRandom));
    BlockProducer blockProducer =
        createProducerWithBls(
            identity, blsKeyPair, keyPair, BlockProducer.BlockProducerStatus.WAITING_FOR_CALLBACK);

    BpOrchestrationContractState pendingProducer = getInitial().markAsPending(blockProducer);

    Hash message =
        pendingProducer.createPopMessage(
            blockProducer.getIdentity(),
            blockProducer.getPublicKey(),
            blockProducer.getBlsPublicKey());
    BlsSignature signature = blsKeyPair.sign(message);

    from(blockProducer.getIdentity());

    ThrowableAssert.ThrowingCallable reusedPublicKey =
        () ->
            serialization.invoke(
                context, pendingProducer, createRegisterRpc(blockProducer, signature));

    Assertions.assertThatThrownBy(reusedPublicKey)
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Public key or BLS public key already in use");

    SecureRandom newSecureRandom = new SecureRandom(hash.getBytes());
    KeyPair newKeyPair = new KeyPair(new BigInteger(32, newSecureRandom));

    ThrowableAssert.ThrowingCallable reusedBlsPublicKey =
        () ->
            serialization.invoke(
                context,
                pendingProducer,
                rpc -> {
                  rpc.writeByte(BpOrchestrationContract.Invocations.REGISTER);
                  rpc.writeString("otherName");
                  rpc.writeString("otherWebsite");
                  rpc.writeString("otherAddress");
                  newKeyPair.getPublic().write(rpc);
                  blockProducer.getBlsPublicKey().write(rpc);
                  rpc.writeInt(12);
                  rpc.writeInt(34);
                  signature.write(rpc);
                });
    Assertions.assertThatThrownBy(reusedBlsPublicKey)
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Public key or BLS public key already in use");
  }

  @Test
  public void declineMultipleUsesOfPublicKeyIfRegistered() {
    String identity = "identity";
    Hash hash = Hash.create(s -> s.writeString(identity));
    SecureRandom secureRandom = new SecureRandom(hash.getBytes());
    KeyPair keyPair = new KeyPair(new BigInteger(32, secureRandom));
    BlsKeyPair blsKeyPair = new BlsKeyPair(new BigInteger(32, secureRandom));
    BlockProducer blockProducer =
        createProducerWithBls(
            identity, blsKeyPair, keyPair, BlockProducer.BlockProducerStatus.PENDING);

    BpOrchestrationContractState registeredProducer =
        getInitial().markAsPending(blockProducer).confirmBlockProducer(blockProducer);

    Hash message =
        registeredProducer.createPopMessage(
            blockProducer.getIdentity(),
            blockProducer.getPublicKey(),
            blockProducer.getBlsPublicKey());
    BlsSignature signature = blsKeyPair.sign(message);

    from(blockProducer.getIdentity());

    ThrowableAssert.ThrowingCallable reusedPublicKey =
        () ->
            serialization.invoke(
                context, registeredProducer, createRegisterRpc(blockProducer, signature));

    Assertions.assertThatThrownBy(reusedPublicKey)
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Public key or BLS public key already in use");

    SecureRandom newSecureRandom = new SecureRandom(hash.getBytes());
    KeyPair newKeyPair = new KeyPair(new BigInteger(32, newSecureRandom));

    ThrowableAssert.ThrowingCallable reusedBlsPublicKey =
        () ->
            serialization.invoke(
                context,
                registeredProducer,
                rpc -> {
                  rpc.writeByte(BpOrchestrationContract.Invocations.REGISTER);
                  rpc.writeString("otherName");
                  rpc.writeString("otherWebsite");
                  rpc.writeString("otherAddress");
                  newKeyPair.getPublic().write(rpc);
                  blockProducer.getBlsPublicKey().write(rpc);
                  rpc.writeInt(12);
                  rpc.writeInt(34);
                  signature.write(rpc);
                });
    Assertions.assertThatThrownBy(reusedBlsPublicKey)
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Public key or BLS public key already in use");
  }

  @Test
  public void disassociateDeclinedBlockProducers() {
    KeyPair sk = new KeyPair(BigInteger.TEN);
    AvlTree<BlockchainAddress, BlockProducer> blockProducers =
        AvlTree.create(
            initialProducers.stream()
                .collect(Collectors.toMap(BlockProducer::getIdentity, bp -> bp)));

    BpOrchestrationContractState state =
        new BpOrchestrationContractState(
            null,
            0,
            1,
            new ThresholdKey(thresholdKey, thresholdKeyId),
            new LargeOracleUpdate(
                FixedList.create(initialProducers),
                new ThresholdKey(sk.getPublic(), "sk"),
                null,
                null,
                null,
                Bitmap.fromBytes(new byte[] {0b1011}), // producer 3 is inactive
                37,
                Bitmap.fromBytes(new byte[] {0b1111}),
                null,
                AvlTree.create(),
                null),
            blockProducers,
            null,
            null,
            null,
            null,
            domainSeparator,
            AvlTree.create(),
            0,
            0,
            FixedList.create(),
            null);

    BlockProducer blockProducer = initialProducers.get(2);

    BpOrchestrationContractState declinedState =
        state.removeBlockProducer(blockProducer, FunctionUtility.noOpConsumer());

    BpOrchestrationContractState oracleUpdateState =
        declinedState.withLargeOracleUpdate(declinedState.getOracleUpdate());
    Hash message =
        oracleUpdateState
            .getOracleUpdate()
            .getMessageToBeSigned(declinedState.getSessionId() + 1, domainSeparator);
    Signature sign = thresholdSecretKey.sign(message);

    BpOrchestrationContractState removedBlockProducerState =
        serialization.invoke(
            context,
            oracleUpdateState,
            rpc -> {
              rpc.writeByte(BpOrchestrationContract.Invocations.AUTHORIZE_NEW_ORACLE);
              sign.write(rpc);
            });

    Assertions.assertThat(removedBlockProducerState.getBlockProducers().size())
        .isEqualTo(oracleUpdateState.getBlockProducers().size() - 1);

    byte[] updatePluginStateRpc = AccountPluginRpc.disassociate(context.getContractAddress());
    Assertions.assertThat(context.getUpdateLocalAccountPluginStates().get(0).getRpc())
        .isEqualTo(updatePluginStateRpc);
  }

  @Test
  public void removeYourself() {
    KeyPair sk = new KeyPair(BigInteger.TEN);
    BpOrchestrationContractState state = getUnconfirmedNewOracle(initialProducers, sk.getPublic());
    from(initialProducers.get(0).getIdentity());
    BpOrchestrationContractState removedBlockProducerState =
        serialization.invoke(
            context,
            state,
            rpc -> {
              rpc.writeByte(BpOrchestrationContract.Invocations.REMOVE_BP);
            });
    Assertions.assertThat(
            removedBlockProducerState
                .getBlockProducers()
                .getValue(initialProducers.get(0).getIdentity()))
        .isNotNull();
  }

  @Test
  public void confirmBlockProducer_nullOracleUpdate() {
    BpOrchestrationContractState nullOracleState =
        getInitial().removeBlockProducer(initialProducers.get(0), FunctionUtility.noOpConsumer());
    Assertions.assertThat(nullOracleState.getOracleUpdate()).isNull();
  }

  @Test
  public void confirmBlockProducer_oracleUpdateProducer() {
    BpOrchestrationContractState withOracleState =
        getInitial().createCommitteeUpdate(initialProducers);
    BlockProducer blockProducer = initialProducers.get(0);
    BpOrchestrationContractState inOracleUpdateAndCommittee =
        withOracleState.removeBlockProducer(blockProducer, FunctionUtility.noOpConsumer());
    Assertions.assertThat(
            inOracleUpdateAndCommittee
                .getBlockProducers()
                .getValue(blockProducer.getIdentity())
                .getStatus())
        .isEqualTo(BlockProducer.BlockProducerStatus.REMOVED);
  }

  @Test
  public void confirmBlockProducer_notInOracleOrCommittee() {
    // Create new producer not part of committee
    BlockProducer nonCommittee =
        createProducer("nonCommittee", BlockProducer.BlockProducerStatus.PENDING_UPDATE);
    BpOrchestrationContractState withOracleState =
        getInitial().createCommitteeUpdate(initialProducers);
    initialProducers.add(nonCommittee);
    BpOrchestrationContractState notInOracleOrCommittee =
        withOracleState.removeBlockProducer(
            initialProducers.get(4), FunctionUtility.noOpConsumer());
    Assertions.assertThat(notInOracleOrCommittee.getBlockProducers().size())
        .isEqualTo(initialProducers.size() - 1);
  }

  @Test
  public void onlyKycCanConfirmUpdates() {
    BlockchainAddress address =
        BlockchainAddress.fromHash(
            BlockchainAddress.Type.ACCOUNT, Hash.create(h -> h.writeString("name")));
    from(address);
    Assertions.assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    getInitial(),
                    rpc -> {
                      rpc.writeByte(BpOrchestrationContract.Invocations.CONFIRM_BP);
                      address.write(rpc);
                      rpc.writeBoolean(true);
                    }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Only a KYC account can confirm pending block producer updates");
  }

  @Test
  public void createPopMessage() {
    BlockProducer bp =
        createProducer("blockProducerA", BlockProducer.BlockProducerStatus.CONFIRMED);
    BpOrchestrationContractState state =
        getInitial()
            .markAsPending(
                createProducer("blockProducerB", BlockProducer.BlockProducerStatus.CONFIRMED));
    Hash message =
        state.createPopMessage(bp.getIdentity(), bp.getPublicKey(), bp.getBlsPublicKey());
    Hash expectedMessage =
        Hash.create(
            h -> {
              bp.getIdentity().write(h);
              bp.getPublicKey().write(h);
              bp.getBlsPublicKey().write(h);
              h.writeLong(state.getSessionId());
            });
    Assertions.assertThat(message).isEqualTo(expectedMessage);
  }

  @Test
  public void invokeRegisterInvalid_wrongSignature() {
    BlockProducer blockProducer =
        createProducer("registeredProducer", BlockProducer.BlockProducerStatus.CONFIRMED);
    from(blockProducer.getIdentity());

    BpOrchestrationContractState state = getInitial();

    Hash message =
        state.createPopMessage(
            blockProducer.getIdentity(),
            blockProducer.getPublicKey(),
            blockProducer.getBlsPublicKey());
    BlsSignature signature = new BlsKeyPair(BigInteger.ONE).sign(message);

    ThrowableAssert.ThrowingCallable wrongSignature =
        () ->
            serialization.invoke(
                context,
                state,
                rpc -> {
                  rpc.writeByte(BpOrchestrationContract.Invocations.REGISTER);
                  rpc.writeString("registeringBpName");
                  rpc.writeString("registeringBpWebsite");
                  rpc.writeString("registeringBpAddress");
                  blockProducer.getPublicKey().write(rpc);
                  blockProducer.getBlsPublicKey().write(rpc);
                  rpc.writeInt(12);
                  rpc.writeInt(34);
                  signature.write(rpc);
                });
    Assertions.assertThatThrownBy(wrongSignature)
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Invalid proof of possession of the BLS key");
  }

  @Test
  public void invokeConfirmBlockProducer() {
    List<BlockProducer> pendingProducers = new ArrayList<>();
    for (int i = 0; i < 4; i++) {
      pendingProducers.add(
          createProducer("pending" + i, BlockProducer.BlockProducerStatus.PENDING));
    }

    List<BlockProducer> manyBlockProducers = createConfirmedProducers(21);
    BpOrchestrationContractState manyBlockProducersState =
        BpOrchestrationContractState.initial(
            FixedList.create(kycAddresses),
            manyBlockProducers,
            thresholdKey,
            thresholdKeyId,
            largeOracleContract,
            systemUpdateContractAddress,
            rewardsContractAddress,
            domainSeparator,
            0,
            0);
    BlockProducer first = pendingProducers.get(0);

    from(kycAddresses.get(0));
    BpOrchestrationContractState lessThanFivePercentDifference =
        serialization.invoke(
            context,
            manyBlockProducersState.markAsPending(first),
            rpc -> {
              rpc.writeByte(BpOrchestrationContract.Invocations.CONFIRM_BP);
              first.getIdentity().write(rpc);
            });
    Assertions.assertThat(
            lessThanFivePercentDifference.getConfirmedBlockProducersSinceLastCommittee().size())
        .isEqualTo(1);
    Assertions.assertThat(
            lessThanFivePercentDifference.getBlockProducers().getValue(first.getIdentity()))
        .isNotNull();
    Assertions.assertThat(lessThanFivePercentDifference.getOracleUpdate()).isNull();

    BlockProducer second = pendingProducers.get(1);
    BpOrchestrationContractState moreThanFivePercentDifference =
        serialization.invoke(
            context,
            lessThanFivePercentDifference.markAsPending(second),
            rpc -> {
              rpc.writeByte(BpOrchestrationContract.Invocations.CONFIRM_BP);
              second.getIdentity().write(rpc);
            });

    Assertions.assertThat(
            moreThanFivePercentDifference.getBlockProducers().getValue(second.getIdentity()))
        .isNotNull();
    List<BlockProducer> oracleUpdateProducers =
        moreThanFivePercentDifference.getOracleUpdate().getProducers();
    Assertions.assertThat(oracleUpdateProducers.size()).isEqualTo(23);
    for (BlockProducer blockProducer : List.of(first, second)) {
      Assertions.assertThat(searchForBpWithName(blockProducer.getName(), oracleUpdateProducers))
          .isTrue();
    }
  }

  private boolean searchForBpWithName(String name, List<BlockProducer> blockProducers) {
    for (BlockProducer bp : blockProducers) {
      if (bp.getName().equals(name)) {
        return true;
      }
    }
    return false;
  }

  @Test
  public void checkUpdateCommittee_oracleUpdateNotNull() {
    // more than five percent difference and valid committee size, but with existing oracle update
    BpOrchestrationContractState existingOracleUpdate =
        getInitial()
            .confirmBlockProducer(createProducer("a", BlockProducer.BlockProducerStatus.PENDING))
            .confirmBlockProducer(createProducer("b", BlockProducer.BlockProducerStatus.PENDING))
            .confirmBlockProducer(createProducer("c", BlockProducer.BlockProducerStatus.PENDING))
            .withLargeOracleUpdate(LargeOracleUpdate.create(List.of()))
            .checkUpdateCommittee();
    Assertions.assertThat(
            existingOracleUpdate.getConfirmedBlockProducersSinceLastCommittee().size())
        .isEqualTo(3);
  }

  @Test
  void wontStartUpdateWithOnly3Producers() {
    List<BlockProducer> producers =
        List.of(
            createProducer("1", BlockProducer.BlockProducerStatus.CONFIRMED),
            createProducer("2", BlockProducer.BlockProducerStatus.PENDING),
            createProducer("3", BlockProducer.BlockProducerStatus.PENDING),
            createProducer("4", BlockProducer.BlockProducerStatus.PENDING));
    BpOrchestrationContractState state =
        serialization.create(
            context,
            rpc -> {
              BlockchainAddress.LIST_SERIALIZER.writeDynamic(rpc, kycAddresses);
              BlockProducer.LIST_SERIALIZER.writeDynamic(rpc, List.of(producers.get(0)));
              thresholdKey.write(rpc);
              rpc.writeString(thresholdKeyId);
              largeOracleContract.write(rpc);
              systemUpdateContractAddress.write(rpc);
              rewardsContractAddress.write(rpc);
              domainSeparator.write(rpc);
              rpc.writeLong(0);
            });
    state = state.setBlockProducer(producers.get(1));
    state = state.setBlockProducer(producers.get(2));
    state = state.setBlockProducer(producers.get(3));
    state = state.confirmBlockProducer(producers.get(1));
    state = state.confirmBlockProducer(producers.get(2));
    state = state.checkUpdateCommittee();
    Assertions.assertThat(state.getOracleUpdate()).isNull();
    state = state.confirmBlockProducer(producers.get(3));
    state = state.checkUpdateCommittee();
    Assertions.assertThat(state.getOracleUpdate()).isNotNull();
  }

  @Test
  void confirmTwiceDuringSameCommitteeDoesNotIncrementConfirmedCounter() {
    BpOrchestrationContractState state = getInitial();
    Assertions.assertThat(state.getConfirmedBlockProducersSinceLastCommittee().size()).isEqualTo(0);
    BlockProducer newProducer =
        createProducer("new producer", BlockProducer.BlockProducerStatus.PENDING);
    // Unseen producer, should increment counter
    state = state.confirmBlockProducer(newProducer);
    Assertions.assertThat(state.getConfirmedBlockProducersSinceLastCommittee().size()).isEqualTo(1);
    state = state.removeBlockProducer(newProducer, FunctionUtility.noOpConsumer());
    // Known producer, should not increment counter
    state = state.confirmBlockProducer(newProducer);
    Assertions.assertThat(state.getConfirmedBlockProducersSinceLastCommittee().size()).isEqualTo(1);
    // Trigger new committee
    state = state.checkUpdateCommittee();
    Assertions.assertThat(state.getConfirmedBlockProducersSinceLastCommittee().size()).isEqualTo(0);
    // Adding the producer should again increment counter since we have a new committee
    state = state.confirmBlockProducer(newProducer);
    Assertions.assertThat(state.getConfirmedBlockProducersSinceLastCommittee().size()).isEqualTo(1);
  }

  @Test
  public void checkUpdateCommittee_shouldOnlyCountConfirmed() {
    // since we only count confirmed block producers, two confirmed + one pending is not a valid
    // committee size
    BpOrchestrationContractState existingOracleUpdate =
        getInitial()
            .confirmBlockProducer(createProducer("a", BlockProducer.BlockProducerStatus.PENDING))
            .confirmBlockProducer(createProducer("b", BlockProducer.BlockProducerStatus.PENDING))
            .markAsPending(createProducer("d", BlockProducer.BlockProducerStatus.PENDING))
            .checkUpdateCommittee();
    Assertions.assertThat(existingOracleUpdate.getOracleUpdate()).isNotNull();
  }

  @Test
  public void removeNonCommitteeBpDisassociatesTokens() {
    BlockProducer bp = createProducer("producer", BlockProducer.BlockProducerStatus.CONFIRMED);
    BlockchainAddress bpAddress = bp.getIdentity();
    BpOrchestrationContractState associatedTokensState = getInitial().markAsPending(bp);
    assertBpStatus(bpAddress, associatedTokensState, BlockProducer.BlockProducerStatus.PENDING);
    from(bpAddress);
    BpOrchestrationContractState disassociatedTokensState =
        serialization.invoke(
            context,
            associatedTokensState,
            rpc -> {
              rpc.writeByte(BpOrchestrationContract.Invocations.REMOVE_BP);
            });
    Assertions.assertThat(disassociatedTokensState.getBlockProducers().containsKey(bpAddress))
        .isFalse();
    LocalPluginStateUpdate localPluginStateUpdate =
        context.getUpdateLocalAccountPluginStates().get(0);
    Assertions.assertThat(localPluginStateUpdate.getContext()).isEqualTo(bpAddress);
    byte[] disassociateRpc = AccountPluginRpc.disassociate(context.getContractAddress());
    Assertions.assertThat(localPluginStateUpdate.getRpc()).isEqualTo(disassociateRpc);
  }

  @Test
  void removeCommitteeMemberDoesNotDisassociateTokens() {
    BlockProducer bp = initialProducers.get(0);
    BpOrchestrationContractState state = getInitial().markAsPending(bp);
    from(bp.getIdentity());
    assertBpStatus(bp.getIdentity(), state, BlockProducer.BlockProducerStatus.PENDING);
    state =
        serialization.invoke(
            context, state, rpc -> rpc.writeByte(BpOrchestrationContract.Invocations.REMOVE_BP));
    Assertions.assertThat(context.getUpdateLocalAccountPluginStates()).isEmpty();
    Assertions.assertThat(state.getCommittee()).contains(bp);
    assertBpStatus(bp.getIdentity(), state, BlockProducer.BlockProducerStatus.REMOVED);
  }

  @Test
  public void invokeConfirmBlockProducer_exactlyFivePercent() {
    List<BlockProducer> committee = createConfirmedProducers(20);
    AvlTree<BlockchainAddress, BlockProducer> registeredProducers =
        AvlTree.create(
            committee.stream()
                .collect(Collectors.toMap(BlockProducer::getIdentity, producer -> producer)));

    BlockProducer newProducer =
        createProducer("newProducer", BlockProducer.BlockProducerStatus.CONFIRMED);
    BpOrchestrationContractState state =
        new BpOrchestrationContractState(
            null,
            0,
            0,
            new ThresholdKey(null, "ID"),
            null,
            registeredProducers,
            FixedList.create(committee),
            null,
            null,
            null,
            domainSeparator,
            null,
            0,
            0,
            FixedList.create(List.of(newProducer.getIdentity())),
            null);

    state = state.checkUpdateCommittee();

    Assertions.assertThat(state.getOracleUpdate()).isNull();
  }

  @Test
  public void invokeConfirmBlockProducer_invalid() {
    from(initialProducers.get(0).getIdentity());
    BpOrchestrationContractState state = getInitial();

    ThrowableAssert.ThrowingCallable nonKycAddress =
        () ->
            serialization.invoke(
                context,
                state,
                rpc -> {
                  rpc.writeByte(BpOrchestrationContract.Invocations.CONFIRM_BP);
                  initialProducers.get(1).getIdentity().write(rpc);
                });
    Assertions.assertThatThrownBy(nonKycAddress)
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Only a KYC account can confirm pending block producer updates");

    from(kycAddresses.get(0));
    ThrowableAssert.ThrowingCallable nonExistingBlockProducer =
        () ->
            serialization.invoke(
                context,
                state,
                rpc -> {
                  rpc.writeByte(BpOrchestrationContract.Invocations.CONFIRM_BP);
                  createProducer("nonProducer", BlockProducer.BlockProducerStatus.PENDING)
                      .getIdentity()
                      .write(rpc);
                });
    Assertions.assertThatThrownBy(nonExistingBlockProducer)
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Only existing block producers can be confirmed");

    from(kycAddresses.get(0));
    BlockProducer nonProducer =
        createProducer("nonProducer", BlockProducer.BlockProducerStatus.PENDING);
    BpOrchestrationContractState updatedState = state.confirmBlockProducer(nonProducer);

    ThrowableAssert.ThrowingCallable nonPendingProducer =
        () ->
            serialization.invoke(
                context,
                updatedState,
                rpc -> {
                  rpc.writeByte(BpOrchestrationContract.Invocations.CONFIRM_BP);
                  nonProducer.getIdentity().write(rpc);
                });
    Assertions.assertThatThrownBy(nonPendingProducer)
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Only pending block producers can be confirmed");
  }

  @Test
  public void invokeRemoveBlockProducer_invalid() {
    from(createProducer("notExisting", BlockProducer.BlockProducerStatus.PENDING).getIdentity());
    BpOrchestrationContractState state = getInitial();

    ThrowableAssert.ThrowingCallable nonExistingBlockProducer =
        () ->
            serialization.invoke(
                context,
                state,
                rpc -> {
                  rpc.writeByte(BpOrchestrationContract.Invocations.REMOVE_BP);
                });
    Assertions.assertThatThrownBy(nonExistingBlockProducer)
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Only existing block producers can be removed");
  }

  @Test
  public void callbackAssociateTokens() {
    BlockProducer firstBlockProducer =
        createProducer("nonProducer", BlockProducer.BlockProducerStatus.PENDING);
    BpOrchestrationContractState state =
        receiveTokensAssociatedCallback(
            (byte) BpOrchestrationContract.Callbacks.REGISTER,
            firstBlockProducer,
            getInitial().setBlockProducer(firstBlockProducer));
    Assertions.assertThat(state).isNotNull();

    Assertions.assertThat(state.getBlockProducers().size()).isEqualTo(initialProducers.size() + 1);
    BlockProducer pendingBlockProducer =
        state.getBlockProducers().getValue(firstBlockProducer.getIdentity());
    Assertions.assertThat(pendingBlockProducer).isEqualTo(firstBlockProducer);
    Assertions.assertThat(state.getBlockProducers().containsKey(firstAddress)).isFalse();
  }

  @Test
  void callbackAssociateTokensForAddConfirmBp() {
    BlockProducer producer =
        createProducer("producer", BlockProducer.BlockProducerStatus.WAITING_FOR_CALLBACK);
    BpOrchestrationContractState state =
        receiveTokensAssociatedCallback(
            (byte) BpOrchestrationContract.Callbacks.REGISTER_AND_CONFIRM,
            producer,
            getInitial().setBlockProducer(producer));
    Assertions.assertThat(state.getOracleUpdate())
        .describedAs(
            "Should trigger a new committee since 1 is more than 5% of the initial 4 producers")
        .isNotNull();
    Assertions.assertThat(state.getBlockProducers().size()).isEqualTo(initialProducers.size() + 1);
    Assertions.assertThat(state.getConfirmedBlockProducers().size())
        .isEqualTo(initialProducers.size() + 1);
  }

  private BpOrchestrationContractState receiveTokensAssociatedCallback(
      byte invocation, BlockProducer blockProducer, BpOrchestrationContractState state) {
    CallbackContext callbackContext = CallbackContext.create(FixedList.create(List.of()));
    return serialization.callback(
        context,
        callbackContext,
        state,
        stream -> {
          stream.writeByte(invocation);
          blockProducer.write(stream);
        });
  }

  @Test
  public void callbackAssociateTokens_invalid() {
    SafeDataInputStream emptyResult = SafeDataInputStream.createFromBytes(new byte[0]);
    CallbackContext callbackContext =
        CallbackContext.create(
            FixedList.create(
                List.of(CallbackContext.createResult(domainSeparator, false, emptyResult))));
    BpOrchestrationContractState state =
        serialization.callback(
            context,
            callbackContext,
            getInitial(),
            BpOrchestrationContract.Callbacks.registerCallback(initialProducers.get(0)));
    Assertions.assertThat(state).isNotNull();
    Assertions.assertThat(state.getBlockProducers().size()).isEqualTo(initialProducers.size());
  }

  @Test
  void failingCallbackRemovesProducer() {
    BpOrchestrationContractState initial = getInitial();
    KeyPair producerInCallback = new KeyPair(BigInteger.valueOf(456));
    BlockProducer producer =
        createProducerWithBls(
            "ident",
            new BlsKeyPair(BigInteger.valueOf(123)),
            producerInCallback,
            BlockProducer.BlockProducerStatus.REMOVED);
    from(kycAddresses.get(0));
    BpOrchestrationContractState state =
        makeAddConfirmedBpInvocation(initial, producer, new BlsKeyPair(BigInteger.valueOf(123)));
    Assertions.assertThat(state.getBlockProducers().getValue(producer.getIdentity()).getStatus())
        .isEqualTo(BlockProducer.BlockProducerStatus.WAITING_FOR_CALLBACK);
    BpOrchestrationContractState stateCallback =
        serialization.callback(
            context,
            CallbackContext.create(
                FixedList.create(
                    List.of(
                        CallbackContext.createResult(
                            domainSeparator,
                            false,
                            SafeDataInputStream.createFromBytes(new byte[0]))))),
            state,
            stream -> {
              stream.writeByte((byte) BpOrchestrationContract.Callbacks.REGISTER_AND_CONFIRM);
              producer.write(stream);
            });
    Assertions.assertThat(
            stateCallback
                .getBlockProducers()
                .containsKey(producerInCallback.getPublic().createAddress()))
        .isEqualTo(false);
    StateSerializableEquality.assertStatesEqual(initial, stateCallback);
  }

  @Test
  public void callbackAssociateTokensForAddConfirmBp_invalid() {
    SafeDataInputStream emptyResult = SafeDataInputStream.createFromBytes(new byte[0]);
    CallbackContext callbackContext =
        CallbackContext.create(
            FixedList.create(
                List.of(CallbackContext.createResult(domainSeparator, false, emptyResult))));
    BpOrchestrationContractState state =
        serialization.callback(
            context,
            callbackContext,
            getInitial(),
            BpOrchestrationContract.Callbacks.registerAndConfirm(initialProducers.get(0)));
    Assertions.assertThat(state).isNotNull();
    Assertions.assertThat(state.getBlockProducers().size()).isEqualTo(initialProducers.size());
  }

  private BlockchainAddress createContractAddress(BlockchainAddress.Type type, String hashString) {
    return BlockchainAddress.fromHash(type, Hash.create(hash -> hash.writeString(hashString)));
  }

  @Test
  public void authorizeNewOracle() {
    List<BlockProducer> newProducers = createConfirmedProducers(4);

    BpOrchestrationContractState initial = getInitial();
    Assertions.assertThat(initial.getSessionId()).isEqualTo(0);
    Assertions.assertThat(initial.getRetryNonce()).isEqualTo(0);

    for (BlockProducer producer : newProducers) {
      initial = initial.confirmBlockProducer(producer);
    }

    LargeOracleUpdate update = LargeOracleUpdate.create(newProducers);
    update = LargeOracleUpdateTest.markAllActive(update, newProducers);
    BpOrchestrationContractState state = initial.withLargeOracleUpdate(update);

    BlockchainPublicKey tpk = new KeyPair(BigInteger.TEN).getPublic();
    List<BlockchainAddress> honestParties =
        newProducers.stream().map(BlockProducer::getIdentity).toList();
    state =
        addCandidate(state, newProducers.get(0), tpk, "Magic identifier for key", honestParties);

    List<CandidateKey> candidateKeys = state.getOracleUpdate().getCandidates();
    Assertions.assertThat(candidateKeys).hasSize(1);
    Assertions.assertThat(candidateKeys.get(0).getThresholdKey().getKey()).isEqualTo(tpk);
    Assertions.assertThat(candidateKeys.get(0).getVotes()).isEqualTo(1);
    // no key yet
    Assertions.assertThat(state.getOracleUpdate().getThresholdKey()).isNull();

    state =
        addCandidate(state, newProducers.get(1), tpk, "Magic identifier for key", honestParties);
    // everyone votes on the same key
    Assertions.assertThat(state.getOracleUpdate().getCandidates()).hasSize(1);
    Assertions.assertThat(state.getOracleUpdate().getCandidates().get(0).getVotes()).isEqualTo(2);
    // no key yet
    Assertions.assertThat(state.getOracleUpdate().getThresholdKey()).isNull();

    state =
        addCandidate(state, newProducers.get(2), tpk, "Magic identifier for key", honestParties);
    BlockchainPublicKey thresholdKey = state.getOracleUpdate().getThresholdKey().getKey();
    Assertions.assertThat(state.getOracleUpdate().getCandidates()).hasSize(1);
    Assertions.assertThat(state.getOracleUpdate().getCandidates().get(0).getVotes()).isEqualTo(3);
    Assertions.assertThat(thresholdKey).isNotNull();
    Assertions.assertThat(thresholdKey).isEqualTo(tpk);

    LargeOracleUpdate oracleUpdate = state.getOracleUpdate();
    Hash message = oracleUpdate.getMessageToBeSigned(state.getSessionId() + 1, domainSeparator);
    Signature sign = thresholdSecretKey.sign(message);

    Assertions.assertThat(state.getCommitteeLog().size()).isEqualTo(0);
    state = addSignature(state, initialProducers.get(0), sign);

    ContractEventInteraction invokeContract =
        (ContractEventInteraction) context.getInteractions().get(0);
    Assertions.assertThat(invokeContract.contract).isEqualTo(largeOracleContract);
    verifyLargeOracleUpdate(invokeContract.rpc, tpk, newProducers);

    ContractEventInteraction invokeContract2 =
        (ContractEventInteraction) context.getInteractions().get(1);
    Assertions.assertThat(invokeContract2.contract).isEqualTo(systemUpdateContractAddress);
    verifySystemUpdateContractUpdate(invokeContract2.rpc, newProducers);

    ContractEventInteraction invokeContract3 =
        (ContractEventInteraction) context.getInteractions().get(2);
    Assertions.assertThat(invokeContract3.contract).isEqualTo(rewardsContractAddress);
    verifyRewardsContractUpdate(invokeContract3.rpc, newProducers);
    Assertions.assertThat(context.getInteractions().size()).isEqualTo(3);
    verifyFastTrackPluginUpdate(
        context.getUpdateGlobalConsensusPluginState().getRpc(), newProducers, state.getSessionId());

    Assertions.assertThat(state.getOracleUpdate()).isNull();
    Assertions.assertThat(state.getThresholdKey().getKey()).isEqualTo(tpk);
    Assertions.assertThat(state.getCommittee()).isEqualTo(newProducers);
    Assertions.assertThat(state.getSessionId()).isEqualTo(1);
    Assertions.assertThat(state.getCommitteeLog().size()).isEqualTo(1);
  }

  private void verifyLargeOracleUpdate(
      DataStreamSerializable value,
      BlockchainPublicKey thresholdKey,
      List<BlockProducer> newProducers) {
    byte[] actualRpc = SafeDataOutputStream.serialize(value);
    byte[] expectedRpc =
        SafeDataOutputStream.serialize(
            LargeOracleRpc.replaceLargeOracle(newProducers, thresholdKey));
    Assertions.assertThat(actualRpc).isEqualTo(expectedRpc);
  }

  private void verifySystemUpdateContractUpdate(
      DataStreamSerializable value, List<BlockProducer> newProducers) {
    byte[] actualRpc = SafeDataOutputStream.serialize(value);
    byte[] expectedRpc =
        SafeDataOutputStream.serialize(VotingRpc.receiveCommitteeInformation(newProducers));
    Assertions.assertThat(actualRpc).isEqualTo(expectedRpc);
  }

  private void verifyRewardsContractUpdate(
      DataStreamSerializable value, List<BlockProducer> newProducers) {
    List<BlockchainAddress> newProducersAddresses =
        newProducers.stream().map(BlockProducer::getIdentity).toList();
    byte[] actualRpc = SafeDataOutputStream.serialize(value);
    byte[] expectedRpc =
        SafeDataOutputStream.serialize(FeeDistributionRpc.updateCommittee(newProducersAddresses));
    Assertions.assertThat(actualRpc).isEqualTo(expectedRpc);
  }

  private void verifyFastTrackPluginUpdate(
      byte[] actualRpc, List<BlockProducer> newProducers, int sessionId) {
    List<BlockchainAddress> newProducersAddresses =
        newProducers.stream().map(BlockProducer::getIdentity).toList();
    Hash hashId =
        Hash.create(
            hash -> {
              hash.writeInt(sessionId);
              BlockchainAddress.LIST_SERIALIZER.writeDynamic(hash, newProducersAddresses);
            });
    byte[] expectedRpc = ConsensusPluginRpc.setCommittee(hashId, newProducers);
    Assertions.assertThat(actualRpc).isEqualTo(expectedRpc);
  }

  @Test
  public void cannotAddCandidateWithoutActiveUpdate() {
    BpOrchestrationContractState initial = getInitial();
    Assertions.assertThatThrownBy(
            () ->
                addCandidate(
                    initial,
                    initialProducers.get(0),
                    thresholdKey,
                    "Magic identifier for key",
                    List.of()))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("No update is currently in progress");
  }

  @Test
  public void cannotAddCandidateIfNotPartOfNewOracle() {
    BpOrchestrationContractState initial = getInitial();
    List<BlockProducer> newProducers = createConfirmedProducers(3);
    LargeOracleUpdate update = LargeOracleUpdate.create(newProducers);
    update = LargeOracleUpdateTest.markAllActive(update, newProducers);
    BpOrchestrationContractState state = initial.withLargeOracleUpdate(update);
    Assertions.assertThatThrownBy(
            () ->
                addCandidate(
                    state,
                    createProducer(
                        "notNewOracleMember", BlockProducer.BlockProducerStatus.CONFIRMED),
                    thresholdKey,
                    "Magic identifier for key",
                    List.of()))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Only new oracle members can propose a new threshold key");
  }

  @Test
  public void cannotAddCandidateIfThresholdKeyHasAlreadyBeenPicked() {
    BpOrchestrationContractState initial = getInitial();
    List<BlockProducer> newProducers = createConfirmedProducers(4);
    LargeOracleUpdate update = LargeOracleUpdate.create(newProducers);
    update = LargeOracleUpdateTest.markAllActive(update, newProducers);
    BpOrchestrationContractState state = initial.withLargeOracleUpdate(update);
    BlockchainPublicKey newKey = new KeyPair(BigInteger.TEN).getPublic();
    List<BlockchainAddress> honestParties =
        newProducers.stream().map(BlockProducer::getIdentity).toList();
    state =
        addCandidate(state, newProducers.get(0), newKey, "Magic identifier for key", honestParties);
    state =
        addCandidate(state, newProducers.get(1), newKey, "Magic identifier for key", honestParties);
    BpOrchestrationContractState finalState =
        addCandidate(state, newProducers.get(2), newKey, "Magic identifier for key", honestParties);
    // cannot add a key since one has already been determined
    Assertions.assertThatThrownBy(
            () ->
                addCandidate(
                    finalState,
                    newProducers.get(3),
                    newKey,
                    "Magic identifier for key",
                    honestParties))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Threshold public key of the new oracle have already been determined");
    Assertions.assertThat(finalState.getOracleUpdate().getSignatureRandomization())
        .isEqualTo(Hash.create(s -> s.writeLong(context.getBlockProductionTime())));
  }

  @Test
  void addBroadcastMessage() {
    BpOrchestrationContractState initial = getInitial();
    List<BlockProducer> producers = createConfirmedProducers(5);
    List<BlockProducer> active = producers.subList(1, 5);

    Assertions.assertThatThrownBy(() -> addBroadcastHash(initial, 1, 1, 0, "too fast"))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("No update in progress");

    LargeOracleUpdate oracleUpdate = LargeOracleUpdate.create(active);
    oracleUpdate = LargeOracleUpdateTest.markAllActive(oracleUpdate, active);
    BpOrchestrationContractState state = initial.withLargeOracleUpdate(oracleUpdate);

    from(active.get(0).getIdentity());
    BpOrchestrationContractState hello = addBroadcastHash(state, 1, 0, 1, "hello");

    LargeOracleUpdate helloUpdate = hello.getOracleUpdate();
    Assertions.assertThat(helloUpdate.getBroadcastTracker(1).getMessageCount()).isEqualTo(1);

    // not an active producer
    from(producers.get(0).getIdentity());
    Assertions.assertThatThrownBy(() -> addBroadcastHash(hello, 1, 0, 1, "hello again"))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Producer not part of protocol");

    from(active.get(0).getIdentity());
    Assertions.assertThatThrownBy(() -> addBroadcastHash(hello, 123, 0, 1, "ignored"))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Invalid session: 1 != 123");

    Assertions.assertThatThrownBy(() -> addBroadcastHash(hello, 1, 123, 1, "ignored"))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Invalid retry nonce: 0 != 123");

    Assertions.assertThatThrownBy(() -> addBroadcastHash(hello, 1, 0, 123, "ignored"))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Invalid broadcast round: 1 != 123");
  }

  private BpOrchestrationContractState addBroadcastHash(
      BpOrchestrationContractState state,
      int sessionId,
      int retryNonce,
      int broadcastRound,
      String hashString) {
    return serialization.invoke(
        context,
        state,
        rpc -> {
          rpc.writeByte(BpOrchestrationContract.Invocations.ADD_BROADCAST_HASH);
          rpc.writeInt(sessionId);
          rpc.writeInt(retryNonce);
          rpc.writeInt(broadcastRound);
          Hash.create(s -> s.writeString(hashString)).write(rpc);
        });
  }

  private BpOrchestrationContractState addHashToState(
      Hash hash, BpOrchestrationContractState state) {
    return addBroadcastHash(state, 1, 0, 1, hash.toString());
  }

  @Test
  void updateBroadcastBits() {
    BpOrchestrationContractState initial = getInitial();
    Assertions.assertThatThrownBy(
            () -> {
              addBroadcastBits(initial, 1, 0, 1, new byte[] {0b0});
            })
        .isInstanceOf(RuntimeException.class)
        .hasMessage("No update in progress");

    List<BlockProducer> producers = createConfirmedProducers(5);
    List<BlockProducer> active = producers.subList(1, 5);
    LargeOracleUpdate oracleUpdate = LargeOracleUpdate.create(active);
    oracleUpdate = LargeOracleUpdateTest.markAllActive(oracleUpdate, active);
    BpOrchestrationContractState state = initial.withLargeOracleUpdate(oracleUpdate);

    from(active.get(0).getIdentity());
    BpOrchestrationContractState withHash =
        addHashToState(Hash.create(s -> s.writeString("hey!")), state);
    from(active.get(1).getIdentity());
    BpOrchestrationContractState withBits = addBroadcastBits(withHash, 1, 0, 1, new byte[] {0b1});

    Assertions.assertThatThrownBy(
            () -> addBroadcastBits(withHash, 1, 0, 1, new byte[] {0b11, 0b10}))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Invalid amount of bytes: 1 != 2");

    from(active.get(0).getIdentity());
    BpOrchestrationContractState withMoreBits =
        addBroadcastBits(withBits, 1, 0, 1, new byte[] {0b1});

    LargeOracleUpdate update = withMoreBits.getOracleUpdate();
    Assertions.assertThat(update.getBroadcastTracker(1).getMessageCount()).isEqualTo(1);
    Bitmap messageBitmap =
        update.getBroadcastTracker(1).getMessageBitmap(active.get(0).getIdentity());
    Assertions.assertThat(messageBitmap).isEqualTo(Bitmap.fromBytes(new byte[] {0b11}));

    from(producers.get(0).getIdentity());
    Assertions.assertThatThrownBy(() -> addBroadcastBits(withBits, 1, 0, 1, new byte[1]))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Producer not part of protocol");

    from(active.get(0).getIdentity());
    Assertions.assertThatThrownBy(() -> addBroadcastBits(withBits, 123, 0, 1, new byte[1]))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Invalid session: 1 != 123");

    Assertions.assertThatThrownBy(() -> addBroadcastBits(withBits, 1, 123, 1, new byte[1]))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Invalid retry nonce: 0 != 123");

    Assertions.assertThatThrownBy(() -> addBroadcastBits(withBits, 1, 0, 123, new byte[1]))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Invalid broadcast round: 1 != 123");
  }

  private BpOrchestrationContractState addBroadcastHashes(
      BpOrchestrationContractState state, List<BlockProducer> producers) {
    for (BlockProducer producer : producers) {
      from(producer.getIdentity());
      state = addBroadcastHash(state, 1, 0, 1, producer.getIdentity().toString());
    }
    return state;
  }

  @Test
  void updateBroadcastBitsWontAdvancePrematurely() {
    BpOrchestrationContractState initial = getInitial(5);

    List<BlockProducer> confirmedProducers = createConfirmedProducers(4);
    LargeOracleUpdate oracleUpdate = LargeOracleUpdate.create(confirmedProducers);
    byte[] bitmap = LargeOracleUpdateTest.createAllOnesBitmap(confirmedProducers);
    for (BlockProducer producer : confirmedProducers) {
      oracleUpdate = oracleUpdate.addHeartbeat(producer.getIdentity(), bitmap, 4, null);
    }
    BpOrchestrationContractState state = initial.withLargeOracleUpdate(oracleUpdate);

    setBlockProductionTime(8);
    state = addBroadcastHashes(state, confirmedProducers);

    for (int i = 0; i < 3; i++) {
      BlockProducer producer = confirmedProducers.get(i);
      from(producer.getIdentity());
      state = addBroadcastBits(state, 1, 0, 1, new byte[] {0b1011});
      Assertions.assertThat(state.getOracleUpdate().getCurrentBroadcastRound()).isEqualTo(1);
    }

    Assertions.assertThat(state.getOracleUpdate().hasReceivedEnoughMessages()).isTrue();
    Assertions.assertThat(state.getOracleUpdate().getCurrentBroadcastRound()).isEqualTo(1);

    setBlockProductionTime(9);
    from(confirmedProducers.get(3).getIdentity());

    BpOrchestrationContractState nextRound = addBroadcastBits(state, 1, 0, 1, new byte[] {0b1011});
    Assertions.assertThat(nextRound.getOracleUpdate().getCurrentBroadcastRound()).isEqualTo(2);

    setBlockProductionTime(8);
    BpOrchestrationContractState afterPoke =
        serialization.invoke(
            context,
            state,
            rpc -> rpc.writeByte(BpOrchestrationContract.Invocations.POKE_ONGOING_UPDATE));
    Assertions.assertThat(afterPoke.getOracleUpdate().getCurrentBroadcastRound()).isEqualTo(1);

    setBlockProductionTime(9);
    afterPoke =
        serialization.invoke(
            context,
            state,
            rpc -> rpc.writeByte(BpOrchestrationContract.Invocations.POKE_ONGOING_UPDATE));
    Assertions.assertThat(afterPoke.getOracleUpdate().getCurrentBroadcastRound()).isEqualTo(2);
  }

  @Test
  void progressToNextRoundWhenEveryoneHasSentBroadcast() {
    BpOrchestrationContractState initial = getInitial(5);
    List<BlockProducer> confirmedProducers = createConfirmedProducers(4);
    LargeOracleUpdate oracleUpdate = LargeOracleUpdate.create(confirmedProducers);
    oracleUpdate = LargeOracleUpdateTest.markAllActive(oracleUpdate, confirmedProducers);
    BpOrchestrationContractState state = initial.withLargeOracleUpdate(oracleUpdate);

    state = addBroadcastHashes(state, confirmedProducers);

    for (int i = 0; i < 2; i++) {
      BlockProducer producer = confirmedProducers.get(i);
      from(producer.getIdentity());
      state = addBroadcastBits(state, 1, 0, 1, new byte[] {0b1111});
    }
    // We have enough broadcasters, but not enough echos
    Assertions.assertThat(state.getOracleUpdate().getBroadcasters(1)).isEqualTo(confirmedProducers);
    Assertions.assertThat(state.getOracleUpdate().getCurrentBroadcastRound()).isEqualTo(1);
    from(confirmedProducers.get(2).getIdentity());
    state = addBroadcastBits(state, 1, 0, 1, new byte[] {0b1111});
    // Now we have enough echos
    Assertions.assertThat(state.getOracleUpdate().getCurrentBroadcastRound()).isEqualTo(2);
  }

  @Test
  void invalidPokes() {
    BpOrchestrationContractState initial = getInitial();
    Assertions.assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    initial,
                    rpc -> rpc.writeByte(BpOrchestrationContract.Invocations.POKE_ONGOING_UPDATE)))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("No update to poke");

    LargeOracleUpdate updateWithKey =
        new LargeOracleUpdate(
            null,
            new ThresholdKey(thresholdKey, thresholdKeyId),
            null,
            null,
            null,
            null,
            0,
            null,
            null,
            AvlTree.create(),
            null);
    BpOrchestrationContractState state = initial.withLargeOracleUpdate(updateWithKey);

    Assertions.assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    state,
                    rpc -> rpc.writeByte(BpOrchestrationContract.Invocations.POKE_ONGOING_UPDATE)))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Cannot poke finished update");
  }

  @Test
  void pokingStalledUpdateWillResetTheUpdate() {
    final BpOrchestrationContractState initial = getInitial(5);
    List<BlockProducer> confirmedProducers = createConfirmedProducers(4);
    LargeOracleUpdate update = LargeOracleUpdate.create(confirmedProducers);
    Assertions.assertThat(update.getRetryNonce()).isEqualTo(0);
    byte[] allOnesBitmap = LargeOracleUpdateTest.createAllOnesBitmap(confirmedProducers);
    for (BlockProducer producer : confirmedProducers) {
      update = update.addHeartbeat(producer.getIdentity(), allOnesBitmap, 1, null);
    }
    setBlockProductionTime(2);
    BpOrchestrationContractState state = initial.withLargeOracleUpdate(update);
    state = addBroadcastHashes(state, confirmedProducers);

    setBlockProductionTime(10);
    state =
        serialization.invoke(
            context,
            state,
            rpc -> rpc.writeByte(BpOrchestrationContract.Invocations.POKE_ONGOING_UPDATE));
    LargeOracleUpdate oracleUpdate = state.getOracleUpdate();
    Assertions.assertThat(oracleUpdate.getRetryNonce()).isEqualTo(1);
  }

  @Test
  public void cannotAuthorizeNewOracleWithInvalidSignature() {
    List<BlockProducer> newProducers = createConfirmedProducers(4);
    KeyPair sk = new KeyPair(BigInteger.TEN);
    BlockchainPublicKey pk = sk.getPublic();
    BpOrchestrationContractState state = getUnconfirmedNewOracle(newProducers, pk);
    Hash invalid = Hash.create(s -> s.writeInt(123));
    Assertions.assertThatThrownBy(
            () -> addSignature(state, initialProducers.get(0), sk.sign(invalid)))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("New oracle signature invalid");
  }

  @Test
  public void addHeartbeat() {
    List<BlockProducer> producers = createConfirmedProducers(4);
    BpOrchestrationContractState initial = getInitial();
    BpOrchestrationContractState state =
        initial.withLargeOracleUpdate(LargeOracleUpdate.create(producers));
    // this mimics LargeOracleUpdateTest#attemptSelectionOfProducers
    from(producers.get(0).getIdentity());
    byte[] bitmap = {0b1111};
    state = addHeartbeat(bitmap, state);
    from(producers.get(1).getIdentity());
    state = addHeartbeat(bitmap, state);
    from(producers.get(2).getIdentity());
    state = addHeartbeat(bitmap, state);
    Assertions.assertThat(state.getOracleUpdate().getActiveProducers())
        .containsExactlyInAnyOrder(
            producers.get(0), producers.get(1), producers.get(2), producers.get(3));

    // cannot add heartbeat from another producer as the active participants have already been
    // selected. It doesn't matter that this person has seen heartbeats from everyone.
    from(producers.get(3).getIdentity());
    BpOrchestrationContractState finalState = state;
    Assertions.assertThatThrownBy(() -> addHeartbeat(new byte[] {0b11111}, finalState))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Active participants have already been selected");
  }

  private BpOrchestrationContractState addHeartbeat(
      byte[] bitmap, BpOrchestrationContractState state) {
    return serialization.invoke(
        context,
        state,
        rpc -> {
          rpc.writeByte(BpOrchestrationContract.Invocations.ADD_HEARTBEAT);
          rpc.writeDynamicBytes(bitmap);
        });
  }

  @Test
  public void unknownProducerCannotAddHeartbeat() {
    BpOrchestrationContractState initial = getInitial();
    LargeOracleUpdate update = LargeOracleUpdate.create(initialProducers);
    BpOrchestrationContractState state = initial.withLargeOracleUpdate(update);
    BlockProducer unknown = createProducer("unknown", BlockProducer.BlockProducerStatus.CONFIRMED);
    from(unknown.getIdentity());
    Assertions.assertThatThrownBy(() -> addHeartbeat(new byte[] {0b11}, state))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Unknown producer");
  }

  @Test
  public void cannotAddHeartbeatWithInProgressUpdate() {
    BpOrchestrationContractState state = getInitial();
    from(initialProducers.get(0).getIdentity());
    Assertions.assertThatThrownBy(() -> addHeartbeat(new byte[] {0b11}, state))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("No oracle update in progress");
  }

  @Test
  void retryNonceResetsToZero() {
    BpOrchestrationContractState state =
        new BpOrchestrationContractState(
            null,
            0,
            1,
            new ThresholdKey(thresholdKey, thresholdKeyId),
            new LargeOracleUpdate(
                FixedList.create(),
                null,
                null,
                null,
                null,
                Bitmap.create(0),
                37,
                Bitmap.create(0),
                LargeOracleUpdateTest.getEmptyBroadcastList(),
                AvlTree.create(),
                null),
            AvlTree.create(),
            null,
            null,
            null,
            null,
            null,
            initialCommitteeLog,
            0,
            0,
            FixedList.create(),
            null);
    Assertions.assertThat(state.getRetryNonce()).isEqualTo(1);
    BpOrchestrationContractState updated =
        state.withNewLargeOracle(signature, 0, FunctionUtility.noOpConsumer());
    Assertions.assertThat(updated.getRetryNonce()).isEqualTo(37);
  }

  @Test
  public void invokeTriggerNewCommitteeAfterOneMonthPassed() {
    AvlTree<BlockchainAddress, BlockProducer> blockProducers =
        createEnoughConfirmedProducersToActiveOracleUpdate();

    BpOrchestrationContractState state = createStateWithBlockProducers(blockProducers.values());
    setBlockProductionTime(ONE_MONTH_MILLISECONDS);
    Assertions.assertThat(state.getOracleUpdate()).isNull();
    BpOrchestrationContractState triggerNewCommittee =
        serialization.invoke(
            context,
            state,
            rpc -> {
              rpc.writeByte(BpOrchestrationContract.Invocations.TRIGGER_NEW_COMMITTEE);
            });
    Assertions.assertThat(triggerNewCommittee.getOracleUpdate().getProducers())
        .isEqualTo(blockProducers.values());
  }

  /** Trying to trigger a new committee before one month has passed will throw an exception. */
  @Test
  public void invokeTriggerNewCommitteeBeforeOneMonthHasPassed() {
    BlockProducer acceptedProducer =
        createProducer("acceptedProducer", BlockProducer.BlockProducerStatus.CONFIRMED);
    BlockProducer declinedProducer =
        createProducer("declinedProducer", BlockProducer.BlockProducerStatus.REMOVED);
    AvlTree<BlockchainAddress, BlockProducer> blockProducers = AvlTree.create();
    blockProducers =
        blockProducers
            .set(acceptedProducer.getIdentity(), acceptedProducer)
            .set(declinedProducer.getIdentity(), declinedProducer);

    BpOrchestrationContractState state = createStateWithBlockProducers(blockProducers.values());
    setBlockProductionTime(ONE_MONTH_MILLISECONDS - 1);
    Assertions.assertThat(state.getOracleUpdate()).isNull();
    Assertions.assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    state,
                    rpc -> {
                      rpc.writeByte(BpOrchestrationContract.Invocations.TRIGGER_NEW_COMMITTEE);
                    }))
        .hasMessage("Not enough time has passed since the creation of the current committee.");
  }

  /** One day needs to have passed, when triggering a new committee from a governance contract. */
  @Test
  public void invokeTriggerNewCommitteeFromGovernance() {
    from(systemUpdateContractAddress);

    BpOrchestrationContractState state = getInitial();
    setBlockProductionTime(ONE_DAY_MILLISECONDS - 1 + state.getCommitteeEnabledTimeStamp());
    Assertions.assertThat(state.getOracleUpdate()).isNull();
    Assertions.assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    state,
                    rpc ->
                        rpc.writeByte(BpOrchestrationContract.Invocations.TRIGGER_NEW_COMMITTEE)))
        .hasMessage("Not enough time has passed since the creation of the current committee.");

    setBlockProductionTime(ONE_DAY_MILLISECONDS + state.getCommitteeEnabledTimeStamp());
    BpOrchestrationContractState triggerNewCommittee =
        serialization.invoke(
            context,
            state,
            rpc -> rpc.writeByte(BpOrchestrationContract.Invocations.TRIGGER_NEW_COMMITTEE));
    Assertions.assertThat(triggerNewCommittee.getOracleUpdate()).isNotNull();
  }

  /**
   * Triggering a new committee when less than one month has passed fails even when the current
   * enabled committee time is not zero.
   */
  @Test
  void invokeTriggerNewCommitteeWithNonZeroEnabledCommitteeTime() {
    AvlTree<BlockchainAddress, BlockProducer> blockProducers =
        createEnoughConfirmedProducersToActiveOracleUpdate();

    BpOrchestrationContractState updateInProgressState =
        new BpOrchestrationContractState(
            null,
            0,
            1,
            new ThresholdKey(thresholdKey, thresholdKeyId),
            new LargeOracleUpdate(
                FixedList.create(),
                null,
                null,
                null,
                null,
                Bitmap.fromBytes(new byte[0]),
                37,
                null,
                null,
                AvlTree.create(),
                null),
            blockProducers,
            null,
            null,
            null,
            null,
            null,
            AvlTree.create(),
            0,
            0,
            FixedList.create(),
            null);
    Assertions.assertThat(updateInProgressState.getCommitteeEnabledTimeStamp()).isEqualTo(0);
    BpOrchestrationContractState updated =
        updateInProgressState.withNewLargeOracle(signature, 1000, FunctionUtility.noOpConsumer());
    Assertions.assertThat(updated.getCommitteeEnabledTimeStamp()).isEqualTo(1000);
    setBlockProductionTime(ONE_MONTH_MILLISECONDS + 999);
    Assertions.assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    updated,
                    rpc -> {
                      rpc.writeByte(BpOrchestrationContract.Invocations.TRIGGER_NEW_COMMITTEE);
                    }))
        .hasMessage("Not enough time has passed since the creation of the current committee.");
  }

  @Test
  public void testRemoveDeclinedBlockProducersAfterCommitteeUpdate() {
    BlockProducer acceptedProducer =
        createProducer("acceptedProducer", BlockProducer.BlockProducerStatus.CONFIRMED);
    BlockProducer declinedProducer =
        createProducer("declinedProducer", BlockProducer.BlockProducerStatus.REMOVED);
    AvlTree<BlockchainAddress, BlockProducer> blockProducers = AvlTree.create();
    blockProducers =
        blockProducers
            .set(acceptedProducer.getIdentity(), acceptedProducer)
            .set(declinedProducer.getIdentity(), declinedProducer);

    BpOrchestrationContractState state =
        new BpOrchestrationContractState(
            null,
            0,
            1,
            new ThresholdKey(thresholdKey, thresholdKeyId),
            new LargeOracleUpdate(
                FixedList.create(blockProducers.values()),
                null,
                null,
                null,
                null,
                Bitmap.fromBytes(new byte[] {0b10}), // declined producer is inactive
                37,
                Bitmap.fromBytes(new byte[] {0b11}),
                null,
                AvlTree.create(),
                null),
            blockProducers,
            null,
            null,
            null,
            null,
            null,
            AvlTree.create(),
            0,
            0,
            FixedList.create(),
            null);
    Assertions.assertThat(state.getBlockProducers().size()).isEqualTo(2);
    BpOrchestrationContractState updated =
        state.withNewLargeOracle(signature, 0, FunctionUtility.noOpConsumer());
    Assertions.assertThat(updated.getBlockProducers().size()).isEqualTo(1);
    Assertions.assertThat(updated.getBlockProducers().containsKey(acceptedProducer.getIdentity()))
        .isTrue();
  }

  @Test
  void onlyEnableActiveProducers() {
    List<BlockProducer> producers = createConfirmedProducers(7);
    AvlTree<BlockchainAddress, BlockProducer> blockProducers = AvlTree.create();
    for (BlockProducer producer : producers) {
      blockProducers = blockProducers.set(producer.getIdentity(), producer);
    }
    BpOrchestrationContractState state =
        new BpOrchestrationContractState(
            null,
            0,
            1,
            new ThresholdKey(thresholdKey, thresholdKeyId),
            new LargeOracleUpdate(
                FixedList.create(producers),
                null,
                null,
                null,
                null,
                Bitmap.fromBytes(new byte[] {0b1111, 0b0}),
                37,
                Bitmap.fromBytes(new byte[] {0b1111, 0b0}),
                LargeOracleUpdateTest.getEmptyBroadcastList(),
                AvlTree.create(),
                null),
            blockProducers,
            null,
            null,
            null,
            null,
            null,
            initialCommitteeLog,
            0,
            0,
            FixedList.create(),
            null);
    Assertions.assertThat(state.getRetryNonce()).isEqualTo(1);
    BpOrchestrationContractState updated =
        state.withNewLargeOracle(signature, 0, FunctionUtility.noOpConsumer());
    Assertions.assertThat(updated.getCommittee())
        .containsExactlyElementsOf(producers.subList(0, 4));
  }

  @Test
  void onUpgrade() {
    BpOrchestrationContractState initial = getInitial();
    BpOrchestrationContractState upgraded = upgradeContract(initial, null);
    Assertions.assertThat(upgraded.getCommittee()).isEqualTo(initial.getCommittee());
  }

  @Test
  void onUpgradeAdditionalKyc() {
    BpOrchestrationContractState initial = getInitial();
    Assertions.assertThat(initial.getKycAddresses()).doesNotContain(firstAddress);
    BpOrchestrationContractState upgraded = upgradeContract(initial, firstAddress);
    Assertions.assertThat(upgraded.getCommittee()).isEqualTo(initial.getCommittee());
    Assertions.assertThat(upgraded.getKycAddresses()).contains(firstAddress);
  }

  @Test
  void onUpgradeConfirmPendingUpdate() {
    BlockProducer blockProducer = initialProducers.get(0);
    BpOrchestrationContractState state =
        getInitial()
            .setBlockProducer(
                blockProducer.updateStatus(BlockProducer.BlockProducerStatus.PENDING_UPDATE));
    Assertions.assertThat(state.getConfirmedBlockProducers()).hasSize(3);
    BpOrchestrationContractState upgraded = upgradeContract(state, null);
    Assertions.assertThat(upgraded.getConfirmedBlockProducers()).containsAll(initialProducers);
    // A single added producer will trigger an update when the current committee is of size 4
    Assertions.assertThat(upgraded.getOracleUpdate()).isNotNull();
  }

  /** Upgrading state with minimum version retains minimum version after upgrade. */
  @Test
  void onUpgradeWithMinimumVersion() {
    BpOrchestrationContractState initial = getInitial();
    SemanticVersion newMinimumVersion = new SemanticVersion(1, 2, 3);
    initial = initial.setMinimumNodeVersion(newMinimumVersion);
    BpOrchestrationContractState upgraded = upgradeContract(initial, null);
    Assertions.assertThat(getMinimumVersion(upgraded)).isEqualTo(newMinimumVersion);
  }

  /** When upgrading state block producers with node version retains version after upgrade. */
  @Test
  void onUpgradeWithNodeVersion() {
    BpOrchestrationContractState initial = getInitial();
    SemanticVersion nodeVersion = new SemanticVersion(1, 2, 3);
    BlockProducer bpWithVersion =
        initial.getBlockProducers().values().get(0).updateNodeVersion(nodeVersion);
    BpOrchestrationContractState state = initial.setBlockProducer(bpWithVersion);
    BpOrchestrationContractState upgraded = upgradeContract(state, null);
    BlockProducer upgradedBpWithVersion =
        upgraded.getBlockProducers().getValue(bpWithVersion.getIdentity());
    Assertions.assertThat(upgradedBpWithVersion.getNodeVersion()).isEqualTo(nodeVersion);
  }

  @Test
  void getProducerInformation() {
    BpOrchestrationContractState initial = getInitial();
    BlockchainAddress producerIdentity = initialProducers.get(0).getIdentity();
    BpOrchestrationContractState state =
        serialization.invoke(
            context,
            initial,
            rpc -> {
              rpc.writeByte(BpOrchestrationContract.Invocations.GET_PRODUCER_INFO);
              producerIdentity.write(rpc);
            });
    Assertions.assertThat(state).isNotNull();
    byte[] result = context.getResult();
    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(result);
    Assertions.assertThat(readBlockproducer(stream)).isEqualTo(initialProducers.get(0));
  }

  @Test
  void getProducerInformationDoesNotExist() {
    BpOrchestrationContractState initial = getInitial();
    BlockchainAddress identity =
        BlockchainAddress.fromString("000000000000000000000000000000000000000000");
    Assertions.assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    initial,
                    rpc -> {
                      rpc.writeByte(BpOrchestrationContract.Invocations.GET_PRODUCER_INFO);
                      identity.write(rpc);
                    }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage(
            "Producer with identity 000000000000000000000000000000000000000000 does not exist");
  }

  @Test
  void getProducerInformationNotConfirmed() {
    BpOrchestrationContractState state = getInitial().markAsPending(initialProducers.get(0));
    BlockchainAddress identity = initialProducers.get(0).getIdentity();
    Assertions.assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    state,
                    rpc -> {
                      rpc.writeByte(BpOrchestrationContract.Invocations.GET_PRODUCER_INFO);
                      identity.write(rpc);
                    }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Producer with identity " + identity.writeAsString() + " is not confirmed");
  }

  private BpOrchestrationContractState makeAddConfirmedBpInvocation(
      BpOrchestrationContractState state, BlockProducer producer, BlsKeyPair blsKeyPair) {
    BlsSignature sign;
    if (blsKeyPair != null) {
      Hash popMessage =
          state.createPopMessage(
              producer.getIdentity(), producer.getPublicKey(), producer.getBlsPublicKey());
      sign = blsKeyPair.sign(popMessage);
    } else {
      BlsKeyPair dummyKey = new BlsKeyPair(BigInteger.TEN);
      sign = dummyKey.sign(Hash.create(s -> s.writeInt(123)));
    }
    return serialization.invoke(
        context,
        state,
        rpc -> {
          rpc.writeByte(BpOrchestrationContract.Invocations.ADD_CONFIRMED_BP);
          producer.getIdentity().write(rpc);
          rpc.writeString(producer.getName());
          rpc.writeString(producer.getWebsite());
          rpc.writeString(producer.getAddress());
          producer.getPublicKey().write(rpc);
          producer.getBlsPublicKey().write(rpc);
          rpc.writeInt(producer.getEntityJurisdiction());
          rpc.writeInt(producer.getServerJurisdiction());
          sign.write(rpc);
        });
  }

  @Test
  void onlyKycCanAddConfirmedBp() {
    BpOrchestrationContractState initial = getInitial();
    BlockProducer blockProducer = initialProducers.get(0);
    from(blockProducer.getIdentity());
    BlsSignature signature =
        new BlsKeyPair(BigInteger.ONE).sign(Hash.create(s -> s.writeString("some message")));
    Assertions.assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    initial,
                    rpc -> {
                      rpc.writeByte(BpOrchestrationContract.Invocations.ADD_CONFIRMED_BP);
                      blockProducer.getIdentity().write(rpc);
                      rpc.writeString(blockProducer.getName());
                      rpc.writeString(blockProducer.getWebsite());
                      rpc.writeString(blockProducer.getAddress());
                      blockProducer.getPublicKey().write(rpc);
                      blockProducer.getBlsPublicKey().write(rpc);
                      rpc.writeInt(blockProducer.getEntityJurisdiction());
                      rpc.writeInt(blockProducer.getServerJurisdiction());
                      signature.write(rpc);
                    }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Only known KYC providers can add confirmed BPs");
  }

  @Test
  void addConfirmedBp() {
    BpOrchestrationContractState initial = getInitial();
    BlsKeyPair blsKeyPair = new BlsKeyPair(BigInteger.valueOf(123));
    KeyPair keyPair = new KeyPair(BigInteger.valueOf(456));
    // status doesn't matter here
    BlockProducer producer =
        createProducerWithBls(
            "ident", blsKeyPair, keyPair, BlockProducer.BlockProducerStatus.REMOVED);
    from(kycAddresses.get(0));
    BpOrchestrationContractState state =
        makeAddConfirmedBpInvocation(initial, producer, blsKeyPair);
    BlockProducer producerInState = state.getBlockProducers().getValue(producer.getIdentity());
    Assertions.assertThat(producerInState.getStatus())
        .isEqualTo(BlockProducer.BlockProducerStatus.WAITING_FOR_CALLBACK);
    Assertions.assertThat(context.getRemoteCalls().callbackCost).isNull();
    byte[] registerCallbackRpc =
        SafeDataOutputStream.serialize(
            stream -> {
              stream.writeByte((byte) BpOrchestrationContract.Callbacks.REGISTER_AND_CONFIRM);
              producerInState.write(stream);
            });
    Assertions.assertThat(context.getRemoteCalls().callbackRpc).isEqualTo(registerCallbackRpc);
    Assertions.assertThat(context.getUpdateLocalAccountPluginStates().get(0).getContext())
        .isEqualTo(producer.getIdentity());
    // Cannot "register" producer again, because they're still waiting for the callback to be
    // executed.
    Assertions.assertThatThrownBy(() -> makeAddConfirmedBpInvocation(state, producer, blsKeyPair))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Cannot update information for producer undergoing registration");
  }

  @Test
  void shouldNotBeAbleToRemoveSelfWhileWaitingForCallback() {
    BpOrchestrationContractState initial = getInitial();
    BlockProducer producer =
        createProducer("ident", BlockProducer.BlockProducerStatus.WAITING_FOR_CALLBACK);

    from(producer.getIdentity());
    BpOrchestrationContractState contractState = initial.setBlockProducer(producer);

    Assertions.assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    contractState,
                    rpc -> {
                      rpc.writeByte(BpOrchestrationContract.Invocations.REMOVE_BP);
                    }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Unable to remove while waiting for callback");
  }

  private BlockProducer changeNameOfProducer(BlockProducer producer) {
    return new BlockProducer(
        "New producer name",
        producer.getWebsite(),
        producer.getName(),
        producer.getIdentity(),
        producer.getPublicKey(),
        producer.getBlsPublicKey(),
        producer.getNumberOfVotes(),
        producer.getEntityJurisdiction(),
        producer.getServerJurisdiction(),
        producer.getStatus(),
        producer.getNodeVersion());
  }

  @Test
  void addConfirmedBpMarksExistingAsConfirmed() {
    BpOrchestrationContractState initial = getInitial();
    BlsKeyPair blsKeyPair = new BlsKeyPair(BigInteger.valueOf(123));
    KeyPair keyPair = new KeyPair(BigInteger.valueOf(456));
    BlockProducer producer =
        createProducerWithBls(
            "cool_", blsKeyPair, keyPair, BlockProducer.BlockProducerStatus.REMOVED);
    from(kycAddresses.get(0));
    BpOrchestrationContractState state =
        makeAddConfirmedBpInvocation(initial, producer, blsKeyPair);
    state = state.markAsPending(producer);
    BlockProducer producerInState = state.getBlockProducers().getValue(producer.getIdentity());
    Assertions.assertThat(producerInState.getName()).isEqualTo("cool_name");
    BlockProducer differentInfo = changeNameOfProducer(producerInState);
    state = makeAddConfirmedBpInvocation(state, differentInfo, blsKeyPair);
    BlockProducer updated = state.getBlockProducers().getValue(producer.getIdentity());
    Assertions.assertThat(updated.getStatus())
        .isEqualTo(BlockProducer.BlockProducerStatus.CONFIRMED);
    Assertions.assertThat(updated.getName()).isEqualTo("New producer name");
  }

  @Test
  void cannotAddConfirmBpWithInvalidPopSignature() {
    BpOrchestrationContractState initial = getInitial();
    BlockProducer producer = createProducer("bogus", BlockProducer.BlockProducerStatus.CONFIRMED);
    from(kycAddresses.get(0));
    Assertions.assertThatThrownBy(() -> makeAddConfirmedBpInvocation(initial, producer, null))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Invalid proof of possession of the BLS key");
  }

  private BpOrchestrationContractState addBroadcastBits(
      BpOrchestrationContractState state,
      int sessionId,
      int retryNonce,
      int broadcastRound,
      byte[] bytes) {
    return serialization.invoke(
        context,
        state,
        rpc -> {
          rpc.writeByte(BpOrchestrationContract.Invocations.ADD_BROADCAST_BITS);
          rpc.writeInt(sessionId);
          rpc.writeInt(retryNonce);
          rpc.writeInt(broadcastRound);
          rpc.writeDynamicBytes(bytes);
        });
  }

  /**
   * A block producer can update their node version through 'UPDATE_NODE_VERSION' invocation.
   * Updated node version is saved in contract state.
   */
  @Test
  void updateNodeVersion() {
    BpOrchestrationContractState initial = getInitial();
    SemanticVersion oldVersion = new SemanticVersion(1, 2, 3);
    BlockProducer producer =
        createProducer(
                "000000000000000000000000000000000000000001",
                BlockProducer.BlockProducerStatus.CONFIRMED)
            .updateNodeVersion(oldVersion);
    from(producer.getIdentity());
    BpOrchestrationContractState state = initial.setBlockProducer(producer);
    Assertions.assertThat(
            state.getBlockProducers().getValue(producer.getIdentity()).getNodeVersion())
        .isEqualTo(oldVersion);

    SemanticVersion newVersion = new SemanticVersion(4, 5, 6);
    Consumer<SafeDataOutputStream> updateVersionRpc =
        rpc -> {
          rpc.writeByte(BpOrchestrationContract.Invocations.UPDATE_NODE_VERSION);
          writeVersion(newVersion, rpc);
        };
    BpOrchestrationContractState updatedState =
        serialization.invoke(context, state, updateVersionRpc);

    Assertions.assertThat(
            updatedState.getBlockProducers().getValue(producer.getIdentity()).getNodeVersion())
        .isEqualTo(newVersion);
  }

  /** Block producer can update their node version from null. */
  @Test
  void updateNodeVersionFromNull() {
    BpOrchestrationContractState initial = getInitial();
    BlockProducer producer =
        createProducer(
            "000000000000000000000000000000000000000001",
            BlockProducer.BlockProducerStatus.CONFIRMED);
    from(producer.getIdentity());
    BpOrchestrationContractState state = initial.setBlockProducer(producer.updateNodeVersion(null));
    Assertions.assertThat(
            state.getBlockProducers().getValue(producer.getIdentity()).getNodeVersion())
        .isEqualTo(null);

    SemanticVersion newVersion = new SemanticVersion(4, 5, 6);
    Consumer<SafeDataOutputStream> updateVersionRpc =
        rpc -> {
          rpc.writeByte(BpOrchestrationContract.Invocations.UPDATE_NODE_VERSION);
          writeVersion(newVersion, rpc);
        };
    BpOrchestrationContractState updatedState =
        serialization.invoke(context, state, updateVersionRpc);

    Assertions.assertThat(
            updatedState.getBlockProducers().getValue(producer.getIdentity()).getNodeVersion())
        .isEqualTo(newVersion);
  }

  /** Only block producers already found in contract state can update their node version. */
  @Test
  void cannotUpdateVersionOfMissingProducer() {
    BpOrchestrationContractState initial = getInitial();
    String producerIdentity = "000000000000000000000000000000000000000001";
    BlockProducer producer =
        createProducer(producerIdentity, BlockProducer.BlockProducerStatus.CONFIRMED);
    from(producer.getIdentity());

    SemanticVersion newVersion = new SemanticVersion(4, 5, 6);
    Consumer<SafeDataOutputStream> updateVersionRpc =
        rpc -> {
          rpc.writeByte(BpOrchestrationContract.Invocations.UPDATE_NODE_VERSION);
          writeVersion(newVersion, rpc);
        };

    Assertions.assertThatThrownBy(() -> serialization.invoke(context, initial, updateVersionRpc))
        .isInstanceOf(RuntimeException.class)
        .hasMessage(
            "Producer with identity " + producer.getIdentity().writeAsString() + " does not exist");
  }

  /** System update contract can change the minimum node version of bpo contract. */
  @Test
  void setMinimumNodeVersion() {
    BpOrchestrationContractState initial = getInitial();
    Assertions.assertThat(getMinimumVersion(initial)).isEqualTo(null);

    from(systemUpdateContractAddress);
    SemanticVersion newVersion = new SemanticVersion(123, 456, 789);
    Consumer<SafeDataOutputStream> updateVersionRpc =
        rpc -> {
          rpc.writeByte(BpOrchestrationContract.Invocations.SET_MINIMUM_NODE_VERSION);
          writeVersion(newVersion, rpc);
        };
    BpOrchestrationContractState updatedState =
        serialization.invoke(context, initial, updateVersionRpc);
    Assertions.assertThat(getMinimumVersion(updatedState)).isEqualTo(newVersion);
  }

  /** Only system update contract can change minimum node version. */
  @Test
  void onlySystemUpdateContractCanSetMinimumNodeVersion() {
    BpOrchestrationContractState initial = getInitial();
    from(BlockchainAddress.fromString("000000000000000000000000000000000000000002"));

    SemanticVersion newVersion = new SemanticVersion(123, 456, 789);
    Consumer<SafeDataOutputStream> updateVersionRpc =
        rpc -> {
          rpc.writeByte(BpOrchestrationContract.Invocations.SET_MINIMUM_NODE_VERSION);
          writeVersion(newVersion, rpc);
        };
    Assertions.assertThatThrownBy(() -> serialization.invoke(context, initial, updateVersionRpc))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Only the system update contract can update the minimum node version");
  }

  /** Blocks producer with out-of-date node versions are not included in future committees. */
  @Test
  void outOfDateBpsAreNotIncludedInCommittee() {
    SemanticVersion newVersion = new SemanticVersion(1, 1, 1);
    List<BlockProducer> blockProducers = new ArrayList<>();
    for (BlockProducer bp : createConfirmedProducers(15)) {
      blockProducers.add(bp.updateNodeVersion(newVersion));
    }

    List<BlockProducer> producers = createConfirmedProducers(5);
    List<BlockProducer> outOfDateProducers = new ArrayList<>();
    outOfDateProducers.add(producers.get(0).updateNodeVersion(null));
    outOfDateProducers.add(producers.get(1).updateNodeVersion(new SemanticVersion(0, 1, 1)));
    outOfDateProducers.add(producers.get(2).updateNodeVersion(new SemanticVersion(1, 0, 1)));
    outOfDateProducers.add(producers.get(3).updateNodeVersion(new SemanticVersion(1, 1, 0)));
    outOfDateProducers.add(producers.get(4).updateNodeVersion(new SemanticVersion(0, 2, 2)));
    blockProducers.add(outOfDateProducers.get(0));
    blockProducers.add(outOfDateProducers.get(1));
    blockProducers.add(outOfDateProducers.get(2));
    blockProducers.add(outOfDateProducers.get(3));
    blockProducers.add(outOfDateProducers.get(4));

    BpOrchestrationContractState state =
        createStateWithBlockProducers(blockProducers).setMinimumNodeVersion(newVersion);
    setBlockProductionTime(ONE_MONTH_MILLISECONDS);
    state =
        serialization.invoke(
            context,
            state,
            rpc -> {
              rpc.writeByte(BpOrchestrationContract.Invocations.TRIGGER_NEW_COMMITTEE);
            });

    Bitmap heartbeats = Bitmap.allOnes(20);
    for (int i = 0; i < 13; i++) {
      from(blockProducers.get(i).getIdentity());
      state = addHeartbeat(heartbeats.asBytes(), state);
    }

    Assertions.assertThat(state.getOracleUpdate().getActiveProducers())
        .doesNotContainAnyElementsOf(outOfDateProducers);
  }

  /** Block producer with up-to-date versions or newer are included from future committees. */
  @Test
  void newerVersionsIsIncludedInCommittee() {
    SemanticVersion newVersion = new SemanticVersion(1, 1, 1);
    List<BlockProducer> blockProducers = new ArrayList<>();
    for (BlockProducer bp : createEnoughConfirmedProducersToActiveOracleUpdate().values()) {
      blockProducers.add(bp.updateNodeVersion(newVersion));
    }

    List<BlockProducer> producers = createConfirmedProducers(4);
    List<BlockProducer> newerVersionProducers = new ArrayList<>();
    newerVersionProducers.add(producers.get(0).updateNodeVersion(new SemanticVersion(1, 1, 1)));
    newerVersionProducers.add(producers.get(1).updateNodeVersion(new SemanticVersion(2, 1, 1)));
    newerVersionProducers.add(producers.get(2).updateNodeVersion(new SemanticVersion(1, 2, 1)));
    newerVersionProducers.add(producers.get(3).updateNodeVersion(new SemanticVersion(1, 1, 2)));
    blockProducers.add(newerVersionProducers.get(0));
    blockProducers.add(newerVersionProducers.get(1));
    blockProducers.add(newerVersionProducers.get(2));
    blockProducers.add(newerVersionProducers.get(3));

    BpOrchestrationContractState state =
        createStateWithBlockProducers(blockProducers).setMinimumNodeVersion(newVersion);
    setBlockProductionTime(ONE_MONTH_MILLISECONDS);
    state =
        serialization.invoke(
            context,
            state,
            rpc -> {
              rpc.writeByte(BpOrchestrationContract.Invocations.TRIGGER_NEW_COMMITTEE);
            });
    Assertions.assertThat(state.getOracleUpdate().getProducers())
        .containsAll(newerVersionProducers);

    byte[] heartbeats = new byte[] {(byte) 0b11111111, 0b0};
    for (int i = 0; i < 5; i++) {
      from(blockProducers.get(i).getIdentity());
      state = addHeartbeat(heartbeats, state);
    }

    Assertions.assertThat(state.getOracleUpdate().getActiveProducers()).containsAll(blockProducers);
  }

  SemanticVersion getMinimumVersion(BpOrchestrationContractState state) {
    StateAccessor stateAccessor = StateAccessor.create(state);
    return SemanticVersion.createFromStateAccessor(stateAccessor.get("minimumNodeVersion"));
  }

  /** If the minimum node version is `null` all node versions are included in the committee. */
  @Test
  void nullMinimumVersionAllowsEverything() {
    List<BlockProducer> blockProducers =
        createEnoughConfirmedProducersToActiveOracleUpdate().values();
    blockProducers.add(
        createProducer("000", BlockProducer.BlockProducerStatus.CONFIRMED)
            .updateNodeVersion(new SemanticVersion(0, 0, 0)));
    blockProducers.add(
        createProducer("min", BlockProducer.BlockProducerStatus.CONFIRMED)
            .updateNodeVersion(new SemanticVersion(Integer.MIN_VALUE, 0, 0)));
    blockProducers.add(
        createProducer("max", BlockProducer.BlockProducerStatus.CONFIRMED)
            .updateNodeVersion(new SemanticVersion(Integer.MAX_VALUE, 0, 0)));

    BpOrchestrationContractState state = createStateWithBlockProducers(blockProducers);

    setBlockProductionTime(ONE_MONTH_MILLISECONDS);
    state =
        serialization.invoke(
            context,
            state,
            rpc -> {
              rpc.writeByte(BpOrchestrationContract.Invocations.TRIGGER_NEW_COMMITTEE);
            });
    Assertions.assertThat(state.getOracleUpdate().getProducers()).containsAll(blockProducers);

    byte[] heartbeats = new byte[] {(byte) 0b01111111};
    for (int i = 0; i < 5; i++) {
      from(blockProducers.get(i).getIdentity());
      state = addHeartbeat(heartbeats, state);
    }

    Assertions.assertThat(state.getOracleUpdate().getActiveProducers()).containsAll(blockProducers);
  }

  /**
   * If a block producer node version is null they are included into the committee only if the
   * minimum node version is also `null`.
   */
  @Test
  void versionIsUpToDateIfBothAreNull() {
    List<BlockProducer> blockProducers =
        createEnoughConfirmedProducersToActiveOracleUpdate().values();
    Assertions.assertThat(blockProducers.get(0).getNodeVersion()).isNull();

    BpOrchestrationContractState state = createStateWithBlockProducers(blockProducers);
    Assertions.assertThat(getMinimumVersion(state)).isEqualTo(null);

    setBlockProductionTime(ONE_MONTH_MILLISECONDS);
    BpOrchestrationContractState triggerNewCommittee =
        serialization.invoke(
            context,
            state,
            rpc -> {
              rpc.writeByte(BpOrchestrationContract.Invocations.TRIGGER_NEW_COMMITTEE);
            });
    Assertions.assertThat(triggerNewCommittee.getOracleUpdate().getProducers())
        .containsAll(blockProducers);
  }

  /**
   * If a block producer updates its node version during register callback its node version is not
   * overwritten.
   */
  @Test
  void bpSetNodeVersionWhileRegistering() {
    BlsKeyPair blsKeyPair = new BlsKeyPair(BigInteger.valueOf(123));
    KeyPair keyPair = new KeyPair(BigInteger.valueOf(456));
    BlockProducer blockProducer =
        createProducerWithBls(
            "registeringBp", blsKeyPair, keyPair, BlockProducer.BlockProducerStatus.PENDING);
    from(blockProducer.getIdentity());
    BpOrchestrationContractState state = getInitial();
    Hash message =
        state.createPopMessage(
            blockProducer.getIdentity(),
            blockProducer.getPublicKey(),
            blockProducer.getBlsPublicKey());
    BlsSignature signature = blsKeyPair.sign(message);

    BpOrchestrationContractState sameState =
        serialization.invoke(context, state, createRegisterRpc(blockProducer, signature));

    SemanticVersion newVersion = new SemanticVersion(1, 2, 3);
    BpOrchestrationContractState updatedBpNodeVersion =
        serialization.invoke(
            context,
            sameState,
            rpc -> {
              rpc.writeByte(BpOrchestrationContract.Invocations.UPDATE_NODE_VERSION);
              writeVersion(newVersion, rpc);
            });

    SafeDataInputStream emptyResult = SafeDataInputStream.createFromBytes(new byte[0]);
    CallbackContext callbackContext =
        CallbackContext.create(
            FixedList.create(
                List.of(CallbackContext.createResult(domainSeparator, true, emptyResult))));

    BpOrchestrationContractState stateAfterCallback =
        serialization.callback(
            context,
            callbackContext,
            updatedBpNodeVersion,
            BpOrchestrationContract.Callbacks.registerCallback(blockProducer));

    Assertions.assertThat(
            stateAfterCallback
                .getBlockProducers()
                .getValue(blockProducer.getIdentity())
                .getNodeVersion())
        .isEqualTo(newVersion);
  }

  /**
   * If a block producer updates its node version during register callback its node version is not
   * overwritten.
   */
  @Test
  void bpSetNodeVersionWhileRegisterAndConfirm() {
    BlsKeyPair blsKeyPair = new BlsKeyPair(BigInteger.valueOf(123));
    KeyPair keyPair = new KeyPair(BigInteger.valueOf(456));
    BlockProducer blockProducer =
        createProducerWithBls(
            "registerAndConfirmBp", blsKeyPair, keyPair, BlockProducer.BlockProducerStatus.PENDING);
    BpOrchestrationContractState state = getInitial();
    state = state.setBlockProducer(blockProducer);

    from(kycAddresses.get(0));
    BpOrchestrationContractState sameState =
        makeAddConfirmedBpInvocation(state, blockProducer, blsKeyPair);

    SemanticVersion newVersion = new SemanticVersion(1, 2, 3);
    from(blockProducer.getIdentity());
    BpOrchestrationContractState updatedBpNodeVersion =
        serialization.invoke(
            context,
            sameState,
            rpc -> {
              rpc.writeByte(BpOrchestrationContract.Invocations.UPDATE_NODE_VERSION);
              writeVersion(newVersion, rpc);
            });

    SafeDataInputStream emptyResult = SafeDataInputStream.createFromBytes(new byte[0]);
    CallbackContext callbackContext =
        CallbackContext.create(
            FixedList.create(
                List.of(CallbackContext.createResult(domainSeparator, true, emptyResult))));

    BpOrchestrationContractState stateAfterCallback =
        serialization.callback(
            context,
            callbackContext,
            updatedBpNodeVersion,
            BpOrchestrationContract.Callbacks.registerAndConfirm(blockProducer));

    Assertions.assertThat(
            stateAfterCallback
                .getBlockProducers()
                .getValue(blockProducer.getIdentity())
                .getNodeVersion())
        .isEqualTo(newVersion);
  }

  /**
   * Old state without minimum node version can be upgraded to state with minimum node version
   * field.
   */
  @Test
  void onUpgradeWithOldState() {
    OldBpOrchestrationContractState oldState = createOldStateWithOneBlockProducer();
    BpOrchestrationContract contract = new BpOrchestrationContract();
    StateAccessor oldStateAccessor = StateAccessor.create(oldState);
    Assertions.assertThat(oldStateAccessor.hasField("minimumNodeVersion")).isFalse();
    Assertions.assertThat(
            oldStateAccessor
                .get("blockProducers")
                .getTreeLeaves()
                .get(0)
                .getValue()
                .hasField("nodeVersion"))
        .isFalse();

    BpOrchestrationContractState upgraded = contract.upgrade(oldStateAccessor, null);
    Assertions.assertThat(upgraded.getBlockProducers().size()).isEqualTo(1);

    StateAccessor newStateAccessor = StateAccessor.create(upgraded);
    Assertions.assertThat(newStateAccessor.hasField("minimumNodeVersion")).isTrue();
    Assertions.assertThat(
            newStateAccessor
                .get("blockProducers")
                .getTreeLeaves()
                .get(0)
                .getValue()
                .hasField("nodeVersion"))
        .isTrue();
  }

  private OldBpOrchestrationContractState createOldStateWithOneBlockProducer() {
    BpOrchestrationContractState initial = getInitial();
    HashMap<BlockchainAddress, OldBlockProducer> oldProducers = new HashMap<>();
    BlockProducer bp = initial.getBlockProducers().values().get(0);
    OldBlockProducer oldProducer =
        new OldBlockProducer(
            bp.getName(),
            bp.getAddress(),
            bp.getWebsite(),
            bp.getIdentity(),
            bp.getEntityJurisdiction(),
            bp.getServerJurisdiction(),
            bp.getPublicKey(),
            bp.getBlsPublicKey(),
            bp.getNumberOfVotes(),
            bp.getStatus());
    oldProducers.put(oldProducer.identity(), oldProducer);
    return new OldBpOrchestrationContractState(
        initial.getThresholdKey(),
        initial.getKycAddresses(),
        0,
        0,
        null,
        AvlTree.create(oldProducers),
        FixedList.create(),
        largeOracleContract,
        systemUpdateContractAddress,
        rewardsContractAddress,
        AvlTree.create(),
        initial.getDomainSeparator(),
        0,
        0,
        FixedList.create());
  }

  private BpOrchestrationContractState upgradeContract(
      BpOrchestrationContractState oldState, BlockchainAddress additionalKyc) {
    BpOrchestrationContract contract = new BpOrchestrationContract();
    StateAccessor stateAccessor = StateAccessor.create(oldState);
    return contract.upgrade(stateAccessor, additionalKyc);
  }

  /**
   * When updating with a new large oracle, producers who are not active in the large oracle update
   * protocol are marked with status 'INACTIVE'.
   */
  @Test
  void inactiveProducersAreSetToInactiveDuringWithNewLargeOracle() {
    List<BlockProducer> producers = createConfirmedProducers(6);
    Bitmap activeProducersBitmap = Bitmap.create(6).setBit(0).setBit(1).setBit(4).setBit(5);
    LargeOracleUpdate oracleUpdate =
        createOracleUpdateWithActiveProducers(producers, activeProducersBitmap);
    BpOrchestrationContractState initial = getInitial(5, producers);

    BpOrchestrationContractState state = initial.withLargeOracleUpdate(oracleUpdate);
    BpOrchestrationContractState updated =
        state.withNewLargeOracle(signature, 0, FunctionUtility.noOpConsumer());
    List<BlockProducer> activeProducers =
        List.of(producers.get(0), producers.get(1), producers.get(4), producers.get(5));
    Assertions.assertThat(updated.getCommittee()).containsExactlyElementsOf(activeProducers);
    AvlTree<BlockchainAddress, BlockProducer> updatedBlockProducers = updated.getBlockProducers();
    for (BlockProducer bp : activeProducers) {
      Assertions.assertThat(updatedBlockProducers.getValue(bp.getIdentity()).getStatus())
          .isEqualTo(BlockProducer.BlockProducerStatus.CONFIRMED);
    }
    List<BlockProducer> inactiveProducers = List.of(producers.get(2), producers.get(3));
    for (BlockProducer bp : inactiveProducers) {
      Assertions.assertThat(updatedBlockProducers.getValue(bp.getIdentity()).getStatus())
          .isEqualTo(BlockProducer.BlockProducerStatus.INACTIVE);
    }
  }

  /**
   * If a producer gets the status 'REMOVED' during the keyGen protocol and does not participate in
   * the protocol, they are removed from the list of producers and thus do not receive the status
   * 'INACTIVE'.
   */
  @Test
  void producersWhoAreRemovedDuringKeyGenAndInactiveIsNotMarkedAsInactive() {
    List<BlockProducer> producers = createConfirmedProducers(6);
    Bitmap activeProducersBitmap = Bitmap.create(6).setBit(0).setBit(1).setBit(4).setBit(5);
    LargeOracleUpdate oracleUpdate =
        createOracleUpdateWithActiveProducers(producers, activeProducersBitmap);
    BpOrchestrationContractState initial = getInitial(5, producers);

    BlockProducer inactiveProducer = producers.get(2);
    BpOrchestrationContractState state =
        initial
            .withLargeOracleUpdate(oracleUpdate)
            .removeBlockProducer(inactiveProducer, FunctionUtility.noOpConsumer());
    BpOrchestrationContractState updated =
        state.withNewLargeOracle(signature, 0, FunctionUtility.noOpConsumer());
    AvlTree<BlockchainAddress, BlockProducer> updatedBlockProducers = updated.getBlockProducers();
    Assertions.assertThat(updatedBlockProducers.containsKey(inactiveProducer.getIdentity()))
        .isEqualTo(false);
  }

  /** Active producer selection with over 1/3 out-of-date producers will fail. */
  @Test
  void outOfDateProducersCannotCauseLossOfOverOneThirdProducers() {
    SemanticVersion newVersion = new SemanticVersion(1, 1, 1);
    List<BlockProducer> producers = createConfirmedProducers(4);
    List<BlockProducer> committee = new ArrayList<>();
    for (BlockProducer bp : producers) {
      committee.add(bp.updateNodeVersion(newVersion));
    }
    List<BlockProducer> outOfDateProducers = createConfirmedProducers(3);
    committee.addAll(outOfDateProducers);

    BpOrchestrationContractState state = createStateWithBlockProducers(committee);
    state = state.setMinimumNodeVersion(newVersion);

    BlockProducer newProducer =
        createProducer("newProducer", BlockProducer.BlockProducerStatus.CONFIRMED)
            .updateNodeVersion(newVersion);
    state = state.setBlockProducer(newProducer);

    Assertions.assertThat(state.getBlockProducers().size()).isEqualTo(8);
    Assertions.assertThat(outOfDateProducers.size()).isEqualTo(3);

    setBlockProductionTime(ONE_MONTH_MILLISECONDS);
    state =
        serialization.invoke(
            context,
            state,
            rpc -> {
              rpc.writeByte(BpOrchestrationContract.Invocations.TRIGGER_NEW_COMMITTEE);
            });

    byte[] heartbeats = new byte[] {(byte) 0b11111111, 0b0};
    for (int i = 0; i < 4; i++) {
      from(committee.get(i).getIdentity());
      state = addHeartbeat(heartbeats, state);
    }

    Assertions.assertThat(state.getOracleUpdate().getActiveProducers()).isNull();
  }

  /**
   * If a producer gets status 'REMOVED' during the keyGen protocol but participates anyway, they
   * are not removed from the list of block producers in state, they keep status 'REMOVED' and are
   * included in the committee.
   */
  @Test
  void producersWhoAreRemovedDuringKeyGenAndActiveAreNotIncludedInCommittee() {
    List<BlockProducer> producers = createConfirmedProducers(6);
    Bitmap activeProducersBitmap = Bitmap.create(6).setBit(0).setBit(1).setBit(4).setBit(5);
    LargeOracleUpdate oracleUpdate =
        createOracleUpdateWithActiveProducers(producers, activeProducersBitmap);
    BpOrchestrationContractState initial = getInitial(5, producers);

    BlockProducer producerBeingRemoved = producers.get(0);
    BpOrchestrationContractState state =
        initial
            .withLargeOracleUpdate(oracleUpdate)
            .removeBlockProducer(producerBeingRemoved, FunctionUtility.noOpConsumer());
    BpOrchestrationContractState updated =
        state.withNewLargeOracle(signature, 0, FunctionUtility.noOpConsumer());
    AvlTree<BlockchainAddress, BlockProducer> updatedBlockProducers = updated.getBlockProducers();
    Assertions.assertThat(
            updatedBlockProducers.getValue(producerBeingRemoved.getIdentity()).getStatus())
        .isEqualTo(BlockProducer.BlockProducerStatus.REMOVED);
    Assertions.assertThat(updated.getCommittee()).doesNotContain(producerBeingRemoved);
  }

  /**
   * If a producer has status 'REMOVED' before a new large oracle update is triggered, they are not
   * included as block producer after update.
   */
  @Test
  void producersWhoAreRemovedPriorToTriggeringLargeOracleUpdateAreRemovedAfterUpdate() {
    List<BlockProducer> producers = createConfirmedProducers(5);
    BlockProducer producerToBeRemoved =
        createProducer("inactiveRemoved", BlockProducer.BlockProducerStatus.REMOVED);
    producers.add(producerToBeRemoved);

    BpOrchestrationContractState initial = getInitial(5, producers);

    // trigger new committee by waiting one month
    setBlockProductionTime(ONE_MONTH_MILLISECONDS + initial.getCommitteeEnabledTimeStamp());
    BpOrchestrationContractState state =
        serialization.invoke(
            context,
            initial,
            rpc -> {
              rpc.writeByte(BpOrchestrationContract.Invocations.TRIGGER_NEW_COMMITTEE);
            });

    List<BlockProducer> oracleUpdateBps = state.getOracleUpdate().getProducers();
    Assertions.assertThat(oracleUpdateBps).doesNotContain(producerToBeRemoved);
    Assertions.assertThat(oracleUpdateBps.size()).isEqualTo(5);
    byte[] activeness = new byte[] {(byte) 0b1111};
    state = executeLargeOracleUpdateProtocol(state, activeness);

    List<BlockchainAddress> afterUpdateBps =
        state.getBlockProducers().values().stream().map(BlockProducer::getIdentity).toList();
    Assertions.assertThat(afterUpdateBps).doesNotContain(producerToBeRemoved.getIdentity());
  }

  /**
   * Executes entire large oracle protocol for a state with a large oracle update.
   *
   * @param state state with large oracle update
   * @param heartBeat activeness of producers
   * @return state after large oracle update
   */
  private BpOrchestrationContractState executeLargeOracleUpdateProtocol(
      BpOrchestrationContractState state, byte[] heartBeat) {
    List<BlockProducer> oracleUpdateProducers = state.getOracleUpdate().getProducers();
    int maliciousCount = (oracleUpdateProducers.size() - 1) / 3;
    for (BlockProducer bp : oracleUpdateProducers.subList(0, 2 * maliciousCount + 1)) {
      from(bp.getIdentity());
      state = addHeartbeat(heartBeat, state);
    }

    BlockchainPublicKey tpk = new KeyPair(BigInteger.TEN).getPublic();
    List<BlockchainAddress> honestParties =
        oracleUpdateProducers.stream().map(BlockProducer::getIdentity).toList();
    for (BlockProducer bp : oracleUpdateProducers.subList(0, 2 * maliciousCount + 1)) {
      from(bp.getIdentity());
      state = addCandidate(state, bp, tpk, "keyId", honestParties);
    }

    LargeOracleUpdate oracleUpdate = state.getOracleUpdate();
    Hash message = oracleUpdate.getMessageToBeSigned(state.getSessionId() + 1, domainSeparator);
    Signature sign = thresholdSecretKey.sign(message);
    state = addSignature(state, initialProducers.get(0), sign);
    return state;
  }

  private static LargeOracleUpdate createOracleUpdateWithActiveProducers(
      List<BlockProducer> producers, Bitmap activeProducers) {
    LargeOracleUpdate oracleUpdate = LargeOracleUpdate.create(producers);
    for (BlockProducer producer : producers) {
      oracleUpdate =
          oracleUpdate.addHeartbeat(producer.getIdentity(), activeProducers.asBytes(), 1, null);
    }
    return oracleUpdate;
  }

  /**
   * Inactive producers can mark themselves active with invocation 'MARK_AS_ACTIVE' and will receive
   * 'CONFIRMED' status.
   */
  @Test
  public void markSelfAsActive() {
    Random random = new Random(1234);
    KeyPair keyPair = new KeyPair(new BigInteger(32, random));
    BlsKeyPair blsKeyPair = new BlsKeyPair(new BigInteger(32, random));
    String identity = "markingActive";
    BlockProducer inactiveProducer =
        createProducerWithBls(
            identity, blsKeyPair, keyPair, BlockProducer.BlockProducerStatus.INACTIVE);
    from(inactiveProducer.getIdentity());

    List<BlockProducer> blockProducers = new ArrayList<>(initialProducers);
    blockProducers.add(inactiveProducer);

    BpOrchestrationContractState state = createStateWithBlockProducers(blockProducers);

    Hash message =
        state.createPopMessage(
            inactiveProducer.getIdentity(),
            inactiveProducer.getPublicKey(),
            inactiveProducer.getBlsPublicKey());
    BlsSignature signature = blsKeyPair.sign(message);

    BpOrchestrationContractState markingActiveState =
        serialization.invoke(
            context,
            state,
            rpc -> {
              rpc.writeByte(BpOrchestrationContract.Invocations.MARK_AS_ACTIVE);
              signature.write(rpc);
            });
    Assertions.assertThat(
            markingActiveState
                .getBlockProducers()
                .getValue(inactiveProducer.getIdentity())
                .getStatus())
        .isEqualTo(BlockProducer.BlockProducerStatus.CONFIRMED);
  }

  /** Producers need a valid BlsSignature to mark themselves active. */
  @Test
  void cannotMarkActiveBpWithInvalidPopSignature() {
    BlockProducer producer = createProducer("bogus", BlockProducer.BlockProducerStatus.INACTIVE);
    from(producer.getIdentity());
    BpOrchestrationContractState state = createStateWithBlockProducers(List.of(producer));
    BlsSignature signature =
        new BlsKeyPair(BigInteger.ONE).sign(Hash.create(s -> s.writeString("invalid")));
    Assertions.assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    state,
                    rpc -> {
                      rpc.writeByte(BpOrchestrationContract.Invocations.MARK_AS_ACTIVE);
                      signature.write(rpc);
                    }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Invalid proof of possession of the BLS key");
  }

  /** Producers need to already be registered to mark as active. */
  @Test
  void onlyExistingBlockProducersCanBeMarkedActive() {
    String identity = "Doesnt Exist";
    Hash hash = Hash.create(s -> s.writeString(identity));
    SecureRandom secureRandom = new SecureRandom(hash.getBytes());
    KeyPair keyPair = new KeyPair(new BigInteger(32, secureRandom));
    BlsKeyPair blsKeyPair = new BlsKeyPair(new BigInteger(32, secureRandom));
    BlockProducer doesntExist =
        createProducerWithBls(
            identity, blsKeyPair, keyPair, BlockProducer.BlockProducerStatus.INACTIVE);
    from(doesntExist.getIdentity());

    BpOrchestrationContractState state = getInitial();

    Hash message =
        state.createPopMessage(
            doesntExist.getIdentity(), doesntExist.getPublicKey(), doesntExist.getBlsPublicKey());
    BlsSignature signature = blsKeyPair.sign(message);
    Assertions.assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    state,
                    rpc -> {
                      rpc.writeByte(BpOrchestrationContract.Invocations.MARK_AS_ACTIVE);
                      signature.write(rpc);
                    }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Only inactive block producers can be marked as active");
  }

  /**
   * Producers who do not have the 'INACTIVE'-status cannot mark themselves active with invocation
   * 'MARK_AS_ACTIVE'.
   */
  @Test
  public void nonInactiveCannotMarkSelfAsActive() {
    String identity = "unconfirmed";
    Hash hash = Hash.create(s -> s.writeString(identity));
    SecureRandom secureRandom = new SecureRandom(hash.getBytes());
    KeyPair keyPair = new KeyPair(new BigInteger(32, secureRandom));
    BlsKeyPair blsKeyPair = new BlsKeyPair(new BigInteger(32, secureRandom));
    BlockProducer nonInactiveProducer =
        createProducerWithBls(
            identity, blsKeyPair, keyPair, BlockProducer.BlockProducerStatus.PENDING);
    from(nonInactiveProducer.getIdentity());

    List<BlockProducer> blockProducers = new ArrayList<>(initialProducers);
    blockProducers.add(nonInactiveProducer);

    BpOrchestrationContractState state = createStateWithBlockProducers(blockProducers);

    Hash message =
        state.createPopMessage(
            nonInactiveProducer.getIdentity(),
            nonInactiveProducer.getPublicKey(),
            nonInactiveProducer.getBlsPublicKey());
    BlsSignature signature = blsKeyPair.sign(message);

    Assertions.assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    state,
                    rpc -> {
                      rpc.writeByte(BpOrchestrationContract.Invocations.MARK_AS_ACTIVE);
                      signature.write(rpc);
                    }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Only inactive block producers can be marked as active");
  }

  /** Invoking GET_PRODUCERS returns all active block producers to the caller. */
  @Test
  public void getProducers() {
    BpOrchestrationContractState state = createStateWithBlockProducers(initialProducers);
    state =
        state.setBlockProducer(
            createProducer("non-committee producer", BlockProducer.BlockProducerStatus.CONFIRMED));
    // Four producers in committee, five total BPs
    Assertions.assertThat(state.getCommittee().size()).isEqualTo(4);
    Assertions.assertThat(state.getBlockProducers().size()).isEqualTo(5);

    BpOrchestrationContractState stateAfterInvocation =
        serialization.invoke(
            context,
            state,
            rpc -> {
              rpc.writeByte(BpOrchestrationContract.Invocations.GET_PRODUCERS);
            });
    byte[] activeProducers = context.getResult();
    List<BlockchainAddress> activeProducerAddresses =
        BlockchainAddress.LIST_SERIALIZER.readDynamic(
            SafeDataInputStream.createFromBytes(activeProducers));
    Assertions.assertThat(activeProducerAddresses)
        .hasSameElementsAs(initialProducers.stream().map(BlockProducer::getIdentity).toList());
    // Invocation does not alter contract state
    StateSerializableEquality.assertStatesEqual(state, stateAfterInvocation);
  }

  private Consumer<SafeDataOutputStream> createRegisterRpc(
      BlockProducer blockProducer, BlsSignature signature) {
    return rpc -> {
      rpc.writeByte(BpOrchestrationContract.Invocations.REGISTER);
      rpc.writeString(blockProducer.getName());
      rpc.writeString(blockProducer.getWebsite());
      rpc.writeString(blockProducer.getAddress());
      blockProducer.getPublicKey().write(rpc);
      blockProducer.getBlsPublicKey().write(rpc);
      rpc.writeInt(blockProducer.getEntityJurisdiction());
      rpc.writeInt(blockProducer.getServerJurisdiction());
      signature.write(rpc);
    };
  }

  private BpOrchestrationContractState getUnconfirmedNewOracle(
      List<BlockProducer> newOracle, BlockchainPublicKey newKey) {
    BpOrchestrationContractState initial = getInitial();
    LargeOracleUpdate update = LargeOracleUpdate.create(newOracle);
    update = LargeOracleUpdateTest.markAllActive(update, newOracle);
    BpOrchestrationContractState state = initial.withLargeOracleUpdate(update);
    List<BlockchainAddress> honestParties =
        newOracle.stream().map(BlockProducer::getIdentity).toList();
    for (int i = 0; i < (newOracle.size() / 2) + 1; i++) {
      state =
          addCandidate(state, newOracle.get(i), newKey, "Magic identifier for key", honestParties);
    }
    return state;
  }

  private BpOrchestrationContractState addCandidate(
      BpOrchestrationContractState state,
      BlockProducer producer,
      BlockchainPublicKey key,
      String keyId,
      List<BlockchainAddress> honestParties) {
    from(producer.getIdentity());
    Bitmap bitmap = Bitmap.allOnes(honestParties.size());
    return serialization.invoke(
        context,
        state,
        rpc -> {
          rpc.writeByte(BpOrchestrationContract.Invocations.ADD_CANDIDATE_KEY);
          key.write(rpc);
          rpc.writeString(keyId);
          rpc.writeDynamicBytes(bitmap.asBytes());
        });
  }

  private BpOrchestrationContractState addSignature(
      BpOrchestrationContractState state, BlockProducer producer, Signature signature) {
    from(producer.getIdentity());
    return serialization.invoke(
        context,
        state,
        rpc -> {
          rpc.writeByte(BpOrchestrationContract.Invocations.AUTHORIZE_NEW_ORACLE);
          signature.write(rpc);
        });
  }

  private void from(BlockchainAddress fromAddress) {
    context =
        new SysContractContextTest(
            context.getBlockProductionTime(), context.getBlockTime(), fromAddress);
  }

  private BpOrchestrationContractState createStateWithBlockProducers(
      List<BlockProducer> blockProducers) {
    return BpOrchestrationContractState.initial(
        FixedList.create(kycAddresses),
        blockProducers,
        thresholdKey,
        thresholdKeyId,
        largeOracleContract,
        systemUpdateContractAddress,
        rewardsContractAddress,
        domainSeparator,
        0,
        0);
  }

  private static AvlTree<BlockchainAddress, BlockProducer>
      createEnoughConfirmedProducersToActiveOracleUpdate() {
    BlockProducer acceptedProducer =
        createProducer("acceptedProducer", BlockProducer.BlockProducerStatus.CONFIRMED);
    BlockProducer acceptedProducer2 =
        createProducer("acceptedProducer2", BlockProducer.BlockProducerStatus.CONFIRMED);
    BlockProducer acceptedProducer3 =
        createProducer("acceptedProducer3", BlockProducer.BlockProducerStatus.CONFIRMED);
    BlockProducer acceptedProducer4 =
        createProducer("acceptedProducer4", BlockProducer.BlockProducerStatus.CONFIRMED);

    AvlTree<BlockchainAddress, BlockProducer> blockProducers = AvlTree.create();
    blockProducers =
        blockProducers
            .set(acceptedProducer.getIdentity(), acceptedProducer)
            .set(acceptedProducer2.getIdentity(), acceptedProducer2)
            .set(acceptedProducer3.getIdentity(), acceptedProducer3)
            .set(acceptedProducer4.getIdentity(), acceptedProducer4);
    return blockProducers;
  }

  static List<BlockProducer> createConfirmedProducers(int numberOfProducers) {
    List<BlockProducer> producers = new ArrayList<>();
    for (int i = 0; i < numberOfProducers; i++) {
      String uniqueName = i + " " + System.currentTimeMillis() + new Random().nextDouble();
      producers.add(createProducer(uniqueName, BlockProducer.BlockProducerStatus.CONFIRMED));
    }
    return producers;
  }

  static BlockProducer createProducerWithBls(
      String identity,
      BlsKeyPair blsKeyPair,
      KeyPair keyPair,
      BlockProducer.BlockProducerStatus status) {
    return new BlockProducer(
        identity + "name",
        identity + "website",
        identity + "address",
        BlockchainAddress.fromHash(
            BlockchainAddress.Type.ACCOUNT, Hash.create(h -> h.writeString(identity))),
        keyPair.getPublic(),
        blsKeyPair.getPublicKey(),
        1,
        42,
        42,
        status,
        null);
  }

  static BlockProducer createProducer(String identity, BlockProducer.BlockProducerStatus status) {
    Hash hash = Hash.create(s -> s.writeString(identity));
    SecureRandom secureRandom = new SecureRandom(hash.getBytes());
    KeyPair keyPair = new KeyPair(new BigInteger(32, secureRandom));
    BlsKeyPair blsKeyPair = new BlsKeyPair(new BigInteger(32, secureRandom));
    return createProducerWithBls(identity, blsKeyPair, keyPair, status);
  }

  static BlockProducer readBlockproducer(SafeDataInputStream stream) {
    String name = stream.readString();
    String website = stream.readString();
    String address = stream.readString();
    BlockchainAddress identity = BlockchainAddress.read(stream);
    long numberOfVotes = stream.readLong();
    BlockchainPublicKey publicKey = BlockchainPublicKey.read(stream);
    BlsPublicKey blsPublicKey = BlsPublicKey.read(stream);
    int entityJurisdiction = stream.readInt();
    int serverJurisdiction = stream.readInt();
    BlockProducer.BlockProducerStatus status =
        stream.readEnum(BlockProducer.BlockProducerStatus.values());
    return new BlockProducer(
        name,
        website,
        address,
        identity,
        publicKey,
        blsPublicKey,
        numberOfVotes,
        entityJurisdiction,
        serverJurisdiction,
        status,
        null);
  }

  /**
   * Write version to stream.
   *
   * @param stream stream to write to
   */
  public void writeVersion(SemanticVersion version, SafeDataOutputStream stream) {
    stream.writeInt(version.major());
    stream.writeInt(version.minor());
    stream.writeInt(version.patch());
  }

  private void setBlockProductionTime(long blockProductionTime) {
    context =
        new SysContractContextTest(blockProductionTime, context.getBlockTime(), context.getFrom());
  }
}
