package com.partisiablockchain.governance.bporchestration;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.tree.AvlTree;
import com.secata.tools.immutable.FixedList;

/** Contract state before minimum version added for testing upgrading to new state. */
@Immutable
record OldBpOrchestrationContractState(
    ThresholdKey thresholdKey,
    FixedList<BlockchainAddress> kycAddresses,
    int sessionId,
    int retryNonce,
    LargeOracleUpdate oracleUpdate,
    AvlTree<BlockchainAddress, OldBlockProducer> blockProducers,
    FixedList<OldBlockProducer> committee,
    BlockchainAddress largeOracleContract,
    BlockchainAddress systemUpdateContractAddress,
    BlockchainAddress rewardsContractAddress,
    AvlTree<Integer, CommitteeSignature> committeeLog,
    Hash domainSeparator,
    long broadcastRoundDelay,
    long committeeEnabledTimestamp,
    FixedList<BlockchainAddress> confirmedBlockProducersSinceLastCommittee)
    implements StateSerializable {}
