package com.partisiablockchain.governance.bporchestration;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.serialization.StateAccessor;
import java.math.BigInteger;
import nl.jqno.equalsverifier.EqualsVerifier;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class ThresholdKeyTest {

  @Test
  void fromStateAccessorNull() {
    ThresholdKey key = ThresholdKey.createFromStateAccessorNullSafe(StateAccessor.create(null));
    Assertions.assertThat(key).isNull();
  }

  @Test
  public void thresholdKey() {
    ThresholdKey thresholdKey = new ThresholdKey(new KeyPair(BigInteger.TEN).getPublic(), "10");
    EqualsVerifier.simple()
        .forClass(ThresholdKey.class)
        .withPrefabValues(
            BlockchainPublicKey.class,
            new KeyPair(BigInteger.TEN).getPublic(),
            new KeyPair(BigInteger.TWO).getPublic())
        .withNonnullFields("key", "keyId")
        .verify();
    Assertions.assertThat(thresholdKey.toString())
        .isEqualTo("ThresholdKey{key=A6BDTZ5H88hiNUd8exrmrl00QtSbGUPCt1KmjipH4kfH, keyId='10'}");
    Assertions.assertThat(thresholdKey.hashCode()).isEqualTo(1386841970);
  }
}
