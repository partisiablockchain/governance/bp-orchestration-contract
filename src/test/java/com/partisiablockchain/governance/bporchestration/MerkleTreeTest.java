package com.partisiablockchain.governance.bporchestration;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.crypto.Hash;
import java.util.Collections;
import java.util.List;
import org.junit.jupiter.api.Test;

/** Test. */
public final class MerkleTreeTest {

  @Test
  public void merkleTreeFromEmptyLeaves() {
    Hash root = MerkleTree.buildTreeFrom(Collections.emptyList());
    assertThat(root).isEqualTo(Hash.create(stream -> {}));
  }

  @Test
  public void merkleTreeFromSingleLeaf() {
    Hash element = Hash.create(s -> s.writeInt(123));
    Hash merkleTree = MerkleTree.buildTreeFrom(List.of(element));
    assertThat(merkleTree).isEqualTo(element);
  }

  @Test
  public void failureWhenBuildingMerkleTreeWithEqualLeaves() {
    Hash element = Hash.create(s -> s.writeInt(123));
    assertThatThrownBy(() -> MerkleTree.buildTreeFrom(List.of(element, element)))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Two element's digests were the same");
  }

  @Test
  void oddNumberOfLeaves() {
    Hash element0 = Hash.create(s -> s.writeInt(123));
    Hash element1 = Hash.create(s -> s.writeInt(456));
    Hash element2 = Hash.create(s -> s.writeInt(789));
    Hash hash = MerkleTree.buildTreeFrom(List.of(element0, element1, element2));
    Hash expected =
        Hash.create(
            ss -> {
              Hash.create(
                      s -> {
                        element1.write(s);
                        element0.write(s);
                      })
                  .write(ss);
              element2.write(ss);
            });
    assertThat(hash).isEqualTo(expected);
  }
}
